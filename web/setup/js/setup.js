/**
 * OpenSong Library Online
 * 
 * version: 2.0
 * file version: 2.0
 * updated: 2021-06-18 | 07:33 UTC
 * 
 */
/* JavaScript file for OpenSong Library Display Site Setup */

/** 
 * Name: checkAdminPwd
 * Description: checks to see if the admin password is okay
 * Arguments: none
 * Returns: boolean to prevent page from being submitted
 */
function checkAdminPwd() {
    var p = $("#site_admin_pwd").val();
    var result = true;

    $("#site_admin_pwd").removeClass("is-invalid");

    if ((p.length > 0 && p.length < 8) || (!p.match(/[A-Z]/) || (!p.match(/[a-z]/)) || (!p.match(/[0-9]/)) || (p.match(/[\"\'\\]/)) )) {
        result = false;
        $("#site_admin_pwd").addClass("is-invalid");
    }
    return result;
}

/** 
 * Name: checkAdminEmail
 * Description: checks to see if the admin email is valid
 * Arguments: none
 * Returns: boolean to prevent page from being submitted
 */
function checkAdminEmail() {
    var e = $("#site_admin_email").val();
    const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    $("#site_admin_email").removeClass("is-invalid");

    if (e.length > 0 && $("#site_admin").val().length > 0 && !re.test(e)) {
        $("#site_admin_email").addClass("is-invalid");
        return false;
    } else {
        return true;
    }
}

/** 
 * Name: checkAdminName
 * Description: checks to see if the admin user name is valid
 * Arguments: none
 * Returns: boolean to prevent page from being submitted
 */
function checkAdminName() {
    var n = $("#site_admin").val();
    var re = /^[A-Za-z0-9_\.\-]+$/;

    $("#site_admin").removeClass("is-invalid");

    if (n.length > 0 && !re.test(n)) {
        $("#site_admin").addClass("is-invalid");
        return false;
    } else {
        return true;
    }
}

/**
 * Name: checkDbPwd
 * Description: checks to see if the database password is long enough
 * Arguments: none
 * Returns: boolean to prevent page from being submitted
 */
function checkDbPwd() {
    $("#db_pass").removeClass("is-invalid");

    if ($("#db_pass").val().length < 4) {
        $("#db_pass").addClass("is-invalid");
        return false;
    } else {
        return true;
    }
}

/**
 * Name: showPass
 * Description: converts the #db_pass field between text and password types
 * Arguments: none
 * Returns: boolean false to prevent page from being submitted
 */
function showPass() {
    var field = $("#db_pass");
    if (field.attr("type") == "password") {
        $("#show_hide_pass").html('<i class="far fa-eye-slash"></i>');
        field.attr("type","text");
    } else if (field.attr("type") == "text") {
        $("#show_hide_pass").html('<i class="far fa-eye"></i>');
        field.attr("type","password");
    }
    return false;
}

/**
 * Name: showPass
 * Description: converts the #db_pass field between text and password types
 * Arguments: none
 * Returns: boolean false to prevent page from being submitted
 */
function validateAll() {
    /* in case user jumped over the db password field, which is required */
    checkDbPwd();
    if ($(".field-check").hasClass("is-invalid")) {
        $("body").scrollTo("#site_admin");
        return false;
    } else {
        return true;
    }
}

$(document).ready(
    function() {
        $("#db_pass").blur(checkDbPwd);
        $("#site_admin_pwd").blur(checkAdminPwd);
        $("#site_admin_email").blur(checkAdminEmail);
        $("#site_admin").blur(checkAdminName);
    }
);
