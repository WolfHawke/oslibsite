<?php
/* 
 * OpenSong Library Online - Display Site
 * Site setup page
 *
 * Copyright © 2011, 2020, 2021 Hawke AI. All Rights Reserved
 *
 * Last Updated: 2020-10-20 | JMW
 */
require_once("../lib/pCloud/autoload.php");

$appKey = "UCEwprBjtBQ";
$appSecret = "6uolCa67uqScI9yJLACOLhcE5Sm7";
$redirect_uri = "";

/** 
 * checkForDir: checks for a directory in pCloud
 * Returns: boolean denoting success
 * 
 * @var     string      access token
 * @var     string      folder name to check
 */
function checkForDir($token, $fn) {
    $locationid = 1;

    /* connect to pCloud */
	$pCloudApp = new pCloud\App();
	$pCloudApp->setAccessToken($token);
	$pCloudApp->setLocationId($locationid);

    /* instantiate objects */
    $pCloudFolder = new pCloud\Folder($pCloudApp);
 
    /* get folder id */
    $folder_id = $pCloudFolder->search("{$fn}/*");
    if ($folder_id == "nodir") { return FALSE; } else { return TRUE; }
}

/**
 * randomId: generates an alphanumeric random ID of a specified length
 * Returns: a string containing alphanumeric random ID
 *
 * @var     int     how many characters the random Id should be in length; defaults to 8
 * @var     string  "any","decimal","alphabet","hexadecimal"; defaults to any
 */
function randomId($length=8,$type='any') {
	$seed = array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G",
	              "H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X",
				  "Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o",
				  "p","q","r","s","t","u","v","w","x","y","z");
	$id = "";
	$start = 0; $end = count($seed)-1;
	if ($type == 'decimal') {
		$end = 9;
	} elseif ($type == 'hexadecimal') {
		$end = 15;
	} elseif ($type == 'alphabet') {
		$start = 10;
	}
	for ($i=0;$i<$length;$i++) {
		$id .= $seed[rand($start,$end)];
	}
	return $id;
}

/**
 * sanitize: Removes HTML and php tags and \ and other disallowed characters from strings
 * Returns: a sanitized string
 * 
 * @var     string  string to sanitize
 * @var     string  type of string: 'regular' (default) or 'filename'
 */
function sanitize($s, $t='regular') {
    $fn = array(':', '?', '*', '<', '>', '|', '"');
    
    $out = strip_tags($s);
    $out = str_replace('\\', '/', $out);
    
    if ($t == 'filename') {
        foreach($fn as $c) {
            $out = str_replace($c, '', $out);
        } 
    }
    return $out;
}

$html = "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <title>OpenSong Library Online Setup</title>
    <!-- CSS -->
    <link rel=\"stylesheet\" href=\"../css/bootstrap.min.css\">
    <link rel=\"stylesheet\" href=\"../fontawesome/css/all.min.css\">
    <link rel=\"stylesheet\" href=\"../css/fac.css\">
    <link rel=\"stylesheet\" href=\"../css/main.css\">
</head>
<body class=\"scrollable\">
<nav class=\"navbar navbar-expand navbar-dark bg-purple\">
        <span class=\"navbar-brand mb-0 h1\">
            <img class=\"oslw-logo\" src=\"../img/oslsite-logo.svg\" alt=\"OpenSong Library Online\">
            OpenSong Library Online Setup
        </span>
</nav>
<div class=\"container\">

";

$app_title = '';
$db_base = '';
$db_host = 'localhost';
$db_pass = '';
$db_user = '';
$new_dir = '';
$os_dir = '';
$proc_dir = '';

/* check if we already have a valid defs.php */
if (file_exists("../config/defs.php")){
    if (isset($_GET['unlock-code'])) {
        include ("../config/defs.php");
        if (isset($access_token) && strlen($access_token) >= 52 && $_GET['unlock-code'] == $unlock_code) {
            $_POST['action'] = 'gentoken';
            if (isset($_GET['error'])) {
                $html .= "<div class=\"alert alert-danger mt-4\"><b><i class=\"far fa-times\"></i> ";
                switch ($_GET['error']) {
                    case 'noremote':
                        $html .= "Unable to connect to the remote directories on pCloud. Please check the directory information you entered and try again.";
                        break;
                    case 'nodb':
                        $html .= "Unable to connect to the database. Please check your database settings and try again.";
                        break;
                }
                $html .= "</b></div>";
            } else {
                $html .= "<div class=\"alert alert-warning mt-4\"><b><i class=\"fal fa-exclamation-triangle\"></i> If you run the setup script now, it will completely reset the app, deleting all data in the database. Only proceed if you are certain. <em>You have been warned!</em></b></div>";    
            }
            $new_dir = substr($new_dir, strrpos($new_dir,"/"));
            $proc_dir = substr($proc_dir, strrpos($proc_dir,"/"));
        } else {
            header("Location: ../");
        }
    } elseif (isset($_POST['action']) && $_POST['action'] == 'savesettings') {
        $html .= "";
        /* really don't do anything here, just execute the script */

    } else {
        header("Location: ../");
    }
} 

try {

    $pCloudApp = new pCloud\App();
    $pCloudApp->setAppKey($appKey);
    $pCloudApp->setAppSecret($appSecret);
    $pCloudApp->setRedirectURI($redirect_uri);

    if (!isset($_POST['action'])) {
        $codeUrl = $pCloudApp->getAuthorizeCodeUrl();
        $html .= "<h1>Connect OpenSong Library Online to pCloud</h1>
        <div id=\"connect_link\">
            <p>Click the button below to log on to pCloud and retrieve the verification code in a new window. When prompted, click the Allow button to give the OpenSong Library Online (oslibweb) app permission to access your pCloud account.</p>
        <p class=\"text-center\"><a href=\"{$codeUrl}\" target=\"_blank\" class=\"btn btn-secondary btn-purple\" onclick=\"$('#connect_link').fadeOut(400, function() { $('#code_form').fadeIn(); });\"><i class=\"fal fa-cloud\"></i>&nbsp;&nbsp;Connect to pCloud</a></p>
        </div>
    <form method=\"POST\" action=\"index.php\" id=\"code_form\">
        <input type=\"hidden\" name=\"action\" value=\"gentoken\">
        <p>Copy the verification code and paste it into the form below to add the access credentials to the OpenSong Library Online.</p>
        <div class=\"form-group\">
            <label for=\"access_code\">Access Code:</label>
            <input type=\"text\" class=\"form-control\" name=\"access_code\" id=\"access_code\" placeholder=\"Paste the pCloud access code here.\">
        </div>
        <p><label for=\"locationid\">Hostname:</label>
            <input type=\"text\" class=\"form-control\" name=\"locationid\" id=\"locationid\" placeholder=\"Hostname\" value=\"api.pcloud.com\">
        </p>
        <p class=\"text-center\"><button class=\"btn btn-secondary btn-purple\"><i class=\"fal fa-badge-check\"></i>&nbsp;&nbsp;Save Access Token</button></p>
    </form>\n";
    } elseif ($_POST['action'] == 'gentoken') {
        if (isset($access_token)) {
            $token = array("access_token" => $access_token);
        } else {
            $token = $pCloudApp->getTokenFromCode($_POST['access_code'], $_POST['locationid']);
        }
        $html .= "<h1>Configure OpenSong Library Online</h1>
        <form method=\"POST\" action=\"index.php\" onsubmit=\"validateAll();\">
            <input type=\"hidden\" name=\"action\" value=\"savesettings\">
            <input type=\"hidden\" name=\"access_token\" value=\"{$token['access_token']}\">
            <h2>Configure Display</h2>
            <div class=\"form_group\">
                <label for=\"app_title\">App title:</label>
                <input class=\"form-control\" type=\"text\" name=\"app_title\" id=\"app_title\" placeholder=\"e.g. My OpenSong Library\" tabindex=\"1\" value=\"{$app_title}\">
                <small>This will display as the title on the front page when the site is live.</small>
            </div>
            <h2>Configure Song File Locations</h2>        
            <div class=\"form_group\">
                <label for=\"os_dir\">Path to OpenSong data directories:</label>
                <input type=\"text\" name=\"os_dir\" id=\"os_dir\" class=\"form-control\" placeholder=\"e.g. /OpenSong/OpenSong Data/Songs/New\" tabindex=\"2\" value=\"{$os_dir}\">
                <small>Enter the location to the <code>OpenSong Data</code> directory in pCloud. The path is relative to your pCloud root. Use <code>/</code> as directory separators, not <code>&bsol;</code>. Begin with a <code>/</code>, but do not append a one!</small>
            </div>
            <div class=\"form-group\">
                <label for=\"proc_dir\">Directory containing processed (displayed) songs:</label>
                <input type=\"text\" name=\"proc_dir\" id=\"proc_dir\" class=\"form-control\" placeholder=\"e.g. /Processed\" tabindex=\"3\" value=\"{$proc_dir}\">
                <small>&ldquo;Processed&rdquo; songs are ones that are already in the database. This is relative to the <code>Songs</code> directory in the OpenSong data directory defined above.  Use <code>/</code> as directory separators, not <code>&bsol;</code>. Begin with a <code>/</code>, but do not append a one!</small>
            </div>
            <div class=\"form_group\">
                <label for=\"new_dir\">Directory containing new songs:</label>
                <input type=\"text\" name=\"new_dir\" id=\"new_dir\" class=\"form-control\" placeholder=\"e.g. /New\" tabindex=\"4\" value=\"{$new_dir}\">
                <small>New songs are ones that have been entered, but are not yet loaded into the online database. This is relative to the <code>Songs</code> directory in the OpenSong data directory defined above. Use <code>/</code> as directory separators, not <code>&bsol;</code>. Begin with a <code>/</code>, but do not append a one!</small>
            </div>
            

            <h2>Configure Database Connection</h2>
            <p>Fill in the fields below to connect the OpenSong Library Online app to the local MySQL/Mariadb Database.</p>
            <div class=\"form-group row\">
                <label for=\"db_host\" class=\"col-sm-2 col-form-label\">Host Server:</label>
                <div class=\"col-sm-4\">
                    <input type=\"text\" class=\"form-control\" name=\"db_host\" id=\"db_host\" placeholder=\"I.P. address or domain name\" tabindex=\"5\" value=\"{$db_host}\">
                </div>
                <div class=\"col-sm-6\"></div>
              </div>
            <div class=\"form-group row\">
                <label for=\"db_base\" class=\"col-sm-2 col-form-label\">Database:</label>
                <div class=\"col-sm-4\">
                    <input type=\"text\" class=\"form-control\" name=\"db_base\" id=\"db_base\" placeholder=\"e.g. oslib\" tabindex=\"6\" value=\"{$db_base}\">
                </div>
                <div class=\"col-sm-6\"></div>
            </div>
            <div class=\"form-group row\">
                    <label for=\"db_user\" class=\"col-sm-2 col-form-label\">User Name:</label>
                    <div class=\"col-sm-4\">
                        <input type=\"text\" class=\"form-control\" name=\"db_user\" id=\"db_user\" placeholder=\"e.g. oslibusr\" tabindex=\"7\" value=\"{$db_user}\">
                    </div>
                    <div class=\"col-sm-6\"></div>
            </div>
            <div class=\"form-group row\">
                <label for=\"db_pass\" class=\"col-sm-2 col-form-label\">User Password:</label>
                <div class=\"col-sm-4\">
                    <input type=\"password\" class=\"form-control field-check\" name=\"db_pass\" id=\"db_pass\" placeholder=\"e.g. Pass1234\" tabindex=\"8\">
                    <div class=\"invalid-feedback\">The database password you entered must be at least 4 characters in length.</div>
                </div>
                <div class=\"col-sm-6\">
                    <button id=\"show_hide_pass\" class=\"btn btn-outline-dark\" onclick=\"showPass(); return false;\" title=\"Show Password\"><i class=\"far fa-eye\"></i></button>
                </div>
            </div>

            <h2>Set Up Site Administrator (optional)</h2>
            <p>Setting up a site admininistrator is optional. Defining the site administrator unlocks advanced features in OpenSong tied to user creation. Users can add songs to the library and create sets directly from the online application. Leaving these fields empty keeps the application in display-only mode. You can add the main administrator later without destroying the database by using the <code>mainadmintool.php</code> command line tool (see documentation for details).</p>
            <div class=\"form-group row\">
                <label for=\"site_admin\" class=\"col-sm-2 col-form-label\">Admin User Name:</label>
                <div class=\"col-sm-9\">
                    <input type=\"text\" class=\"form-control field-check\" name=\"site_admin\" id=\"site_admin\" placeholder=\"SiteServant\" tabindex=\"9\">
                    <small>User name must be at least 3 characters long, is case sensitive, and can only consist of standard upper- and lowercase alphanumeric characters and <code>. _</code> or <code>-</code>. Special international characters cannot be used. We recommend <i>not</i> using well-know user names for an administrator like &ldquo;admin&rdquo; or &ldquo;Administator&rdquo; or &ldquo;Manager&rdquo;.</small>
                    <div class=\"invalid-feedback\">The user name entered does not follow the requirements above. Please check it and try again.</div>
                </div>
            </div>
            <div class=\"form-group row\">
                <label for=\"site_admin_pwd\" class=\"col-sm-2 col-form-label\">Admin Password:</label>
                <div class=\"col-sm-9\">
                    <input type=\"password\" class=\"form-control field-check\" name=\"site_admin_pwd\" id=\"site_admin_pwd\" placeholder=\"Password\" tabindex=\"10\">
                    <small>Password must be at least 8 characters long and contain at least one upper case letter, one lowercase letter and one numeral. It is recommended to use special characters except <code>&quot; &apos;</code> or <code>&bsol;</code>.</small>
                    <div class=\"invalid-feedback\" id=\"password_error_incorrect\">The password does not conform to the specifications above. Please check it and try again.</div>
                </div>
            </div>
            <div class=\"form-group row\">
                <label for=\"site_admin_email\" class=\"col-sm-2 col-form-label\">Admin e-Mail Address:</label>
                <div class=\"col-sm-9\">
                    <input type=\"text\" class=\"form-control field-check\" name=\"site_admin_email\" id=\"site_admin_email\" placeholder=\"siteservant@{$_SERVER['HTTP_HOST']}\" tabindex=\"11\">
                    <small>This e-mail address is only used to send app update notifications (like new songs submitted to the library) to; it is only displayed on your profile page.</small>
                    <div class=\"invalid-feedback\">The e-mail address is invalid. Please check it and try again.</div>
                </div>
            </div>
            <p class=\"text-center\"><button class=\"btn btn-secondary btn-purple\" tabindex=\"12\"><i class=\"fal fa-save\"></i>&nbsp;&nbsp;Save Settings</button></p>
        </form>\n";
    } elseif ($_POST['action'] == 'savesettings') {
        $input = array();

        /* sanitize input */
        foreach ($_POST as $k => $i) {
            if ($k != 'action') {
                $type = (strpos($i, 'dir') === FALSE) ? 'regular' : 'filename';
                $input[$k] = sanitize($i, $type);
            }
        }

        if ($input['app_title'] == "") { $input['app_title'] = "OpenSong Library Online"; }

        $input['unlock_code'] = randomId(15);

        $input['sets_dir'] = '';
        $input['new_dir'] = "{$input['os_dir']}/Songs{$input['new_dir']}";
        $input['proc_dir'] = "{$input['os_dir']}/Songs{$input['proc_dir']}";
        
        /* instantiate proper server url */
        $srv_url = 'http';
        if (isset($_SERVER['HTTPS'])) { $srv_url .= 's'; }
        $srv_url .= '://' . $_SERVER['HTTP_HOST'];

        /* instantiate proper REQUEST URI */
        $srv_uri = $_SERVER['REQUEST_URI'];
        if (strpos($srv_uri, '?') !== FALSE) {
            $srv_uri = substr($srv_uri, 0, strpos($srv_uri, '?'));
        }

        /* check for remote folders */
        if (checkForDir($input['access_token'], $new_dir) && checkForDir($input['access_token'], $proc_dir)) {
            if (checkForDir($input['access_token'], "{$input['os_dir']}/Sets")) {
                $input['sets_dir'] = "{$input['os_dir']}/Sets";
            }
        }
        if ($input['sets_dir'] == '') {
            header("Location: ./?unlock-code={$input['unlock_code']}&error=noremote");
        }

        ksort($input);
        
        $out = "<?php
/**
 * OpenSong Library Display Site
 * Site Definitions
 * 
 * Last Updated: " . date('c') . "
 */
\n";
        foreach ($input as $k => $d) {
            if ($k != "action" && $k != "site_admin" && $k != "site_admin_pwd" && $k != "site_admin_email") {
                $out .= '$' . $k . " = '{$d}';\n";
            }
        }

        try {
            if ($f = @fopen("../config/defs.php","w")) {
                fwrite($f, $out);
                fclose($f);

                $html .= "<div class=\"alert alert-success mt-4\"><i class=\"far fa-check\"></i> Settings were saved successfully.</div>";

                /* only insert admin if we have one defined */
                if (isset($input['site_admin']) && ($input['site_admin'] != "")){
                    $admin = array("name" => $input['site_admin'], "pwd" => $input['site_admin_pwd'], 'email' => $input['site_admin_pwd']);
                }

                /* initialize database */
                include("oslibInitDb.php");

                $html .= "<div>To access the setup page again, use the following address: <a href=\"{$srv_url}{$srv_uri}?unlock-code={$input['unlock_code']}\">{$srv_url}{$srv_uri}?unlock-code={$input['unlock_code']}</a>. Save this link somewhere safe.</div>
    <div class=\"text-center mb-4\"><a href=\"../\">Return to OpenSong Library Online Front Page</a></div>";
            } else {
                $html .= "<div class=\"alert alert-danger\"><i class=\"fal fa-exclamation-triangle\"></i> Unable to open file <code>defs.php</code> to write. Please check that you have write access to the <code>config</code> directory and try again.</div>";
            }

        } catch (Exception $e) {
            $html .= "<p><b>Error!</b></p>\n<pre>\n" . $e->getMessage() . "</pre>\n";
        }
    }
} catch (Exception $e) {
    $html .= "<p><b>Error!</b></p>\n<pre>\n" . $e->getMessage() . "</pre>\n";
}

$html .= "</div>

<!-- Scripts -->
<script type=\"text/javascript\" src=\"../js/jquery-3.5.1.min.js\"></script>
<script type=\"text/javascript\" src=\"../js/jquery.scrollTo.min.js\"></script>
<script type=\"text/javascript\" src=\"../js/popper.min.js\"></script>
<script type=\"text/javascript\" src=\"../js/bootstrap.min.js\"></script>
<script type=\"text/javascript\" src=\"js/setup.js\"></script>
</body>
</html>";

print($html);

?>
