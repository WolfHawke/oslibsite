<?php
/* 
 * OpenSong Library Display Site
 * Database initialization script
 *
 * Copyright © 2011, 2020, 2021 Hawke AI. All Rights Reserved
 *
 * Last Updated: 2020-11-23 | JMW
 */
require_once("../config/defs.php");
require_once("../components/db_functions.php");

if ($osdb === FALSE) {
  header("Location: ../?unlock-code={$input['unlock-code']}&error=nodb");
}

if (!isset($html)) { 
    print("Can&rsquo;t be run in standalone mode");
    die();
}

$html .= ("<ul>");

/* array of tables */
$tables = array(
    'os_authors prep' => "DROP TABLE IF EXISTS `os_authors`;",
    'os_authors table' => "CREATE TABLE `os_authors` (
        `auth_key` int(11) UNSIGNED NOT NULL COMMENT 'Primary Key',
        `auth_first` varchar(255) DEFAULT NULL COMMENT 'Author''s first name',
        `auth_last` varchar(255) NOT NULL COMMENT 'Author''s last name or special name'
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
    'os_authors key' => "ALTER TABLE `os_authors` ADD PRIMARY KEY (`auth_key`);",
    'os_authors auto_increment' => "ALTER TABLE `os_authors` MODIFY `auth_key` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';",
    'os_lyrics prep' => "DROP TABLE IF EXISTS `os_lyrics`;",
    'os_lyrics table' => "CREATE TABLE `os_lyrics` (
        `lyrics_key` int(10) UNSIGNED NOT NULL COMMENT 'Primary key',
        `song_id` int(11) UNSIGNED NOT NULL COMMENT 'Internal song ID',
        `lyrics_lyrics` longtext NULL COMMENT 'Full lyrics to the song (with markup)',
        `lyrics_presentation` varchar(255) NULL COMMENT 'Default presentation order for song',
        `lyrics_copyright` varchar(255) NULL COMMENT 'Copyright information'
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
    'os_lyrics key' =>"ALTER TABLE `os_lyrics` ADD PRIMARY KEY (`lyrics_key`);",
    'os_lyrics auto increment' => "ALTER TABLE `os_lyrics` MODIFY `lyrics_key` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key';",
    'os_sa_xlink prep' => "DROP TABLE IF EXISTS `os_sa_xlink`;",
    'os_sa_xlink table' => "CREATE TABLE `os_sa_xlink` (
        `sa_key` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
        `auth_key` int(10) UNSIGNED NOT NULL COMMENT 'Author primary key',
        `song_id` int(11) UNSIGNED NOT NULL COMMENT 'Internal song ID'
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
    'os_sa_xlink key' => "ALTER TABLE `os_sa_xlink` ADD PRIMARY KEY (`sa_key`);",
    'os_sa_xlink auto increment' =>  "ALTER TABLE `os_sa_xlink` MODIFY `sa_key` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';",
    'os_songs prep' => "DROP TABLE IF EXISTS `os_songs`;",'os_songs table' => "CREATE TABLE `os_songs` (
        `song_key` int(10) UNSIGNED NOT NULL COMMENT 'Key of songs',
        `song_id` int(11) UNSIGNED NOT NULL COMMENT 'Internal song ID',
        `song_title` text NOT NULL COMMENT 'Title text',
        `song_sort` text NOT NULL COMMENT 'Sortable song title',
        `song_aka` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Whether or not the title is an alternate title',
        `song_ccli` varchar(11) DEFAULT NULL COMMENT 'SongSelect (CCLI) number of the song',
        `song_tags` varchar(256) DEFAULT NULL COMMENT 'Tags for special topics or seasons the song addresses.',
        `song_filename` varchar(255) DEFAULT NULL COMMENT 'File name of the song in the directory',
        `song_modified` bigint(20) UNSIGNED DEFAULT current_timestamp() COMMENT 'Unix timestamp of date and time the song file was modified (only used for canonical songs'
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
    'os_songs key' => "ALTER TABLE `os_songs` ADD PRIMARY KEY (`song_key`);",
    'os_songs auto increment' => "  ALTER TABLE `os_songs` MODIFY `song_key` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Key of songs';",
    'os_status prep' => "DROP TABLE IF EXISTS `os_status`;",'os_status table' => "CREATE TABLE `os_status` (
        `status_key` int(11) NOT NULL,
        `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'last update timestamp',
        `total_songs` int(11) UNSIGNED NOT NULL COMMENT 'Total number of actual songs in database'
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
    'os_status key' => "ALTER TABLE `os_status` ADD PRIMARY KEY (`status_key`);",
    'os_status auto increment' => "ALTER TABLE `os_status` MODIFY `status_key` int(11) NOT NULL AUTO_INCREMENT;",
    'os_users prep' => "DROP TABLE IF EXISTS `os_users`;",
    'os_users table' => "CREATE TABLE `os_users` (
      `user_key` int(11) UNSIGNED NOT NULL COMMENT 'User key',
      `user_name` varchar(40) NOT NULL COMMENT 'User Name',
      `user_full_name` varchar(255) DEFAULT NULL COMMENT 'Full name of user',
      `user_email` varchar(255) DEFAULT NULL COMMENT 'e-mail address for user (optional)',
      `user_pass` varchar(255) NOT NULL COMMENT 'User Password',
      `user_lock` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Whether or not user is locked out. 1=locked out',
      `user_pwdreset` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Whether to reset the password next time; 0 = don''t reset password; anything else= reset password',
      `user_admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Whether or not user is admin. 7=admin',
      `user_lib_admin` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'If user should get notifications of additions to library (0=no; 1=yes)'
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
    'os_users key' => "ALTER TABLE `os_users` ADD PRIMARY KEY (`user_key`), ADD UNIQUE KEY `user_name` (`user_name`);",
    'os_users auto increment' => "ALTER TABLE `os_users` MODIFY `user_key` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'User key';",
    'os_banned prep' => "DROP TABLE IF EXISTS `os_banned`;",
    'os_banned table' => "CREATE TABLE `os_banned` (
      `ban_key` int(11) NOT NULL,
      `ban_ip` varchar(256) NOT NULL,
      `ban_from` datetime NOT NULL,
      `ban_until` datetime NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
    'os_banned key' => "ALTER TABLE `os_banned` ADD PRIMARY KEY (`ban_key`), ADD KEY `banned_ips` (`ban_ip`);",
    'os_banned auto increment' => "ALTER TABLE `os_banned` MODIFY `ban_key` int(11) NOT NULL AUTO_INCREMENT;"
);

$err = FALSE;

/* execute inserts */
foreach ($tables as $name => $sql) {
    // $html .= ("<li>Attempting to process {$name} on database.</li>"); flush();
    if ($q = mysqli_query($osdb, $sql)) {
       // $html .= ("<li>Successfully processed {$name} on database.</p>"); flush();
    } else {
        $err = TRUE;
        $html .= ("<li class=\"text-danger\">Unable to process {$name} on database!</li>"); flush();
    }
} 

if (isset($admin) && is_array($admin)) {
/* insert admin user */
  $uarr = array(
    'user_name' => $admin['name'],
    'user_full_name' => "Main Admin",
    'user_pass' => password_hash($admin['pwd'], PASSWORD_DEFAULT),
    'user_email' => $admin['email'],
    'user_lock' => 0,
    'user_admin' => 7,
    'user_lib_admin' => 1
  );
  exQuery('INSERT','os_users',$uarr);
}


$html .= ("</ul>");
if ($err) {
    $html .= ("<p class=\"alert alert-danger\"><b><span style=\"color:red\">Unable to insert all tables to database. Please check the database to see what went wrong.");
} else {
    $html .= ("<p class=\"alert alert-success\"><i class=\"far fa-check\"></i> Empty database for OpenSong Library Display Site successfully created!</p>");
}

/* XSK885yTfAMBe9r# */
/* https://v2.ipcasonglib.com/setup/index.php?unlock-code=R4CYgVMYX9u7oA7 */
?>
