<?php
/**
 * OpenSong Library Display Site
 * pCloud Management Functions
 * 
 * version: 2.0
 * file version: 2.0.1
 * updated: 2021-06-18 | 08:21 UTC
 * 
 * Copyright © 2020 Hawke AI
 */

require_once("lib/pCloud/autoload.php");

/* update sequence 
1. Check for files in New Songs
2. If files in New Songs, download all of them
3. Iterate through files and see if <user2> = * or 0
4. If <user2> = * or 0, import song into database
5. Update <user2> to new song number
6. Upload file to English
7. Delete folder from New Songs
8. When complete delete all songs in newsongs folder
 */

/**
 * procSongs: looks up and downloads any new songs for processing
 * Returns: 'get' = array of file ids
 *          'put' = boolean denoting success
 * 
 * @var string  action to take ('get-all', 'get-list', 'get-song', 'put' or 'move')
 * @var string  Access token
 * @var string  Directory path containing songs to be processed
 * @var string  name ('put') or id ('get-song', 'move') of file to be uploaded
 *                    Leave empty to 'get' all songs
 */
function procSongs($action, $token, $song_loc, $file="") {
    global $logger;
    $locationid = 1;

    /* connect to pCloud */
	$pCloudApp = new pCloud\App();
	$pCloudApp->setAccessToken($token);
	$pCloudApp->setLocationId($locationid);

    /* instantiate objects */
    $pCloudFolder = new pCloud\Folder($pCloudApp);
    $pCloudFile = new pCloud\File($pCloudApp);

    /* get folder id */
    $folder_id = $pCloudFolder->search("{$song_loc}/*");
    if ($folder_id == "nodir") { return "pCloudPathBad"; }
    $folder_id = $folder_id->metadata->folderid;
    
    /* check for newsongs directory */
    if (!file_exists("newsongs") || !is_dir("newsongs")) {
        if (file_exists("newsongs") && !is_dir("newsongs")) {
            unlink("newsongs");
        }
        mkdir("newsongs");
    }

    switch ($action) {
        /* download all songs */
        case 'get-all':
        case 'get-list':
            $fids = array();
            $song = array();
            try {
                $songs = $pCloudFolder->getContent($folder_id);
            } catch (Exception $e) {
                $logger->error($e->getMessage());
                return json_encode(array("error" => "pCloudPathBad"));
            }
            if (count($songs) > 0) {
                foreach ($songs as $skey => $sobj) {
                    try {
                        if ($action == 'get-all') {
                            $pCloudFile->download($sobj->fileid, "./newsongs");
                        }
                        $fids[] = array ("name" => $sobj->name,
                                         "id" => $sobj->fileid,
                                         "modified" => strtotime($sobj->modified));
                    } catch (Exception $e) {
                        $logger->error($e->getMessage());
                    }
                }
            }
            return $fids;
            break;
        
        case 'get-song':
            $metaData = $pCloudFile->getInfo($file);
            $pCloudFile->download($file, "./newsongs");
            return array("name" => $metaData->metadata->name, "id" => $metaData->metadata->fileid,
            "modified" => strtotime($metaData->metadata->modified));
            break;

        /* upload completed song */
        case 'put':
            try {
                if (file_exists("./newsongs/{$file}")) {
                    $metaData = $pCloudFile->upload("./newsongs/{$file}", $folder_id);
                    return array('name'=> $metaData->metadata->name, 'id' => $metaData->metadata->fileid, 
                                 'modified' => strtotime($metaData->metadata->modified));                    
                } else {
                    return "nofile";
                }
            } catch (Exception $e) {
                return ($e->getMessage());
            }
            break;
        
        case 'move':
            try {
                $fileMetaData = $pCloudFile->move($file, $folder_id);
                return "success";
            } catch (Exception $e) {
                return $e->getMessage();
            }
            break;

        case 'del':
            try {
                $result = $pCloudFile->delete($file);
                return "success";
            } catch (Exception $e) {
                return $e->getMessage();
            }
            break;
    }
}

?>
