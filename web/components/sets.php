<?php
/**
 * OpenSong Library Display Site
 * User processing functions
 *
 * version: 2.0
 * file version: 2.0
 * updated: 2021-06-18 | 08:41 UTC
 * 
 * Copyright © 2020, 2021 Hawke AI
 */

if (!function_exists('fixApostrophe')) {
    /**
     * fixApostrophe: Doubles apostrophes to protect SQL queries
     * Returns: repaired string
     * 
     * @var string  string to apostrophize
     */
    function fixApostrophe($string) {
        $placeholder= '~';
        while (strpos($string,"'") !== FALSE) {
            $string = str_replace("'", $placeholder, $string);
        }
        while (strpos($string, $placeholder) !== FALSE) {
            $string = str_replace($placeholder, "''", $string);
        }
        return $string;
    }
}

/** 
 * loadSet: loads a given set from an XML file and parses it
 * Returns: array containing set data
 * 
 * @var     string  filename relative to ./newsongs
 */
function loadSet($f) {
    $out = array();

    /* load xml */
    if ($set_xml = @simplexml_load_file("./newsongs/{$f}")) {
        /* convert to array */  
        $set_xml = json_decode(json_encode($set_xml), TRUE);
        
        if (isset($set_xml['slide_groups']) && isset($set_xml['slide_groups']['slide_group'])) {
            foreach($set_xml['slide_groups']['slide_group'] as $s) {
                $sa = array('type' => $s['@attributes']['type'],
                            'name' => $s['@attributes']['name']);
                if ($sa['type'] == 'song') {
                    $sa['id'] = 0;
                    $sa['title'] = "";
                    $sa['ccli'] = "";
                    
                    $sq = exQuery('SELECT', 'os_songs', '*', "song_filename LIKE '" . fixApostrophe($sa['name']) . "' AND song_aka = 0");
                    if (is_array($sq)) {
                        $sa['id'] = intval($sq[0]['song_id']);
                        $sa['title'] = $sq[0]['song_title'];
                        $sa['ccli'] = $sq[0]['song_ccli'];
                    } 
                } elseif ($sa['type'] != 'style' && $sa['type'] != 'external') {
                    $sa['slides'] = count($s['slides']['slide']);
                }
                $out[] = $sa;
            }
        }
    } else {
        $out['error'] = "no file";
    }
    return $out;
}

/**
 * saveSet: saves a set and uploads it to OpenSong
 * Returns: success or error message
 * 
 * @var string  file name to save the set to
 * @var array   array containing data
 */
function saveSet($fn, $setinfo) {
    global $access_token;
    global $new_dir;
    global $proc_dir;
    global $sets_dir;
    global $logger;

    if (!is_array($setinfo)) { return 'noarray'; }

    $loc = array('new' => substr($new_dir, strrpos($new_dir, "/")+1),
    'procd' => substr($proc_dir, strrpos($proc_dir, "/")+1));

    $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<set name=\"$fn\">
<slide_groups>\n";
    foreach($setinfo as $set) {
        $xml .= "    <slide_group name=\"{$set['fn']}\" type=\"song\" presentation=\"\" path=\"{$loc[$set['songtype']]}/\"/>\n";
    }
    $xml .= "</slide_groups></set>\n";
    
    if ($f = @fopen("./newsongs/{$fn}", "w")) {
        fwrite($f, $xml);
        fclose($f);
    } else {
        $logger->error("Unable to write set to ./newsongs/{$fn}");
        return 'nowrite';
    }

    /* upload to remote */
    $s = procSongs('put', $access_token, $sets_dir, $fn);
    if (is_array($s)) {
        unlink("./newsongs/{$fn}");
        return 'success';
    } else {
        $logger->error("Unable to upload set {$fn}");
        return $s;        
    }
    
}
