<?php
/**
 * OpenSong Library Display Site
 * File processing functions
 *
 * version: 2.0
 * file version: 2.0
 * updated: 2021-06-18 | 08:41 UTC
 * 
 * Copyright © 2020, 2021 Hawke AI
 */

/**
 * fixApostrophe: Doubles apostrophes to protect SQL queries
 * Returns: repaired string
 * 
 * @var	string	string to apostrophize
 */
function fixApostrophe($string) {
	$placeholder= '~';
	while (strpos($string,"'") !== FALSE) {
		$string = str_replace("'", $placeholder, $string);
	}
	while (strpos($string, $placeholder) !== FALSE) {
		$string = str_replace($placeholder, "''", $string);
	}
	return $string;
}

/*
 * getNextSong: generates the next song id to load into the database
 * Returns: integer
 */
function getNextSong() {
	$q = exQuery('SELECT', 'os_songs', 'MAX(song_id) as last_song', 'song_aka = 0');
	$s = 0;
	if (is_array($q)) {
		$s = intval($q[0]['last_song']);
	}
	$s++;
	return $s;	
}

/**
 * insertAuthor: Checks if author exists in database, inserts if not there, returns key otherwise
 * Returns: auth_key for author
 * 
 * @var string	first name of author
 * @var string  last name of author
 */
function insertAuthor($f, $l) {
	global $logger; 

	/* check if author exists */
	$q = exQuery('SELECT', 'os_authors', 'auth_key', "`auth_first` LIKE '{$f}' AND `auth_last` LIKE '{$l}'");
	if (is_array($q)) {
		return $q[0]['auth_key'];
	} elseif ($q === FALSE) {
		try {
			exQuery('INSERT', 'os_authors', array('auth_first'=>$f, 'auth_last'=>$l));
			$logger->info("Inserted author {$f} {$l}");
		} catch (Exception $e) {
			$logger->error("Unable to insert author {$f} {$l}", $e->getMessage());
		}
		$q = exQuery('SELECT', 'os_authors', 'auth_key', "`auth_first` LIKE '{$f}' AND `auth_last` LIKE '{$l}'");
		if (is_array($q)) {
			return $q[0]['auth_key'];
		} else {
			$logger->error("Unable to process author {$f} {$i}");
			return FALSE;
		}
	}
}

/**
 * insertSong: Inserts passed song data into database 
 * Returns: boolean denoting success
 * 
 * @var	string	song file name
 * @var	string 	song xml data file
 * @var integer	unix epoch time modified (defaults to timestamp of now)
 */
function insertSong($file, $song, $modt=0) {
	global $logger;
	if ($modt == 0) { $modt = time(); } /* if no file mod time given, use now as timestamp */

	/* skip song if we have an empty or non-integer user2 field */
	if (intval($song['user2']) == 0) { return FALSE; }
	
	/* remove all records concerning the song */
	exQuery('DELETE','os_sa_xlink', '', "`song_id` = {$song['user2']}");
	exQuery('DELETE','os_songs', '', "`song_id` = {$song['user2']}");
	exQuery('DELETE','os_lyrics', '', "`song_id` = {$song['user2']}");

	/* process authors */
	$authors = parseAuthor($song['author']);
	foreach ($authors as $a) {
		$akey = insertAuthor($a['first'], fixApostrophe($a['last']));
		if ($akey !== FALSE) {
			$q = exQuery('INSERT', 'os_sa_xlink', array('auth_key' => $akey, 'song_id' => $song['user2']));
		}
	}

	/* insert official song */
	$qarr = array('song_id' => $song['user2'], 
				  'song_title' => $song['title'],
				  'song_sort' => normalizeTitles($song['title']),
				  'song_aka' => 0,
				  'song_ccli' => $song['ccli'],
				  'song_tags' => $song['user3'],
				  'song_filename' => fixApostrophe($file),
				  'song_modified' => $modt);
	
	$q = exQuery('INSERT', 'os_songs', $qarr);

	/* process alternate titles */
	if (strlen($song['aka']) > 0) {
		$aka = parseAka($song['aka']);
		foreach ($aka as $a) {
			$qarr = array('song_id' => $song['user2'], 
			'song_title' => $a,
			'song_sort' => normalizeTitles($a),
			'song_aka' => 1,
			'song_ccli' => $song['ccli'],
			'song_tags' => $song['user3'],
			'song_filename' => fixApostrophe($file),
			'song_modified' => $modt);

			$q = exQuery('INSERT', 'os_songs', $qarr);
		}
	}

	/* process extra information (lyrics) */
	$qarr = array('song_id' => $song['user2'],
				  'lyrics_lyrics' => fixApostrophe($song['lyrics']),
				  'lyrics_copyright' => fixApostrophe($song['copyright']),
				  'lyrics_presentation' => $song['presentation']);
	$q = exQuery('INSERT', 'os_lyrics', $qarr);

	$logger->info("Added “{$song['title']}” with song_id {$song['user2']}");
	return TRUE;
}


/**
 * normalizeTitles: Normalizes the titles by removing "The", "A" and "An" from
 *              the beginning and appending it to the end.
 * Returns: normalized title as string
 * 
 * @var string	text to normalize
 */
function normalizeTitles($title) {
	$prefixes = array("A", "An", "The");
	
	$title_array = explode(" ",$title);
	
	foreach ($prefixes as $prefix) {
		if ($title_array[0] == $prefix) {
			array_shift($title_array);
			$title_array[(count($title_array)-1)] = $title_array[(count($title_array)-1)] . ",";
			array_push($title_array,$prefix);
		}
	}
	return implode(" ",$title_array);
}

/**
 * parseAka: Parses string with multiple alternate titles and returns them as an array.
 * Returns: array containing parsed field contents
 * 
 * @var string	contents of aka field
 */
function parseAka($akaList) {
	$akaArr = explode(";", $akaList);
	$out = array();
	foreach ($akaArr as $a) {
		$out[] = trim($a);
	}
	return $out;
}

/**
 * parseAuthor: Parses string with multiple authors and retursn them as a multi-dimensional array.
 * Returns: array containing parsed field contents
 * 
 * @var string	contents of authors field
 */
function parseAuthor($authList) {
	/* check for suffixes and change commas to semicolons */
	$suffixes = array (", Jr" => '*Jr', ",Jr" => '*Jr', ", Sr" => '*Sr', ",Sr" => "*Sr", 
	                   " II" => "+II", " IV" => "+IV", " V" => "+V", " IX" => "+IX", " X" => "*X", "," => ';');
	foreach ($suffixes as $k => $i) {
		$authList = str_replace($k, $i, $authList);
	}
	$authArray = explode(";", $authList);
	
	/* process individual authors */
	$out = array();
	foreach ($authArray as $auth) {
		$aArr = explode(" ", trim($auth));
		$out[] = array("last" => str_replace('*',', ', str_replace("+"," ", array_pop($aArr))),
					   "first" => implode(" ", $aArr));
	}
	
	return $out;
}

/**
 * parseSong: Opens, reads and parses song data xml file, returning designated fields (must follow OpenSong naming conventions)
 * Returns: array containing designated fields or boolean FALSE if nothing found
 * 
 * @var	string	name of song file to parse
 * @var string	comma separated list of fields to return (default: title,author,ccli,aka,user2,lyrics,presentation,copyright)
 */
function parseSong($song_file, $fields="title,author,ccli,aka,user2,user3,lyrics,presentation,copyright") {	
	/* initialize array */
	$song_data = array();
	
	/* default is that we don't have XML for a song */
	$song_xml = FALSE;
	
	/* make sure that we get the user2 field */
	if ((strpos($fields,'user2') === FALSE)) {
		$fields .= ",user2";
	}
	
	/* get fields */
	$field_array = explode(",", $fields);
	
	/* make sure we're getting an xml file */
		/* get xml data */
	$song_xml = @simplexml_load_file($song_file);
	
	if ($song_xml) {
		foreach($field_array as $field_item) {
			foreach($song_xml->children() as $child) {
				if ($child->getName() == $field_item) {
					$field_data = trim(fixApostrophe((string)$child));
					$song_data[$field_item] = $field_data;
				}
			}
		}
	}

	if ($song_data['user2'] == "*" || $song_data['user2'] == "0") {
		$song_data['user2'] = getNextSong();
	} 

	if (count($song_data) > 0) {
		return $song_data;
	} else {
		return FALSE;
	}
} 

/** 
 * timeToUpdate: checks to see if it's time to update the database
 * Returns: boolean noting if it is time or not
 */
function timeToUpdate() {
	$lc = 0;
	$now = intval(date("U"));
	if (file_exists("config/last_check.txt")) {
		$lc = intval(@file_get_contents("config/last_check.txt"));
	}
	$interval = $now - $lc;
	/* only trigger every 24 hours or if we haven't checked yet */
	if ($interval > 86400 || $lc == 0) {             
		$f = @fopen("config/last_check.txt", "w");
		fwrite($f, $now);
		fclose($f);
		return TRUE;
	} else {
		return FALSE;
	}
}

/**
 * updateStats: updates the statistics for the site (song count, last update)
 * Returns: boolean denoting success
 */
function updateStats() {
	$q = exQuery('SELECT', 'os_songs', 'COUNT(`song_id`) as total_songs', '`song_aka` = 0');
	return exQuery('INSERT', 'os_status', array('total_songs' => $q[0]['total_songs']));
}

/** 
 * writeSongIdToXML: writes the song XML to the loacal disk
 * Returns: boolean denoting result
 * 
 * @var integer	song Id
 * @var string	file name
 * @var string	directory to write to (defaults to ./newsongs); no closing slash!
 */
function writeSongIdToXML($sid, $fn, $dir="./newsongs") {
	if ($sid < 1 || intval($sid) == 0) { return FALSE; } /* we don't have an integer as the song Id */
	$xml = @file_get_contents("{$dir}/{$fn}");
	if ($xml !== FALSE) {
		$xml = str_replace("<user2>*<", "<user2>{$sid}<", $xml);
		$xml = str_replace("<user2>0<", "<user2>{$sid}<", $xml);
		if ($f = @fopen("{$dir}/{$fn}",'w')) {
			fwrite($f, $xml);
			fclose($f);
			return TRUE;
		}
	} else {
		return FALSE;
	}
}

?>
