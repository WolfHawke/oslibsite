<?php
/**
 * OpenSong Library Display Site
 * Communication (e-mail) functions
 * 
 * version: 2.0
 * file version: 2.0
 * updated: 2021-06-18 | 08:40 UTC
 * 
 * Copyright © 2020 Hawke AI
 */


/**
 * mailIt: function to email information; e-mail address is pulled from $_SESSION
 * Returns: message denoting success
 * 
 * @var string  key to validate mail function
 * @var array   user information
 * @var string  message subject
 * @var string  message body
 */
function mailIt($mk, $uinfo, $s, $msg) {
    global $_SERVER;
    global $app_title;
    global $MAILKEY;

    $r = array();

    if (isset($MAILKEY) && $mk == $MAILKEY) {
        if (isset($uinfo['ufull']) && isset($uinfo['umail'])) {
            $to = "{$uinfo['ufull']} <{$uinfo['umail']}>";
            $h = "From: {$app_title} <noreply@{$_SERVER['HTTP_HOST']}>";
            if (mail($to, $s, $msg, $h)) {
                $r['success']  = TRUE;
            } else {
                $r['error'] = 'sending';
            }
        } else {
            $r['error'] = 'receiver';
        }
    } else {
        $r['error'] = 'validation';
    }
    return $r;
}

?>
