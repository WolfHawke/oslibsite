<?php
/**
 * OpenSong Library Display Site
 * User processing functions
 *
 * version: 2.0
 * file version: 2.0
 * updated: 2021-06-18 | 08:41 UTC
 * 
 * Copyright © 2020, 2021 Hawke AI
 */

/**
 * addSong: saves song data to a file and returns the name where it was saved
 * Returns: string 
 * 
 * @var array   contains the various fields
 */
function addSong($song_data) {
    global $logger;
    global $_SESSION;

    if (!is_array($song_data)) { return array('error' => 'bad data'); }
    /* check for the fields we need */
    if (!isset($song_data['title'])) { return array("error" => "no title"); }
    if (!isset($song_data['authors'])) { $song_data['authors'] == ''; }
    if (!isset($song_data['ccli'])) { $song_data['ccli'] = ''; }
    if ($song_data['ccli'] == '' && $song_data['authors'] == '') { return array('error' => 'no check possible'); }
    if (!isset($song_data['lyrics'])) { $song_data['lyrics'] = ''; }
    if (!isset($song_data['presOrder'])) { $song_data['presOrder'] = ''; }
    if (!isset($song_data['source'])) { $song_data['source'] = ''; }
    if (!isset($song_data['copyright'])) { $song_data['copyright'] = ''; }

    $xml_tpl = '<?xml version="1.0" encoding="UTF-8"?>
    <song>
      <title>' . strip_tags($song_data['title']) . '</title>
      <lyrics>' . strip_tags($song_data['lyrics']) . '</lyrics>
      <author>' . strip_tags($song_data['authors']) . '</author>
      <copyright>' . strip_tags($song_data['copyright']) . '</copyright>
      <hymn_number></hymn_number>
      <presentation>' . strip_tags($song_data['presOrder']) . '</presentation>
      <ccli>' . strip_tags($song_data['ccli']) . '</ccli>
      <capo print="false"></capo>
      <key></key>
      <aka></aka>
      <key_line></key_line>
      <user1>' . strip_tags($song_data['source']) . '</user1>
      <user2></user2>
      <user3></user3>
      <theme></theme>
      <linked_songs/>
      <tempo></tempo>
      <time_sig></time_sig>
      <backgrounds resize="screen" keep_aspect="true" link="false" background_as_text="false"/>
    </song>
';
    /* save file */
    $fn = stripCharsForFileName($song_data['title']);

    /* check to see if we already have a file with that name in the database */
    $dup = (is_array(exQuery('SELECT', 'os_songs', '*', "song_filename LIKE '{$fn}'"))) ? TRUE : FALSE;

    if ($f = fopen("./newsongs/$fn", 'w')){
        fwrite($f, $xml_tpl);
        fclose($f);
    } else {
        return FALSE;
    }
    
    /* upload file to pCloud */
    global $access_token;
    global $new_dir;
    global $_SESSION;

    if (is_array(procSongs('put', $access_token, $new_dir, $fn))) {
        unlink("./newsongs/$fn");
        $logger->info("Song \"$fn\" added by {$_SESSION['user']}.");
        return array('filename' => $fn, 'duplicate' => $dup);
    } else {
        return FALSE;
    }
}


/** 
 * checkKey: compares the passed key to the session key
 * Returns: boolean
 * 
 * @var string  passed key
 */
function checkKey($k) {
    if (isset($_SESSION['key']) && $k == $_SESSION['key']) {
        return true;
    } else {
        return false;
    }
}

/** 
 * checkPwd: checks to see if the password is valid
 * Returns: boolean denoting success
 * 
 * @var string  username to check
 * @var string  password to check
 */
function checkPwd($u, $p) {
    /* retrieve pwd from db */
    $q = exQuery('SELECT', 'os_users', 'user_pass', "user_name LIKE '{$u}'");
    if (is_array($q)){
        return password_verify($p, $q[0]['user_pass']);
    } else {
        return FALSE;
    }
}

/** 
 * genKey: generates a new key and assigns it to the session key
 * Returns: string containing new key
 */
function genKey() {
    $_SESSION['key'] = randomId(32);
    return $_SESSION['key'];
}

/** 
 * getAllUsers: retrieves all users from database and returns them as JSON Object
 * Returns: string in JSON
 */
function getAllUsers() {
    global $_SESSION;
    /* check for admin rights */
    if ($_SESSION['level'] != 7) { return FALSE; }
    $out = array();
    $q = exQuery('SELECT','os_users','*','','user_name ASC');
    if (is_array($q)) {
        foreach ($q as $i) {
            $out[] = array(
                'id' => $i['user_key'],
                'user' => $i['user_name'],
                'userfull' => $i['user_full_name'],
                'email' => $i['user_email'],
                'lock' => $i['user_lock'],
                'level' => ($i['user_admin'] == 7) ? "mgr" : "normal",
                'lib' => $i['user_lib_admin'],
            );
        }
    } else {
        $out['error'] = "no user data in database";
    }
    return json_encode($out);
}

/** 
 * sanitizeKeys: removes numeric keys from an array that has both numeric and non-numeric keys
 * Returns: array
 * 
 * @var array   array with mixed numeric and textual keys
 */
function sanitizeKeys($a) {
    $out = array();
    foreach ($a as $k => $i) {
        if (!is_numeric($k)) {
            $out[$k] = $i;
        }
    }
    return $out;
}

/**
 * sendAddSong: sends an email to the library admins confirming song addition
 * Returns: boolean denoting success
 * 
 * @var string  song title
 * @var array   song file name and file duplicate status
 */
function sendAddSong($title, $fn) {
    global $LANGUAGE;
    global $MAILKEY;
    global $app_title;
    global $new_dir;

    /* get next Sunday's date */
    $today = date('Y-m-d');
    $wday = intval(date('w', strtotime($today)));
    $wday = 7 - $wday;
    $sun = date('Y-m-d', strtotime("{$today} + {$wday} days"));

    /* generate Subject and Message */
    $sub = str_replace('%sitename%', $app_title, $LANGUAGE['emails']['add_song_subject']);
    $msg = str_replace('%userfullname%', $_SESSION['ufull'], $LANGUAGE['emails']['add_song_text']);
    $msg = str_replace('%username%', $_SESSION['user'], $msg);
    $msg = str_replace('%songtitle%', $title, $msg);
    $msg = str_replace('%songfolder%', substr($new_dir, (strrpos($new_dir, "/") + 1)), $msg);
    $msg = str_replace('%songfile%', $fn['filename'], $msg);
    $msg = str_replace('%date%', $sun, $msg);
    if ($fn['duplicate']) { 
        /* add a new-line character at the end of this line for safety. */
        $msg = str_replace('%duplicate%', $LANGUAGE['emails']['add_song_duplicate'] . "\n", $msg);
    } else {
        $msg = str_replace('%duplicate%', '', $msg);
    }
    
    /* get library admins */
    $libmin = exQuery('SELECT', 'os_users', 'user_full_name, user_email', 'user_lib_admin = 1');
    if (is_array($libmin)) {
        foreach ($libmin as $m) {
            mailIt($MAILKEY, array('ufull' => $m['user_full_name'], 'umail' => 'user_email'), $sub, $msg);
            $MAILKEY = randomId();
        }
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * stripCharsForFileName: strips all special characters except _ - . ( ) from a string
 * Returns: string
 * 
 * @var string  to strip
 */
function stripCharsForFileName($s) {
    $toStrip = array('`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '_', '{', '}', '[', ']', ',', ':', ';', "'", '"', '\\', '|', '<', '>', '/', '?', '“', '”', '’', '‘');
    
    foreach($toStrip as $c) {
        $s = str_replace($c, '', $s);
    }
    
    return $s;
}

/** 
 * updateUser: updates user information
 * Returns: boolean denoting success
 * 
 * @var string  userid to modify
 * @var string  "pwd", "name", "lock", "admin"
 * @var string  data to update to
 */
function updateUser($uid, $item, $d) {
    switch ($item) {
        case "pwd":
            $f = 'user_pass';
            $d = password_hash($d, PASSWORD_DEFAULT);
            break;
        case "name":
            $f = 'user_full_name';
            break;
        case 'email':
            $f = 'user_email';
            break;
        case 'lock':
            $f = 'user_lock';
            $d = ($d === FALSE || $d == 0) ? 0 : 1;
            break;
        case 'admin':
            $f = 'user_admin';
            $d = ($d === TRUE || $d == 7) ? 7 : 0;
            break;
        case 'pwdLock':
            $f = "user_pwdreset";
            $d = ($d === TRUE) ? 1 : 0;
    }
    return exQuery('UPDATE', 'os_users', array($f => $d), "user_key = {$uid}");
}

/* user management functions */
if (isset($_SESSION['user']) && $_SESSION['level'] == 7) {
    /**
     * resetPassword: resets a given user's password
     * Returns: array denoting success or error
     * 
     * @var string  user name
     */
    function resetPassword($u) {
        global $MAILKEY; 
        global $LANGUAGE;
        global $app_title;
        global $logger;

        $r = array();
        /* generate password */
        $p = randomId(10);
        /* reset password in database */
        $uobj = exQuery('SELECT', 'os_users', '*', "user_name LIKE '{$u}'");
        if (is_array($uobj)) {
            $uobj = $uobj[0];
            if (updateUser($uobj['user_key'], "pwd", $p) && updateUser($uobj['user_key'], "pwdLock", TRUE)) {
                $r['success'] = array('key' => 0, 'tmppwd' => $p, 'uid' => $u, 'mail' => FALSE);
                /* email password */
                $uinfo = array('ufull' => $uobj['user_full_name'], 'umail' => $uobj['user_email']);
                if ($uinfo['umail'] != "") {
                    $s = str_replace('%sitename%', $app_title, $LANGUAGE['emails']['pwd_reset_subject']);
                    $msg = str_replace('%sitename%', $app_title, $LANGUAGE['emails']['pwd_reset_text']);
                    $msg = str_replace('%user%', $uinfo['ufull'], $msg);
                    $msg = str_replace('%tmppwd%', $p, $msg);
                    $m = mailIt($MAILKEY, $uinfo, $s, $msg);
                    if (isset($m['success'])) {
                        $r['success']['mail'] = TRUE;
                    } else {
                        $logger->error("Unable to send e-mail to {$uinfo['umail']} because of a {$m['error']} error");
                    }
                }
                $logger->info("Password reset for {$u}");
            } else {
                $r['error'] = 'nochange';
            }
        } else {
            $r['error'] = 'nouser';
        }
        /* return result */
        return $r;
    }

    /**
     * userAddEdit: adds or updates a user
     * Returns: error or success message in JSON
     * 
     * @var string  'insert' or 'update' (default)
     * @var array   user data in format of database
     */
    function userAddEdit($action, $udata) {
        global $LANGUAGE;
        global $MAILKEY;
        global $logger;
        global $app_title;
        $result = array();

        /* make sure we have proper data */
        if (!is_array($udata)) { return json_encode(array('error' => 'invalid data')); }

        /* set action */
        if ($action == 'insert' || $action == 'update') {
            $go = strtoupper($action);
            
            /* validate user name */
            $regex = '/^[A-Za-z0-9_\.\-]+$/';
            if (preg_match($regex, $udata['user_name'])) {
                /* validate e-mail address */
                $regex = '/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i';
                if (preg_match($regex, $udata['user_email']) || $udata['user_email'] == '') {
                    /* sanitize user full name */
                    $udata['user_full_name'] = strip_tags($udata['user_full_name']);
                    /* if insert create temporary password, add password reset */
                    if ($action == 'insert') {
                        $pwd = randomId(12);
                        $udata['user_pass'] = password_hash($pwd, PASSWORD_DEFAULT);
                        $udata['user_pwdreset'] = 1;
                        $where = '';
                    } elseif ($action == 'update') {
                        $where = "user_name LIKE '{$udata['user_name']}'";
                    }
                   
                    /* execute insert or update */
                    $q = exQuery($go, 'os_users', $udata, $where);
                    if ($action == "insert") {
                        /* if insert and e-mail address, send password */
                        $result = array('success' => array('action'=>'add', 'seckey' => genKey(), 'user' => $udata['user_name'], 'mail'=>FALSE, 'challenge'=>$pwd));
                        if ($udata['user_email'] != '') {
                            $msg = str_replace('%sitename%', $app_title, $LANGUAGE['emails']['usr_creation_text']);
                            $msg = str_replace('%user%', $udata['user_full_name'], $msg);
                            /* all this stuff only works if we have https, anyway, so we assume it's there */
                            $msg = str_replace('%siteurl%', 'https://' . $_SERVER['HTTP_HOST'], $msg);
                            $msg = str_replace('%uname%', $udata['user_name'], $msg);
                            $msg = str_replace('%tmppwd%', $pwd, $msg);
                            $MAILKEY = randomId();
                            $m = mailIt($MAILKEY, array('umail' => $udata['user_email'], 'ufull' => $udata['user_full_name']),
                                        str_replace('%sitename%', $app_title, $LANGUAGE['emails']['usr_creation_subject']),
                                        $msg);
                            if (is_array($m) && array_key_exists('success', $m)) { $result['success']['mail'] = TRUE; }        
                        } 
                    } else {
                        $result = array('success' => array('action'=>'update', 'user' => $udata['user_name'], 'seckey' => genKey()));
                    }
                    $log_msg = "{$udata['user_name']} successfully {$action}";
                    $log_msg .= ($action == "insert") ? "e" : "";
                    $log_msg .= "d into database";
                    $logger->info($log_msg);
                } else {
                    $result = array("error" => "invalid email");
                }
            } else {
                $result = array("error" => "invalid username");
            }
        } else {
             $result = array('error' => 'invalid action');
        }
        /* return result */
        if (array_key_exists('error', $result)) {
            $logger->error("Unable to insert or update user: {$result['error']}");
        }
        return json_encode($result);
    }

    /**
     * userDelete: deletes a user from the database
     * Returns: success or error message
     * 
     * @var string  user name to delte
     */
    function userDelete($u) {
        global $logger;

        $d = exQuery('DELETE', 'os_users', '', "user_name LIKE '{$u}'");
        if ($d === TRUE) {
            $logger->info("Deleted user {$u}");
            return json_encode(array("success" => genKey()));
        } else {
            $logger->error("Could not delete user {$u}");
            return json_encode(array("error" => "Could not delete {$u}"));
        }
    }
}
