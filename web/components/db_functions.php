<?php
/* 
 * OpenSong Library Display Site
 * Database Access Functions
 *
 * version: 2.0
 * file version: 2.0.2
 * updated: 2021-06-18 | 08:40 UTC
 * 
 * Copyright © 2011, 2020, 2021 Hawke AI. All Rights Reserved
 */

/** 
 * countTables: checks to see how many tables there are in a database
 * Returns: nothing
 */
function countTables() {
	global $osdb;
	$dbResults = mysqli_query($osdb, "SHOW TABLES;");
 	return $dbResults->num_rows;
}

/**
 * dbConnect: intializes the database link
 * Returns: either a link to the database or FALSE
 * 
 * @var string	database host server
 * @var string	database user id
 * @var	string	database user password plaintext
 * @var string	database name
 */
function dbConnect($dbHost, $dbUser, $dbPass, $dbBase) {	
	if ($dbLink = @mysqli_connect($dbHost, $dbUser, $dbPass, $dbBase)) {
		if (mysqli_set_charset($dbLink,'utf8') && mysqli_select_db($dbLink,$dbBase)) {
			return($dbLink); 
		}
	} else {
		return(FALSE);
	}
}

/**
 * exQuery: Creates the SQL string and, by default, executes it.
 * Returns: One of four things:
 *          1. an array containing the result from the database
 *          2. boolean indicating success or failure
 *          3. the number of rows returned
 *          4. the well-formed SQL string
 * 
 * @var string	command to execute with query: 'DELETE', 'INSERT', 'SELECT', 'UPDATE'
 * @var string	table name to operate on
 * @var string  1. in case of 'INSERT' or 'UPDATE' an array of column => data
 *  		    2. in case of 'SELECT' a string of column names
 *			    3. in case of 'DELETE' an empty string
 * @var string	the where statement content
 * @var string	the content of the 'ORDER BY' statement
 * @var boolean determine whether to return a the number of rows rather than the data. Only works with SELECT;
 * @var boolean makes the funciton return only the SQL string without executing the query.
 */
function exQuery($command, $table, $fields, $where='', $sort='', 
				 $numrows=false, $retSQLstring=false) {
	
	// get DB connection
	global $osdb;

	/* make sure that db connection is a mysqli object */
	if (!is_object($osdb) || get_class($osdb) != "mysqli") {
		throw new Exception ("No mysqli object passed: " . print_r($osdb, TRUE));
		return false;
	}
	
	// form SQL string
	switch($command) {
		case 'DELETE':
			$query = "DELETE FROM {$table}";
			if ($where == '') {
				throw new Exception ("Unable to execute DELETE: no WHERE statement was defined.");
			}
			break;
		
		case 'INSERT':
			if (! is_array($fields)) {
				throw new Exception ("The fields to be inserted were not passed as an array. Unable to execute INSERT.");
			}			
			$query = "INSERT INTO {$table} SET ";
			foreach ($fields as $column => $data) {
				$query .= "`{$column}` = '{$data}', ";
			}
			$query = rtrim($query, ', ');
			break;
			
		case 'SELECT':
			if (is_array($fields)) {
				throw new Exception("The fields passed were an array, not a string. Unable to execute SELECT.");
			}
			$query = "SELECT $fields FROM {$table}";
			break;
			
		case 'UPDATE':
			if (! is_array($fields)) {
				throw new Exception ("The fields to be inserted were not passed as an array. Unable to execute UPDATE.");
			} elseif ($where == '') {
				throw new Exception ("Unable to execute UPDATE: no WHERE statement was defined.");
			}
			$query = "UPDATE {$table} SET ";
			foreach ($fields as $column => $data) {
				$query .= "`{$column}` = '{$data}', ";
			}
			$query = rtrim($query, ', ');
			break;
	}
	
	// add where statement
	if ($where != '') {
		$query .= " WHERE {$where}";
	}
	
	// add sort statement
	if ($sort != '') {
		$query .= " ORDER BY {$sort}";
	}
	
	// close query
	$query .= ";";
	
	// check to see if it must be returned
	if ($retSQLstring) {
		return $query;
	} else {
	// execute query
		if($dbResults = mysqli_query($osdb,$query)) {
			if ($command == 'SELECT') { // return data for SELECT
				if ($numrows) { // check to see if only rows need to be returned
					$allData = mysqli_affected_rows($osdb);
				} else {
					if(mysqli_affected_rows($osdb) > 0) { // otherwise return data
						while($oneRow = mysqli_fetch_array($dbResults)) {
							$allData[] = $oneRow;
						}
					} else {
						$allData = FALSE;
					}
				}
			} else { // return data from other commands
				$allData = TRUE;
			}
		} else {
			throw new Exception ("Unable to execute query: <pre>{$query}</pre>");
			$allData = FALSE;
		}
	}
	return $allData;
}

/**  
 * emptyTable: Empties (truncates) all the records in a given table
 * Returns: boolean denoting success
 * 
 * @var string	name of table to truncate
 */
function emptyTable($table="") {
	/* get DB connection */
	global $osdb;
	
	if ($table != "") {
		$query = "TRUNCATE `{$table}`;";
		if ($dbResults = mysqli_query($osdb,$query)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

$osdb = dbConnect($db_host, $db_user, $db_pass, $db_base);

if (!function_exists("randomId")) {
	/**
	 * randomId: generates an alphanumeric random ID of a specified length
	 * Returns: a string containing alphanumeric random ID
	 *
	 * @since    0.8
	 * @var     int     how many characters the random Id should be in length; defaults to 8
	 * @var     string  "any","decimal","alphabet","hexadecimal"; defaults to any
	 */
	function randomId($length=8,$type='any') {
		$seed = array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G",
					"H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X",
					"Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o",
					"p","q","r","s","t","u","v","w","x","y","z");
		$id = "";
		$start = 0; $end = count($seed)-1;
		if ($type == 'decimal') {
			$end = 9;
		} elseif ($type == 'hexadecimal') {
			$end = 15;
		} elseif ($type == 'alphabet') {
			$start = 10;
		}
		for ($i=0;$i<$length;$i++) {
			$id .= $seed[rand($start,$end)];
		}
		return $id;
}
}
?>
