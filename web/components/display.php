<?php
/**
 * OpenSong Library Display Site
 * Front End Data Display Functions
 *
 * version: 2.0
 * file version: 2.0
 * updated: 2021-06-18 | 08:40 UTC
 * 
 * Copyright © 2020, 2021 Hawke AI
 */

/** 
 * banIP: bans a given IP address for 72 hours
 * Returns: boolean denoting success
 * 
 * @var string	I.P. address to ban; defaults to current IP address.
 */
function banIP($ip=0) {
    /* if no IP is passed, ban the current IP */
    if ($ip == 0) { $ip = $_SERVER['REMOTE_ADDR']; }
    $interval = 72; /* hours */
    $now = time();
    $end_ban = $now + ($interval*60*60);
    /* check to see if IP is already banned */

    /* execute insert */
    exQuery('INSERT','os_banned', array("ban_ip" => $ip, "ban_from" => date('Y-m-d H:i:s', $now), 
                                        "ban_until" => date('Y-m-d H:i:s', $end_ban)));

    /* remove all items that are older than 6 days */
    $six_days = $now + (6 * 24 * 60 * 60);
    exQuery('DELETE', 'os_banned', '', "ban_until < '{$six_days}'");

}

/** 
 * checkIPBan: checks to see if the IP is banned.
 * Returns: boolean denoting whether banned or not
 * 
 * @var string	IP to check; defaults to current IP
 */
function checkIPBan($ip=0) {
	/* if no IP is passed, ban the current IP */
	if ($ip == 0) { $ip = $_SERVER['REMOTE_ADDR']; }

	$now = date('Y-m-d H:i:s');

	$q = exQuery('SELECT', 'os_banned', '*', "(ban_ip LIKE '$ip') AND (ban_until > '{$now}')");

	if (is_array($q)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

/**
 * doSeach: executes a search for author or title
 * Returns: JSON object with results
 * 
 * @var string	terms to search for
 */
function doSearch($terms) {
	if (!is_string($terms) || $terms == "") { return json_encode(array("results"=>-1)); }
	$out = array('results' => 0, 'terms' => $terms, 'songs' => array(), 'authors' => array());
	$terms = explode(" ", $terms);

	/* search song titles */
	$where = '';
	foreach ($terms as $t) {
		$where .= "`song_title` LIKE '%{$t}%' OR";
	}
	$where = substr($where,0,-3);
	$q = exQuery('SELECT', 'os_songs', '*', $where, 'song_sort ASC');
	if (is_array($q)) {
		foreach($q as $k => $i) {
			$out['songs'][] = array(
				'id' => $i['song_id'],
                'title' => $i['song_title'],
				'ccli' => $i['song_ccli'],
				'aka' => $i['song_aka'],
				'filename' => $i['song_filename']
			);
			$last = count($out['songs']) - 1;
			if ($i['song_aka'] == 1) {
				$qo = exQuery('SELECT', 'os_songs', 'song_title', "`song_id` = {$i['song_id']} AND `song_aka` = 0");
				$out['songs'][$last]['official'] = $qo[0]['song_title'];
			} else {
				$qa = exQuery('SELECT', 'os_sa_xlink', '*', "`song_id` = {$i['song_id']}");
				if (is_array($qa)) {
					$out['songs'][$last]['authors'] = getAuthorList('crosslink', $qa)[0];
				}
			}
		$out['results']++;
		}
	}

	/* authors */
	$where = '';
	foreach ($terms as $t) {
		$where .= "`auth_first` LIKE '%{$t}%' OR `auth_last` LIKE '%{$t}%' OR";
	}
	$where = substr($where,0,-3);
	$q = exQuery('SELECT', 'os_authors', '*', $where, 'auth_last ASC, auth_first ASC');
	if (is_array($q)) {
		foreach ($q as $k => $i) {
			$out['authors'][] = array('first' => $i['auth_first'], 
									  'last' => $i['auth_last'], 
									  'songs' => array());
			$last = count($out['authors']) - 1;
			/* get song title crosslinks */
			$qs = exQuery('SELECT', 'os_sa_xlink', '*', "`auth_key` = {$i['auth_key']}");
			if (is_array($qs)) {
				foreach ($qs as $ks => $is) {
					$qss = exQuery('SELECT', 'os_songs', '*', "`song_id` = {$is['song_id']} AND `song_aka` = 0");
					if (is_array($qss)){
						$out['authors'][$last]['songs'][] = array(
							'id' => $qss[0]['song_id'],
							'title' => $qss[0]['song_title'],
							'ccli' => $qss[0]['song_ccli'],
							'filename' => $qss[0]['song_filename']
						);
					}
				}
			}
		$out['results']++;
		}
	}
	return json_encode($out);
}

/**
 * getAuthorList: pulls author contents from database
 * Returns: json object
 * 
 * @var	string	'last' (default), 'first', 'crosslink'
 * @var array 	crosslinks to process
 */
function getAuthorList($sort='last', $xlinks="") {
	global $logger;
	global $_SESSION;

    $out = array();
    $where = "";
    switch ($sort) {
        case 'crosslink':
            if (is_array($xlinks)) {
                foreach ($xlinks as $k => $i) {
                    $where .= "auth_key = {$i['auth_key']} OR ";
                }
                $where = substr($where, 0, -4);
            } else {
                $logger->error("No crosslink array passed for author list.");
                return FALSE;
            }
            $order_by = "auth_last ASC";
            break;
        case 'first':
            $order_by = "auth_first ASC";
            break;
        case 'last':
            $order_by = "auth_last ASC";
    }
    $q = exQuery('SELECT', 'os_authors', '*', $where, $order_by);
    if (is_array($q)) {
        foreach ($q as $k => $i) {
            $a = array("first" => $i['auth_first'], "last" => $i['auth_last']);
            if ($sort != 'crosslink') {
                $a['songs'] = array();
                $ql = exQuery('SELECT', 'os_sa_xlink', 'song_id', "`auth_key` = {$i['auth_key']}");
                if (is_array($ql)) {
					$where = '';
                    foreach ($ql as $lk => $li) {
						$where .= "`song_id` = {$li['song_id']} OR ";
					}
					$where = substr($where, 0, -4);
                    $where = "({$where}) AND `song_aka` = 0";
                    $qs = exQuery('SELECT', 'os_songs', '*', $where, '`song_sort` ASC');
                    if (is_array($qs)) {
                        foreach ($qs as $sk => $si) {
                            $a['songs'][] = array('id' => $si['song_id'], 
                                                  'title' => $si['song_title'], 
                                                  'ccli' => $si['song_ccli'], 
												  'sort' => $si['song_sort'],
												  'filename' => $si['song_filename']);
                        }
                    } else {
                        $logger->error("No songs assosciated with the crosslinks for author {$i['auth_key']}");
                    }
                } else {
                    $logger->error("No crosslinks found for author at {$i['auth_key']}");
                }
            }
            $out[] = $a;
        }
    } else {
        $logger->error("No authors found in database.");
    }

	if ($sort == 'crosslink'){
		return array($out);
	} else {
		return json_encode(array("type"=>"authors", "sort"=>$sort, "data"=>$out, "update"=>$_SESSION['timeToUpdate'], ));
	}
}

/**
 * getSongDetails: returns all details for a song from the database
 * Returns: JSON object
 * 
 * @var	integer	song_id to return
 */
function getSongDetails($sid=0) {
	global $logger;
	global $_SESSION;
    if ($sid == 0) { return FALSE; }
    $out = array();
    $where = "`song_id`={$sid}";

    $q = exQuery('SELECT', 'os_songs', '*', "`song_id`={$sid} AND `song_aka`=0");
    if (is_array($q)) {
        $out = array('id' => $q[0]['song_id'], 
                     'title' => $q[0]['song_title'], 
                     'ccli' => $q[0]['song_ccli'], 
                     'tags' => $q[0]['song_tags'], 
                     'filename' => $q[0]['song_filename'], 
                     'modified' => $q[0]['song_modified'], 
                     'authors' => array(),
                     'aka' => array());
        $qa = exQuery('SELECT', 'os_sa_xlink', '*', "`song_id` = {$sid}");
        if (is_array($qa)) {
            $out['authors'] = getAuthorList('crosslink', $qa)[0];
        } else {
            $logger->error("No authors associated with song_id {$sid}");
        }
        $qa = exQuery('SELECT', 'os_songs', 'song_title', "`song_id` = {$sid} AND `song_aka` = 1", "song_sort ASC");
        if (is_array($qa)) {
            foreach ($qa as $k => $i) {
                $out['aka'][] = $i['song_title'];
            }
            /* we don't need an else here, because there are songs without an alternate title */
        }
        /* pull extra data for lyrics */
        $ql = exQuery('SELECT', 'os_lyrics', '*', "`song_id` = {$sid}");
        if (is_array($ql)) {
			$l = processLyrics($ql[0]['lyrics_lyrics']);
			$out['lyrics'] = $l['lyrics'];
			$out['chords'] = $l['chords'];
            $out['present'] = processOrder($ql[0]['lyrics_presentation']);
            $out['copyright'] = $ql[0]['lyrics_copyright'];
        }
    } else {
        $logger->error("No song found at song_id {$sid}");
    }

    return json_encode(array("type"=>"details", "data" => $out, "update"=>$_SESSION['timeToUpdate'], ));
}

 /**
  * getSongList: Pulls song contents from database
  * Returns: JSON object
  *
  * @var string	for songs: 'all' (default), 'official', 
  */
function getSongList($sort='all') {
	global $logger;
	global $_SESSION;

    switch ($sort) {
        case 'all':
            $where = "";
            break;
        case 'official':
            $where = "`song_aka` = 0";
            break;
        default:
            return "[]";
            break;
    }

    $out = array();

    $q = exQuery('SELECT', 'os_songs', '*', $where, 'song_sort ASC');

    if (is_array($q)) {
        foreach ($q as $k => $i) {
            /* get base info */
            $out[] = array(
                'id' => $i['song_id'],
                'title' => $i['song_title'],
                'ccli' => $i['song_ccli'],
                'aka' => $i['song_aka'],
                'filename' => $i['song_filename'],
				'sorted' => $i['song_sort'],
			);
			$last = count($out) - 1;
            /* get authors */
            switch ($i['song_aka']) {
                case 0:
                    $ql = exQuery('SELECT', 'os_sa_xlink', 'auth_key', "`song_id` = {$i['song_id']}");
                    if (is_array($ql)) {
						$out[$last]['authors'] = getAuthorList('crosslink', $ql)[0];
						/* the extra [0] at the end of the line prevents a double array wrapper */
                    } else {
                        $logger->error("No crosslinks found for song_id {$i['song_id']}");
                    }
                    break;
                case 1:
                    $qs = exQuery('SELECT', 'os_songs', 'song_title', "`song_id` = {$i['song_id']} AND `song_aka` = 0");
                    if (is_array($qs)) {
                        $out[$last]["official"] = $qs[0]['song_title'];
                    } else {
                        $logger->error("Official song title for song_id {$i['song_id']} not found.");
                    }
                    break;
			}
        }
    } else {
        $out['firstrun'] = TRUE;
        $logger->info('Running database population for the first time.');
    }

    return json_encode(array("type"=>"songs", "sort"=>$sort, "data"=>$out, "update"=>$_SESSION['timeToUpdate'], ));
}

/**
 * getStats: pulls latest stats from database
 * Returns: JSON object
 */
function getStats() {
    $q = exQuery('SELECT', 'os_status', '*', '', 'status_key DESC');
    if (is_array($q)){
        $out = array("time" => $q[0]['last_update'], "songs" => $q[0]['total_songs'], 'timestamp' => strtotime($q[0]['last_update']));
    } else {
        $out = array("time" => date("r"), "songs" => 0, 'timestamp' => date('U'));
    }
    return json_encode($out);
}

/**
 * isLoginEnabled: checks to see if the master user exists
 * Returns: boolean
 */
function isLoginEnabled() {
	if (is_array(exQuery('SELECT', 'os_users', '*', 'user_key = 1'))) {
		return TRUE;
	} else {
		return FALSE;
	}
}

/**
 * lockUser: locks out a given user
 * Returns: boolean denoting success
 * 
 * @var	string	user name to lock out
 */
function lockUser($uname) {
	/* check if user exists */
	$w = "user_name LIKE '{$uname}'";
	$q = exQuery('SELECT', 'os_users', 'user_name', $w);
	if (is_array($q)) {
		return exQuery('UPDATE', 'os_users', array('user_lock' => 1), $w);
	} else {
		return FALSE;
	}
}

/**
 * processLyrics: parses OpenSong style lyrics, expanding headings,
 *              cropping out comments and chords and formatting
 *              lyric lines.
 * Returns: array containing modified lyrics and presentation order
 * 
 * @var string	lyrics to process
 */
function processLyrics($lyrics) {
	/* initialize function-level variables */
	$headings = array();
	$bridges = array();
	$choruses = array();
	$prechoruses = array();
	$sec_list = array();
	$tags = array();
	$verses = array();
	$eol = "\n"; /* default end of line is Linux */
	$sectype = "";
	$cursec = "";
	$weird_verses = FALSE;
	$validsec = FALSE; /* to prevent words under oddball sections like [Interlude] being displayed */
	$has_chords = FALSE;
	
	/* determine end-of-line character */
	if (strpos($lyrics,"\r\n") !== FALSE) { /* Windows */
		$eol = "\r\n";		
	} elseif (strpos ($lyrics,"\r") !== FALSE) { /* Mac */
		$eol = "\r";
	}
	
	/* split lyrics string */
	$lyrics_array = explode($eol,$lyrics);

	/* process lyrics */
	foreach($lyrics_array as $line) {
		$linestart = substr($line,0,1);
		if ($linestart == '[') {
			$line = trim($line,' ');
			if (preg_match('/\[[B,C,P,T,V]([\d]+)?\]/', $line)) {
				$line = str_replace('[', '', $line);
				$line = str_replace(']', '', $line);
				$sectype = $line;
				$line = replaceOpenSongSections($line);
				$headings[$sectype] = "<p class=\"songsection\">{$line}</p>";
				if ($weird_verses && ($sectype == 'C' || $sectype == "C1")) {
					array_splice($sec_list,1,0,$sectype);
				} else {
					$sec_list[] = $sectype;
				}
				$validsec = TRUE;
			} else {
				$validsec = FALSE;
			}
		} elseif (is_numeric($linestart)) {
			$weird_verses = TRUE;
			/* get verse number */
			preg_match('/^\d+/', $line, $matches);
			$key = $matches[0];
			/* set correct headings */
			if (isset($headings['V'])) {
				 unset($headings['V']);
				if (($sec_key = array_search('V', $sec_list)) !== FALSE) {
					unset ($sec_list[$sec_key]);
				}
				 
			}
			if (!isset($headings["V{$key}"])) {
				 $headings["V{$key}"] = "<p class=\"songsection\">Verse {$key}</p>"; 
			}
			$sec_list[]="V{$key}";
			$line = substr($line, strlen($key));
			/* strip underscores  andpipes */
			$line = str_replace('|', '', $line);
			$line = str_replace('_', '', $line);
			$line = "<p class=\"songlyrics\">{$line}</p>\n";
			if (!isset($verses[$key])) {
				$verses[$key] = "";
			}			
			$verses[$key] .= $line;
		} elseif ($validsec && ($linestart == ' ') && (strlen($line) > 1)) {
			/* strip underscores and pipes */
			$line = str_replace('|', '', $line);
			$line = str_replace('_', '', $line);
			$line = "<p class=\"songlyrics\">{$line}</p>\n";
			if (preg_match('/[0-9]+/',$sectype,$matches) == 1) {
				$key = $matches[0];
			} else {
				$key = "";
			}
			switch (substr($sectype,0,1)) {
				case "B":
					/* we are in an already active section, so append to last item */
					if ($cursec == $sectype) {
						if ($key == "") {
							$key = count($bridges) - 1;
						}
					} else {
						/* we are in a new section, so start new key */
						$cursec = $sectype;
						if ($key == "") {
							$key = count($bridges);
						}
					}
					/* initialize new key */
					if (!isset($bridges[$key])) { 
						$bridges[$key] = '';
					}
					/* append item to selected array section */
					$bridges[$key] .= $line;
					break;
				case "C":
										/* we are in an already active section, so append to last item */
					if ($cursec == $sectype) {
						if ($key == "") {
							$key = count($choruses) - 1;
						}
					} else {
						/* we are in a new section, so start new key */
						$cursec = $sectype;
						if ($key == "") { 
							$key = count($choruses);
						}
					}
					/* initialize new key */
					if (!isset($choruses[$key])) { 
						$choruses[$key] = '';
					}
					/* append item to selected array section */
					$choruses[$key] .= $line;
					break;
				case "P":
					/* we are in an already active section, so append to last item */
					if ($cursec == $sectype) {
						if ($key == "") {
							$key = count($prechoruses) - 1;
						}
					} else {
						/* we are in a new section, so start new key */
						$cursec = $sectype;
						if ($key == "") {
							$key = count($prechoruses);
						}
					}
					/* initialize new key */
					if (!isset($prechoruses[$key])) { 
						$prechoruses[$key] = '';
					}
					/* append item to selected array section */
					$prechoruses[$key] .= $line;
					break;
				case "T":
					/* we are in an already active section, so append to last item */
					if ($cursec == $sectype) {
						if ($key == "") {
							$key = count($tags) - 1;
						}
					} else {
						/* we are in a new section, so start new key */
						$cursec = $sectype;
						if ($key == "") {
							$key = count($tags);
						}
					}
					/* initialize new key */
					if (!isset($tags[$key])) { 
						$tags[$key] = '';
					}
					/* append item to selected array section */
					$tags[$key] .= $line;
					break;
				case "V":
					/* we are in an already active section, so append to last item */
					if ($cursec == $sectype) {
						if ($key == "") {
							$key = count($verses) - 1;
						}
					} else {
						/* we are in a new section, so start new key */
						$cursec = $sectype;
						if ($key == "") {
							$key = count($verses);
						}
					}
					/* initialize new key */
					if (!isset($verses[$key])) { 
						$verses[$key] = '';
					}
					/* append item to selected array section */
					$verses[$key] .= $line;
					break;
			}
		} elseif ($linestart == '.') {
			$has_chords = TRUE;
		}
	}
	
	
	/* build new lyrics string */
	$processed_lyrics = "";
	$processed_sections = "";
	foreach($sec_list as $item) {
		if (strpos($processed_sections, $item) === FALSE) {
			$processed_lyrics .= $headings[$item];
			if (strlen($item) == 1) {
				$key = 0;
			} else {
				$key = substr($item,1);
			}
			switch(substr($item,0,1)) {
				case "B":
					$processed_lyrics .= (isset($bridges[$key])) ? $bridges[$key] : '';
					break;
				case "C":
					$processed_lyrics .= (isset($choruses[$key])) ? $choruses[$key] : '';
					break;
				case "P":
					$processed_lyrics .= (isset($prechoruses[$key])) ? $prechoruses[$key] : '';
					break;
				case "T":
					$processed_lyrics .= (isset($tags[$key])) ? $tags[$key] : '';
					break;
				case "V":
					$processed_lyrics .= (isset($verses[$key])) ? $verses[$key] : '';
					break;
			}
			
			$processed_sections .= ",{$item}";
		} 
	}

	return array('lyrics' => $processed_lyrics, 'chords' => $has_chords); 
}

/** 
 * processOrder: Fixes the Presentation order to use full words
 * Returns: string containing the processed presentation order
 * 
 * @var	string	OpenSong presentation order string
 */
function processOrder($order) {
	/* function-specific items*/
	$processed_order = '';
	$to_process = array();
	 
	/* get presentation order */
	if ($order == "" || $order == " ") {
		$processed_order = 'No presentation order defined';
	} else {
		$processed_presorder = "";
		/* strip out double spaces */
		while (strpos($order,'  ') !== FALSE) {
			$order = str_replace('  ',' ',$order);
		}
		$to_process = explode(' ',$order);
		foreach ($to_process as $item) {
			$processed_order .= replaceOpenSongSections($item) . ', ';
		}
		$processed_order = trim($processed_order, ', ');
	}
	return $processed_order;
}

/**
 * replaceOpenSongSections: Replaces OpenSong section codes with fullly written
 *                          out names
 * Returns: string containing processed string 
 * 
 * @var string	text in which to replace codes
 */
function replaceOpenSongSections($text) {
	global $LANGUAGE;
	$text = str_replace('B', "{$LANGUAGE['song_details']['bridge']} ", $text);
	$text = str_replace('C', "{$LANGUAGE['song_details']['chorus']} ", $text);
	$text = str_replace('P', "{$LANGUAGE['song_details']['prechorus']} ", $text);
	$text = str_replace('T', "{$LANGUAGE['song_details']['tag']} ", $text);
	$text = str_replace('V', "{$LANGUAGE['song_details']['verse']} ", $text);
	return trim($text,' ');
}

?>
