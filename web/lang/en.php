<?php
/** 
 * OpenSong Library Online
 * 
 * Version: 2.0
 * 
 * Localization file: English
*/

$LANGUAGE = array(
    "code" => "en",
    "title" => "OpenSong Library Online",
    "about_link" => "About",
    "about_pg" => "<p>This website contains the list of English-language titles, authors and Song Select (CCLI) numbers currently available in our OpenSong library. You can browse the songs in the list and sort them by title and alias (i.e. other titles the song may be known under), by official title, by author’s last name or by the author’s first name. Please be aware that the authors listed are the actual people who wrote the songs, not the recording artists who performed the song. You can also search for specific songs by using the title, alias, or author’s name.</p>
    <p>Questions in regard to the website can be addressed to the web site administrator.</p>",
    "display_link" => "Display",
    "display_all_titles_link" => "All Song Titles",
    "display_official_titles_link" => "All Official Song Titles",
    "display_authors_last_link" => "Authors by Last Name",
    "display_authors_first_link" => "Authors by First Name",
    "log_in_link" => "Sign In",
    "log_out_link" => "Sign Out",
    "search_box_placeholder" => "Search title or author",
    "link_back_to_list" => "Return to List",
    "footer_last_updated" => "Last Updated:",
    "footer_total_songs" => "Total Songs:",
    "footer_upgood_title" => "Updating database",
    "footer_upbad_title" => "Check for new songs failed. See console.for details",
    "footer_upgood_title" => "Check for new songs successfully completed.",
    "footer_refreshdb_title" => "Refresh database",
    "legend" => array(
        "songs_all" => "Title (Author Last Name(s); CCLI #)<br>\n<i>Alias</i> (Original Title; CCLI #)",
        "songs_original" => "Title (Author Last Name(s); CCLI #)",
        "authors_first" => "Author Last Name, Author First Name<br>\nOfficial Title (CCLI #)",
        "authors_last" =>"Author First Name Author Last Name<br>\nOfficial Title (CCLI #)",
    ),
    "sort_order" => array(
        "songs_all" =>"All song titles A-Z",
        "songs_official" => "Official song titles A-Z",
        "authors_first" => "Authors listed by first name",
        "authors_last" => "Authors listed by last name",
    ),
    "search_results" => array(
        "title" => "Search Results",
        "no_results" => "If you don&rsquo;t tell me what to search for, I won't be able to find anything...",
        "term_text_sing" => "term",
        "term_text_plur" => "terms",
        "term_text_and" => "and",
        "results_text" => "I&rsquo;ve found %r% results for the search",
        "songs" => "Songs",
        "authors" => "Authors",
    ),
    "song_details" => array(
        "file_name" => "File name",
        "lyrics" => "Lyrics",
        "pres_order" => "Default Presentation Order",
        "not_def" => "Not defined",
        "details" => "Details",
        "authors" => "Author(s)",
        "copyright" => "Copyright",
        "ccli" => "Song Select (CCLI) Number",
        "no_ccli" => "Song does not have a CCLI Number.",
        "alt_titles" => "Alternate Titles",
        "no_alt_titles" => "No alternate titles found.",
        "last_update" => "File last updated", 
        "verse" => "Verse",
        "chorus" => "Chorus",
        "bridge" => "Bridge",
        "tag" => "Tag",
        "prechorus" => "Pre-Chours",
        "chords" => "This song has chords in OpenSong. Contact a library manager to get a copy of them.",
    ),

    "modal_no_setup" => array(
        "msg" => "OpenSong Library Online has not yet been set up. Click the button below to run the setup script.",
        "button" => "Set Up OpenSong Library Online",
    ),

    "firstrun" => array(
        "msg_init" => "You are running OpenSong Online<br> for the first time.<br>\nPreparing song database.<br>\nThis may take a while, please be patient.",
        "msg_cont" => "Loading songs for the first time...<br>\nPlease be patient. This may take a while...<br>\nProcessing song %current% of %total% <br>\n<b>Please don&rsquo;t close your browser!</b>"
        
    ),

    "login" => array(
        "title" => "Sign In",
        "error" => "Incorrect user name or password entered. Please check your entry and try again. Alternatively, contact your the site admin to regain your login credentials.",
        "user_title" => "User Name",
        "user_case" => "User name is case sensitive",
        "pwd_title" => "Password",
        "btn_text" => "Sign in",
    ),

    /* strings that are only available once login is complete */
    "secured" => array(
        "links" => array (
            "add_song" => "Add Song",
            "libmgmt_menu" => "Library",
            "libmgmt_dbupdate" => "Update Library now",
            "sets" => "Sets",
            "sets_num" => "Number of songs in the current unsaved set",
            "sets_current" => "Current Set",
            "sets_all" => "All Sets",
            "user_profile" => "Profile",
            "user_management" => "User Management",
            "user_logout" => "Sign Out",

        ),
        "profile" => array(
            "title" => "Profile Page of",
            "full_name_title" => "Full Name",
            "full_name_placeholder" => "Enter your full name here.",
            "full_name_button" => "Save Full Name",
            "email_title" => "e-Mail address",
            "email_placeholder" => "Enter your e-mail address",
            "email_button" => "Save e-Mail address",
            "email_err" => "Invalid e-mail address. Please check it and try again.",
            "pwd_chg_title" => "Change Password",
            "pwd_chg_old_title" => "Current Password",
            "pwd_chg_old_placeholder" => "Enter your current password",
            "pwd_chg_old_err" => "The password entered is incorrect.",
            "pwd_chg_new_title" => "New Password",
            "pwd_chg_new_placeholder" => "Enter a new password",
            "pwd_chg_new_directives" => "Please enter a password that is at least 8 characters long and that has at least one lower case letter, upper case letter and one digit. It is recommended to add special characters as well, except for <code>&quot;</code> <code>&apos;</code> or <code>&bsol;</code>.",
            "pwd_chg_new_err" => "The password entered does not conform to the limitations above. Please check your password and try again.",
            "pwd_chg_cnf_title" => "Confirm New Password",
            "pwd_chg_cnf_placeholder" => "Enter the new password again",
            "pwd_chg_cnf_err" => "The password confirmation does not match the new password. Please check the confirmation and try again.",
            "pwd_chg_btn" => "Reset Password",
            "pwd_success" => "Password changed successfully.",
            "ufull_success" => "Full name changed sucessfully.",
            "email_success" => "Email address changed successfully",
            "pwdrst_title" => "Reset Your Password",
            "pwdrst_err_txt" => "Unable to reset password. Check to see that your new password conforms to the password requirements. If the error persists, contact the site administrator.",
            "pwdrst_tmppwd" => "Temporary password",
            "pwdrst_tmppwd_placeholder" => "Enter the temporary password you just used",
            "pwdrst_but_cancel" => "Cancel",
            "pwdrst_success_msg" => "You have successfully reset your password. Please log in again using your new password.<br> Click the button below to continue.",
            "mdl_btn_close" => "Close",
        ),
        "usr_mgmt" => array(
            "col_uname" => "User Name",
            "col_fname" => "Full Name",
            "col_email" => "e-mail Address",
            "col_locked" => "Locked",
            "col_admin" => "Site Admin",
            "col_admin_blurb" => "Can manage user access to the web site",
            "col_lib_admin" => "Library",
            "col_lib_admin_blurb" => "Manages the pCloud hosted OpenSong Library files and receives e-mails notifying about new songs.",
            "but_usrpwdrst" => "Reset Password",
            "but_usredit" => "Edit User",
            "but_usrdel" => "Delete User",
            "but_usradd" => "Add User",
            "but_save" => "Save User",
            "but_cancel" => "Cancel",
            "but_ok" => "OK",
            "but_yes" => "Yes",
            "but_no" => "No",
            "ph_uname" => "Unique user name",
            "ph_fname" => "User&rsquo;s full name",
            "ph_email" => "e-mail address (optional)",
            "uname_explanation" => "User name must be at least 3 characters in length, is case sensitive, can consist of standard upper and lowercase alphanumeric characters and . - or _ and MUST NOT be &ldquo;new_user&rdquo;. International characters cannot be used.",
            "mdl_title" => "Password Reset!",
            "mdl_txt1" => "The password for user %u% was reset successfully.",
            "mdl_txt_mail" => "The user has been e-mailed the following temporary password:",
            "mdl_txt_nomail" => "The user has no e-mail address. Please pass the following temporary password on to them:",
            "mdl_txt2" => "They will be required to change their password the next time they sign in.",
            "mdl_btn_close" => "Close",
            "uadd_success_title" => "User Added Successfully!",
            "uadd_success_txt1" => "The user %u% has successfully been added to %apptitle%.",
            "uadd_success_txt3" => "Click OK to continue.",
            "uadd_error" => "Unable to add or update user! Check the data entered and try again.",
            "uupd_success_title" => "User Updated Successfully!",
            "uupd_success_txt1" => "The data for the user %u% has been updated successfully successfully.",
            "udel_mdl_title" => "Delete User",
            "udel_mdl_txt1" => "Are you sure you want to delete the user %u%?",
            "udel_mdl_txt2" => "This action cannot be undone!",
        ),
        "add_song" => array (
            'title' => 'Add Song',
            'blurb' => 'Is there a song missing from the Library? Add it here. All <span class="text-bold">bold</span> fields are required.',
            'song_title_title' => 'Song Title',
            'song_title_blurb' => 'This should be the offcial title found in the <a href="https://songselect.ccli.com/" target="_blank">SongSelect Library</a>. Feel free to add a designation in parentheses to make the title more specific: e.g. <code>(Hillsong)</code>',
            'song_title_err' => 'A song title must have at least one character in it! It may not contain the following characters: <code>&gt; &lt = { }</code>.',
            'authors_title' => 'Author(s)',
            'authors_placeholder' => 'David Ben Jesse; Owen Chisholm, Jr.; Martha Bethany',
            'authors_blurb' => 'These are the <span class="text-italic">authors</span> of the song, not the performing group (e.g. <code>Matt Crocker; Joel Houston; Dylan Thomas</code> rather than <code>Hillsong UNITED</code>). Please separate multiple authors with a <code>;</code> (semicolon).',
            'authors_err' => 'An author&rsquo;s name needs to be at least one character long and can only contain letters, numbers, <code>.</code>, <code>&apos;</code> or <code>,</code>. Spaces and <code>;</code> can also be entered as separators. Or fill in a CCLI number below.',
            'ccli_title' => 'CCLI #',
            'auth_ccli_err' => 'You must enter either the authors of the song or else a CCLI # so the library manager can check the entry.',
            'copy_title' => 'Copyright',
            'copy_btn' => 'Add &copy; symbol to beginning of copyright field',
            'lyrics_title' => 'Song Lyrics',
            'lyrics_blurb1' => 'Please enter the lyrics using OpenSong tags to define the Verses, Chorus, etc. You can use the buttons below to add the various headings.',
            'lyrics_blurb2' => 'You can refer to the <a href="http://www.opensong.org/home/getting-started" target="_blank">OpenSong Getting Started documentation</a> under Songs, point 4, as to how to properly enter songs.',
            'lyrics_btn_b' => 'Bridge',
            'lyrics_btn_c' => 'Chorus',
            'lyrics_btn_p' => 'Pre-Chorus',
            'lyrics_btn_t' => 'Tag',
            'lyrics_btn_v' => 'Verse',
            'order_title' => 'Presentation Order',
            'order_placeholder' => 'V1 C V2 C B C T',
            'order_blurb' => 'Use the OpenSong tags to define the order in which the song will be presented by default.',
            'order_err' => 'There are references to invalid tags in the presentation order field. Please check these and correct them',
            'source_title' => 'Source of Chords and Lyrics:',
            'source_placeholder' => 'https://www.worshiptogether.com/songs/song-title/',
            'source_blurb' => 'Enter a URL, book name, or sheet music repository here where you got the chords and lyrics from. This will allow us to check the key. You can leave this blank if you aren&rsquo;t submitting any chords.',
            'add_to_set_title' => 'Add song to current set',
            'btn_submit_song'  => 'Submit Song',
            'err_bad_fields' => 'Some of the fields did not pass validation. Please check them and try again.',
            'err_no_save' => 'Unable to save song. Please contact the site administrator.',
            'success1' => 'The song “%s%” was saved successfully. It will now be reviewed by the library administrators.',
            'success2' => 'The song was also successfully added to the current set.',
        ),

        "sets" => array(
            "add_btn_lbl" => "Add this song to the current set", 
            "cur_set_title" => "Current Set",
            "cur_set_btn_new" => "New Set",
            'cur_set_btn_save' => "Save Current Set",
            'cur_set_blurb' => "Drag the songs to reorder them and hit the <i class=\"far fa-save\"></i> Save button to save the set to the OpenSong Library. Newly added songs may not be immediately available in the song library when opening the set in the OpenSong Application. Clicking <i class=\"far fa-file\"></i> New Set will erase all songs from this list.",
            'cur_set_set_title' => "Set Title",
            'cur_set_set_title_btn_save' => "Save Current Set Title",
            'cur_set_empty' => "There are no songs in the current set. Please add some songs to create a set.",
            'cur_set_btn_del' => 'Remove item from set',
            'cur_set_music_icon_reg' => 'Song in library',
            'cur_set_music_icon_new' => 'New Song',
            'item_del_btn' => 'Remove item from set',
            'item_song_procd' => 'Song in library',
            'item_song_new' => 'New song',
            'cur_set_save_success' => "The current set was saved successfully.",
            'cur_set_save_error' => "Unable to save the current set. Please contact the site administrator.",
            "cur_set_mdl_title" => "Are You Sure?",
            "cur_set_mdl_body" => "The current set has not been saved. Are you sure you want to start a new one? The current set will be deleted. This action cannot be undone!",
            "cur_set_mdl_logout_body" => "The current set has not been saved. Are you sure you want to log out?",
            "but_yes" => "Yes",
            "but_no" => "No",
            'old_sets_modal_title' => 'Load Existing Set?',
            'old_sets_modal_text1' => 'If you load an existing set into %apptitle% and save it under the same name, all existing items (Scripture, Images, etc.) other than the Songs will be deleted.',
            'old_sets_modal_text2' => 'Are you sure you want to load this existing set?',
            'old_sets_title' => 'All Sets',
            'but_edit_set' => 'Edit this set',
            'oldset_file_error' => 'Unable to open the file containing the set.',
            'sets_items' => array(
                'song' => 'Song',
                'scripture' => 'Scripture',
                'custom' => 'Slide',
                'slides' => 'slides', /* plural of above */
                'image' => 'Image',
                'images' => 'images', /* plural of above */
                'style' => 'Style',
                'external' => 'External'
            ),
            'change_style' => 'Change presentation style',
        ),
    ),
    'emails' => array(
        'pwd_reset_subject' => 'Your password at %sitename% has been reset!',
        'pwd_reset_text' => "Hi, %user%,

The administrator has successfully reset your access password for the 
%sitename%. You can now log in with the following temporary password:

    %tmppwd%

For security, you will be required to create a new password when you log in 
with the temporary one above.

Enjoy using the %sitename%!

The Maintenance Team\n",

        'usr_creation_subject' => 'A user account has been created for you at %sitename%',
        'usr_creation_text' => "Hi, %user%,

An account for you has successfully been created at the %sitename%, which you 
can access at %siteurl%. You can log in using the 
following credentials:

    User Name: %uname%
    Temporary password: %tmppwd%

For security, you will be required to create a new password when you log in 
with the temporary one above.

Enjoy using the %sitename%!

The Maintenance Team\n",
        'usr_deletion_subject' => 'Your user account at %sitename% has been deleted!',
        'usr_deletion_text' => "Hi, %user%,

Your account at the %sitename% was deleted by the site administrator. 
You will no longer have access to the registered users’ portion of the website.
If this is in error, please contact the site administrator directly.

The Maintenance Team\n",
        'add_song_subject' => 'New song added to %sitename%',
        'add_song_text' => "%userfullname% (%username%) has added the song “%songtitle%” to the library. 
You can find it in the %songfolder% folder under the file name %songfile%. 
%duplicate%Please review it before next Sunday, %date%. Thanks!

The Maintenance Team\n",
        'add_song_duplicate' => 'A file with the same name already exists in the database. Be sure to modify it before saving.',
    ),
);

/* do not edit below this line! */
if (file_exists("lang/{$LANGUAGE['code']}-about.html")) {
    $LANGUAGE['about_pg'] = @file_get_contents("lang/{$LANGUAGE['code']}-about.html");
} 
