<?php
/** 
 * OpenSong Library Online
 * 
 * Version: 2.0
 * 
 * Localization file: German
*/

$LANGUAGE = array(
    "code" => "de",
    "title" => "OpenSong Liederbuch Online",
    "about_link" => "Über Uns",
    "about_pg" => "<p>Auf dieser Website befinden sich die in unserem OpenSong Liederbuch befindlichen deutschsprachigen Titel, Autoren und CCLI Liednummern. Die Lieder können in einer liste aufgezeigt werden oder nach offiziellem Titel und anderen Titeln unter dem das Lied bekannt ist, nur offiziellen Titeln oder nach dem Vor- oder Nachnamen der Autoren sortiert werden. Die aufgelisteten Autoren sind die eigentlichen Verfasser der angegebenen Lieder, nicht to die Künstler/innen, die das Lied aufgenommen haben. Es kann auch nach Titel oder Autorennamen gesucht werden.</p>
    <p>Fragen zur Webseite können an den Websiteadministrator gerichtet werden</p>",
    "display_link" => "Anzeigen",
    "display_all_titles_link" => "Alle Lieder",
    "display_official_titles_link" => "Alle offizielle Titles",
    "display_authors_last_link" => "Autoren nach Nachnamen",
    "display_authors_first_link" => "Autoren nach Vornamen",
    "log_in_link" => "Anmelden",
    "log_out_link" => "Abmelden",
    "search_box_placeholder" => "Suche nach Titel od. Autor",
    "link_back_to_list" => "Zurück zur List",
    "footer_last_updated" => "Letzter Update:",
    "footer_total_songs" => "Liederanzahl:",
    "footer_upbad_title" => "Update der neuen Lieder fehlgeschlagen. Console einsehen.",
    "footer_upgood_title" => "Update der neuen Lieder erfolgreich.",
    "footer_refreshdb_title" => "Neue Daten werden überprüft",
    "legend" => array(
        "songs_all" => "Titel (Autorennachname(n); CCLI NR.)<br>\n<i>Alternativtitel</i> (offizieller Titel; CCLI Nr.)",
        "songs_original" => "Titel (Aurorennachname(n); CCLI Nr.)",
        "authors_first" => "Autorennachname, Autorenvorname<br>\nOffizieller Titel (CCLI Nr.)",
        "authors_last" =>"Autorenvorname Autorennachname<br>\nOffizieller Titel (CCLI Nr.)",
    ),
    "sort_order" => array(
        "songs_all" =>"Alle Liedtitel von A-Z",
        "songs_official" => "Offizielle Liedtitel von A-Z",
        "authors_first" => "Authoren nach Vornamen",
        "authors_last" => "Authoren nac Nachnamen",
    ),
    "search_results" => array(
        "title" => "Suchresultate",
        "no_results" => "Also, wenn man mir nichts zu suchen gibt, finde ich auch nichts!",
        "term_text_sing" => "Begriff",
        "term_text_plur" => "Begriffe",
        "term_text_and" => "und",
        "results_text" => "Ich habe für die folgende suche %r% resultate gefunden: ",
        "songs" => "Lieder",
        "authors" => "Autoren",
    ),
    "song_details" => array(
        "file_name" => "Dateiname",
        "lyrics" => "Liettext",
        "pres_order" => "Reihenfolge der Präsentation",
        "not_def" => "Nicht definiert",
        "details" => "Details",
        "authors" => "Autor(en)",
        "copyright" => "Urheberrechte",
        "ccli" => "Song Select (CCLI) Nummer",
        "no_ccli" => "Dieses Lied hat keine CCLI Nummer.",
        "alt_titles" => "Alternative Titele",
        "no_alt_titles" => "Keine alternative Titel gefunden.",
        "last_update" => "Letzter Dateiupdate", 
        "verse" => "Vers",
        "chorus" => "Refrain",
        "bridge" => "Bridge",
        "tag" => "Tag",
        "prechorus" => "Pre-Chours",
        "chords" => "Dieses Lied hat in OpenSong auch Akkorde. Eine Kopie kann von dem Liederbuchmanager geholt werden.",
    ),

    "modal_no_setup" => array(
        "msg" => "OpenSong Liederbuch Online ist noch nicht eingerichtet. Clicken Sie den Knopf um die Einrichtung zu starten.",
        "button" => "OpenSong Liederbuch Online einrichten",
    ),

    "firstrun" => array(
        "msg_init" => "OpenSong Liederbuch Online<br>  wird zum ersten mal gestartet<br>\nLiederdatenbank wird eingerichtet.<br>\nDies kann etwas dauern. Wir bitten um Geduld.",
        "msg_cont" => "Lieder werden zum ersten mal geladen...<br>\nDies kann eine Weile dauern. Wir bitten um Geduld.<br>\nLied %current% von %total% <br>\n<b>Den Browser bitten nicht schließen!</b>"
        
    ),

    "login" => array(
        "title" => "Anmeldung",
        "error" => "Ein falscher name oder password wurde eingegeben. Kontrollieren sie bitte Ihre Daten und probieren sie es nochmals. Sie können sich auch an den Websitemanager wenden, um neue Logindaten zu bekommen.",
        "user_title" => "Benutzername",
        "user_case" => "Groß- und Kleinschreibung beachten!",
        "pwd_title" => "Passwort",
        "btn_text" => "Anmelden",
    ),

    /* strings that are only available once login is complete */
    "secured" => array(
        "links" => array (
            "add_song" => "Lied Hinzufügen",
            "libmgmt_menu" => "Liederbuch",
            "libmgmt_dbupdate" => "Liederbuch jetzt updaten",
            "sets" => "Präsentationen",
            "sets_num" => "Nichtgespeicherte Lieder in dieser Präsentation",
            "sets_current" => "Aktive Präsentation",
            "sets_all" => "Alle Präsentationen",
            "user_profile" => "Profil",
            "user_management" => "Benutzermanagement",
            "user_logout" => "Abmelden",

        ),
        "profile" => array(
            "title" => "Profilseite von",
            "full_name_title" => "Voller Name",
            "full_name_placeholder" => "Vollen Namen hier eingeben.",
            "full_name_button" => "Vollen Namen speichern",
            "email_title" => "e-Mailadresse",
            "email_placeholder" => "e-Mailadresse hier eingeben",
            "email_button" => "e-Mailaddresse speichern",
            "email_err" => "Die eingegebene e-Mailadresse ist nicht richtig. Bitte kontrollieren und nochmals abspeichern.",
            "pwd_chg_title" => "Passwort ändern",
            "pwd_chg_old_title" => "Aktuelles passwort Passwort",
            "pwd_chg_old_placeholder" => "Aktuelles Passwort eingeben",
            "pwd_chg_old_err" => "Der aktuelle Passwort ist nicht richtig.",
            "pwd_chg_new_title" => "Neues Passwort",
            "pwd_chg_new_placeholder" => "Neues passwort eingeben",
            "pwd_chg_new_directives" => "Das eingegebene Passwort must aus mindestens 8 Zeichen bestehen, von denen eines ein kleiner Buchtabe, eines ein großer Buchstabe und eines eine Ziffer seien muss. Es ist ratsam, auch Sonderzeichen einzugeben mit ausnahme der folgenden: <code>&quot;</code> <code>&apos;</code> oder <code>&bsol;</code>.",
            "pwd_chg_new_err" => "Das Passwort stimmt nicht mit den oben angegebenen Regeln ein. Bitte kontrollieren und nochmals abspeichern.",
            "pwd_chg_cnf_title" => "Neues Passwort bestätigen",
            "pwd_chg_cnf_placeholder" => "Neues passwort nochmals eingeben",
            "pwd_chg_cnf_err" => "Das Passwort konnte nicht bestätigt werden. Bitte kontrollieren und nochmals abspeichern.",
            "pwd_chg_btn" => "Passwort zurücksetzen",
            "pwd_success" => "Das Passwort wurde geändert.",
            "ufull_success" => "Der volle Name wurde gespeichert.",
            "email_success" => "Die e-Mailadresse wurde gespeichert.",
            "pwdrst_title" => "Passwort zurücksetzen",
            "pwdrst_err_txt" => "Passwort konnte nicht zurückgesetzt werden. Kontrollieren Sie bitte, dass das eingegebene neue Passwort den Bedingungen entspricht. Wenn der Fehler weiterhin auftrit, bitte den Websiteadministrator benachrichtigen.",
            "pwdrst_tmppwd" => "Vorübergehendes Passwort",
            "pwdrst_tmppwd_placeholder" => "Das eben gebrauchte vorübergehende Passwort eingeben",
            "pwdrst_but_cancel" => "Abbrechen",
            "pwdrst_success_msg" => "Das Passwort wurde erfolgreich zurückgesetzt. Melden Sie sich bitte mit Ihrem neuen Passwort erneut an.<br> Clicken Sie den Knopf um fortzufahren.",
            "mdl_btn_close" => "Schließen",
        ),
        "usr_mgmt" => array(
            "col_uname" => "Benutzername",
            "col_fname" => "Voller Name",
            "col_email" => "e-Mailadresse",
            "col_locked" => "Ausgeschlossen",
            "col_admin" => "Adminsitrator",
            "col_admin_blurb" => "Verwaltet die Benutzer",
            "col_lib_admin" => "Liederbuch",
            "col_lib_admin_blurb" => "Verwaltet den OpenSong Liederbuch in pCloud und bekommt e-Mailsnachrichten über neue Lieder.",
            "but_usrpwdrst" => "Passwort zurücksetzen",
            "but_usredit" => "Benutzer bearbeiten",
            "but_usrdel" => "Benutzer löschen",
            "but_usradd" => "Benutzer hinzufügen",
            "but_save" => "Benutzer speichern",
            "but_cancel" => "Abbrechen",
            "but_ok" => "OK",
            "but_yes" => "Ja",
            "but_no" => "Nein",
            "ph_uname" => "Einmaliger Benutzername",
            "ph_fname" => "Voller Name des Benutzers",
            "ph_email" => "e-Mailadresse (optional)",
            "uname_explanation" => "Der Benutzername must midestens aus 3 Standartbuchstaben in Groß oder Kleinformat, Ziffern oder den Zeichen . - oder _ bestehen. Die Groß- und Kleinschreibung ist wichtig! Es darf nicht &ldquor;new_user&ldquo; lauten.",
            "mdl_title" => "Passwort zurückgesetzt!",
            "mdl_txt1" => "Das Passwort für den Benutzer %u% wurde erfolgreich zurückgesetzt.",
            "mdl_txt_mail" => "Dem Benutzer wurde das folgende vorübergehende Passwort per e-Mail zugeschickt:",
            "mdl_txt_nomail" => "Der Benutzer hat keine e-Mailadresse. Das folgende vorübergehende Passwort muss an ihn weitergeleitet werden:",
            "mdl_txt2" => "Bei der nächsten Anmeldung wird der Benutzer sein Passwort ändern müssen.",
            "mdl_btn_close" => "Schließen",
            "uadd_success_title" => "Benutzer erfolgreich hinzugefügt!",
            "uadd_success_txt1" => "Der Benutzer %u% ist erfolgreich zu &ldquor;%apptitle%&ldquo; hinzugefügt worden.",
            "uadd_success_txt3" => "OK clicken um fortzufahren.",
            "uadd_error" => "Der Benutzer konnte nicht gespeichert werden. Die Daten bitte kontrollieren und nochmals versuchen.",
            "uupd_success_title" => "Benutzerdaten erfolgreich gespeichert!",
            "uupd_success_txt1" => "Die Daten des Benutzers %u% wurden erfolgreich gespeichert.",
            "udel_mdl_title" => "Benutzer löschen",
            "udel_mdl_txt1" => "Sind Sie sicher, dass sie den Benutzer %u% löschen wollen?",
            "udel_mdl_txt2" => "Diese Aktion kann nicht zurückgefahren werden!",
        ),
        "add_song" => array (
            'title' => 'Lied hinzufügen',
            'blurb' => 'Fehlt in dem Liederbuch ein Lied? Hier kann es hinzugefügt werden. Alle <span class="text-bold">fettgedruckten</span> Datenfelder müssen ausgefüllt werden.',
            'song_title_title' => 'Titel',
            'song_title_blurb' => 'Dies sollte der offizielle Titel des Liedes sein, dass in der <a href="https://songselect.ccli.com/" target="_blank">SongSelect Bibliothek</a> zu finden ist. Es kann auch eine zusätzliche Deisgnation in Paranthese eingegeben werden, um das Lied mehr spezifisch zu machen, z.B. <code>(Hillsong)</code>',
            'song_title_err' => 'Der Titel muss mindestens ein Schriftzeichen beinhalten. Es darf aber die folgenden Schriftzeichen nicht beinhalten: <code>&gt; &lt = { }</code>.',
            'authors_title' => 'Autor(en)',
            'authors_placeholder' => 'David Ben Jesse; Owen Chisholm, Jr.; Martha Bethany',
            'authors_blurb' => 'Dies sind die <span class="text-italic">Verfasser</span> des Liedes, nicht die Künstler oder Band, die es aufgenommen hat (z.B. <code>Matt Crocker; Joel Houston; Dylan Thomas</code> anstatt <code>Hillsong UNITED</code>). Mehrere Autoren bitte mit einem <code>;</code> (Strichpunkt) trennen.',
            'authors_err' => 'Der Name des Autoren mus mindestens aus einem Schriftzeicchen Bestehen. Er darf Buchstaben, Zahlen und den Schriftzeichen <code>.</code>, <code>&apos;</code> oder <code>,</code> beinhalten. Leerzeichen und <code>;</code> können als Trennzeichen gebraucht werden. Alternativ kann das Feld für die CCLI Nummer ausgefüllt werden.',
            'ccli_title' => 'CCLI Nr.',
            'auth_ccli_err' => 'Es müssen entweder die Verfasser des Liedes oder die CCLI Nummer eingegeben werden damit der Liederbuchverwalter die Daten überprüfen kann.',
            'copy_title' => 'Urheberrechte',
            'copy_btn' => '&copy; Symbol am Anfang des Datenfeldes einfügen',
            'lyrics_title' => 'Liedtext',
            'lyrics_blurb1' => 'Den Liedtext bitte unter Benutzung der OpenSong Codes zum Bestimmen der Verse, des Refrains, usw. eingeben. Die untenliegenden Tasten können gebraucht werden, um die richtigen Codes einzugeben.',
            'lyrics_blurb2' => 'Für weitere Informationen wie Liedtexte eingegeben werden sollten, siehe <a href="http://www.opensong.org/home/getting-started" target="_blank">OpenSong Getting Started documentation</a> Songs, Punkt 4 (die Seite gibt es zurzeit nur auf Englisch).',
            'lyrics_btn_b' => 'Bridge',
            'lyrics_btn_c' => 'Refrain',
            'lyrics_btn_p' => 'Pre-Chorus',
            'lyrics_btn_t' => 'Tag',
            'lyrics_btn_v' => 'Vers',
            'order_title' => 'Reihenfolge der Präsentation',
            'order_placeholder' => 'V1 C V2 C B C T',
            'order_blurb' => 'Bitte die OpenSong Codes gebrauchen um die Reihenfolge in der das Lied standardmäßig präsentiert werden soll einzugeben.',
            'order_err' => 'Es befinden sich ungültige Codes in dem Datenfeld. Bitte korrigieren.',
            'source_title' => 'Quelle der Akkorde und des Liedtextes:',
            'source_placeholder' => 'https://www.worshiptogether.com/songs/song-title/',
            'source_blurb' => 'Ein URL, Liederbuchtitel oder Partiturwebsite wo die eingegebenen Akkorde und Liedtexte herkommen. Dies ermöglicht es, die Tonart zu kontrollieren. Dieses Datenfeld kann leergelassen werden, wenn keine Akkorde eingegeben worden sind.',
            'add_to_set_title' => 'Lied der aktuellen Präsention hinzufügen',
            'btn_submit_song'  => 'Lied Absenden',
            'err_bad_fields' => 'Einige Datenfelder enthalten fehler. Bitte korrigieren und nochmals absenden.',
            'err_no_save' => 'Lied konnte nicht gesandt werden. Bitte mit dem Websiteadministrator in Verbindung treten.',
            'success1' => 'Das Lied „%s%“ wurde erfolgreich gesandt. Es wird nun von dem Liederbuchadministrator kontrolliert.',
            'success2' => 'Das Lied wurde auch erfolgreich zur aktuellen Präsentation hinzugefügt.',
        ),

        "sets" => array(
            "add_btn_lbl" => "Lied zur aktuellen Präsentation hinzufügen", 
            "cur_set_title" => "Aktuelle Präsentation",
            "cur_set_btn_new" => "Neue Präsentation",
            'cur_set_btn_save' => "Aktuelle Präsentation speichern",
            'cur_set_blurb' => "Ziehen Sie die Lieder um die Reihenfolge der Lieder zu verändern. Die Präsentation kann mit der <i class=\"far fa-save\"></i> Speichertaste in OpenSong gespeichert werden. Neu hinzugefügte Lieder können möglicherweise erst nach einer bestimmten Frist im OpenSong-Programm aufzufinden seien. Wenn die <i class=\"far fa-file\"></i> Neue Präsentation Taste gedrückt wird, werden alle Lieder aus dieser Liste gelöscht.",
            'cur_set_set_title' => "Titel der Präsenation",
            'cur_set_set_title_btn_save' => "Titel Speichern",
            'cur_set_empty' => "Es sind noch keine Lieder hinzugefügt worden. Bitte Lieder hinzufügen um die Präsentation zu erstellen.",
            'cur_set_btn_del' => 'Lied aus der Präsentation entfernen',
            'cur_set_music_icon_reg' => 'Lied ist im Liederbuch',
            'cur_set_music_icon_new' => 'Neues Lied',
            'item_del_btn' => 'Lied aus der Präsentation entfernen',
            'item_song_procd' => 'Lied ist im Liederbuch',
            'item_song_new' => 'Neues Lied',
            'cur_set_save_success' => "Die aktuelle Präsentation wurde gespeichert.",
            'cur_set_save_error' => "Die aktuelle Präsentation konnte nicht gespeichert werden. Bitte den Webseitenadminstrator benachrichtigen.",
            "cur_set_mdl_title" => "Sind Sie Sicher?",
            "cur_set_mdl_body" => "Die aktuelle Präsentation ist noch nicht gespeichert worden. Sind Sie sicher, dass Sie eine neue Präsentation erstellen wollen? Alle Lieder werden entfernt und dies kann nicht zurückgenommen werden!",
            "cur_set_mdl_logout_body" => "Die aktuelle Präsentation ist noch nicht gespeichert worden. Sind Sie sicher, dass Sie sich abmelden wollen?",
            "but_yes" => "Ja",
            "but_no" => "Nein",
            'old_sets_modal_title' => 'Existierende Präsentation bearbeiten?',
            'old_sets_modal_text1' => 'Wenn eine existierende Präsentation bearbeitet und unter dem gleichen Namen gespeichert wird, dann werden alle Elemente (Bibletexte, Bilder, usw.) außer den Liedern gelöscht.',
            'old_sets_modal_text2' => 'Sind Sie Sicher, dass Sie die Präsentation laden wollen?',
            'old_sets_title' => 'Alle Präsentationen',
            'but_edit_set' => 'Präsentation bearbeiten',
            'oldset_file_error' => 'Die Präsentationsdatei konnte nicht geöffnet werden.',
            'sets_items' => array(
                'song' => 'Lied',
                'scripture' => 'Bibeltext',
                'custom' => 'Text',
                'slides' => 'Texte', /* plural of above */
                'image' => 'Bild',
                'images' => 'Bilder', /* plural of above */
                'style' => 'Stil',
                'external' => 'Fremdinhalt'
            ),
            'change_style' => 'Präsenationsstil ändern',
        ),
    ),
    'emails' => array(
        'pwd_reset_subject' => 'Das Passwort für %sitename% ist zurückgesetzt worden!',
        'pwd_reset_text' => "Hallo, %user%,

Der Administrator hat dass Passwort für %sitename%
erfolgreich zurückgesetzt. Es kann nun mit dem folgenden vorübergebenden
Passwort eingeloggt werden:

    %tmppwd%

Zu unserer aller Sicherheit, muss bei der nächsten Anmeldung ein neues 
Passwort geschaffen werden.

Viel Freude beim Benutzen von %sitename%!

Das Wartungsteam\n",

        'usr_creation_subject' => 'Ein Benutzerkonto wurde bei %sitename% erstellt!
        A user account has been created for you at %sitename%',
        'usr_creation_text' => "Guten Tag, %user%,

Es wurde für Sie bei %sitename% ein Benuzerkonto erstellt.
Sie können die Webseit unter der Adresse %siteurl% erreichen.
Sie können sich mit den folgenden Daten anmelden:

    Benutzername: %uname%
    Vorübergehendes Passwort: %tmppwd%

Zu unserer aller Sicherheit, muss bei der nächsten Anmeldung ein neues 
Passwort geschaffen werden.
    
Viel Freude beim Benutzen von %sitename%!
    
Das Wartungsteam\n",
        'usr_deletion_subject' => 'Benutzerkonto bei %sitename% gelöscht',
        'usr_deletion_text' => "Guten Tag, %user%,

Einer der Administratoren hat ihr Benutzerkonto bei %sitename%
gelöscht. Sie haben nun keinen Zugriff zu der Benutzeroberfläche für
registrierte Benutzer. Falls dies fehlerhaft geschehen ist, wenden Sie
sich bitte and den Administrator.

Das Wartungsteam\n",
        'add_song_subject' => 'Ein neues Lied wurde hinzugefügt - %sitename',
        'add_song_text' => "%userfullname% (%username%) hat ein neues Lied mit dem Titel
„%songtitle%“ dem Liederbuch hinzugefügt.
Das Lied ist in dem Verzeichnis %songfolder% unter dem Dateinamen 
%songfile% zu finden.
%duplicate%Bitte, diese Datte vor Sonntag, dem %date%, überprüfen. Danke!

Das Wartungsteam\n",
        'add_song_duplicate' => 'Ein Lied mit dem gleichen Dateinamen ist schon im Liederbuch vorhanden.
Den Dateinamen bitte vor dem Speichern abändern.',
    ),
);

/* do not edit below this line! */
if (file_exists("lang/{$LANGUAGE['code']}-about.html")) {
    $LANGUAGE['about_pg'] = @file_get_contents("lang/{$LANGUAGE['code']}-about.html");
} 
