/**
 * OpenSong Library Online
 * 
 * version: 2.0
 * file version: 2.0.1
 * updated: 2021-06-18 | 07:33 UTC
 * 
 */

/* JavaScript display functions for displaying data */
window.songDetail = false;
window.searching = false;

/* Objects */
/* RegularWrites object contains the functions necessary to do the non-logged in versions of line writes */
window.RegularWrites = function () {
    /**
     * Function: writeList
     * Description: writes the list data to the screen
     * Arguments: d = data object
     * Returns: nothing
     */
    this.writeList = function (d) {
        if (typeof d != "object") { return false; }
        var html = "";
        var curLet = ""; /* current letter */
        var letters = []
        var idLet = "0";
        var line = "";
        var s = d["data"];

        for (var i=0; i < s.length; i++) {
            switch (d.type) {
                case "songs":
                    line = s[i].sorted;
                    break;
                case "authors":
                    if (d.sort == "last") {
                        line = s[i].last;
                    } else if (d.sort == "first") {
                        line = (s[i].first == "") ? "#" : s[i].first;
                    }
                    break;
            }
            if (curLet != getFirstChar(line)) {
                curLet = getFirstChar(line);
                letters[letters.length] = curLet
                idLet = (curLet == "#") ? "0" : curLet;
                html += "<li id=\"loc_" + idLet + "\" class=\"newsection\">"
                html += curLet + "</li>"
            } 
            if (d.type == "songs") {
                html += "<li class=\"item item_nav\" data-songid=\"" + s[i].id + "\">"
                if (s[i].aka == 1) {
                    html += "<i>" + s[i].title + "</i> (" + s[i].official;
                } else {
                    /* write out authors */
                    a = "";
                    for (var j=0; j<s[i].authors.length; j++) {
                        a += s[i].authors[j].last + ", ";
                    }
                    a = a.substr(0, a.length-2)
                    html += s[i].title + " (" + a;
                }
                if (s[i].ccli != '') {
                    html += "; " + s[i].ccli;
                }
                html += ")</li>";
            } else if (d.type == "authors") {
                layerId = s[i].last.replace("'","");
                layerId = layerId.replace(",","");
                layerId = layerId.replace(".","");
                layerId = layerId.replace(" ","_");
                layerId = layerId + "_" + randomID(7);
                html += "<li class=\"item\">"
                html += "<div id=\"" + layerId  + "\" class=\"item_dropdown\">";
                if (d.sort == "first") {
                    html += s[i].first + " " + s[i].last;
                } else if (d.sort == "last") {
                    html += s[i].last + ", " + s[i].first;
                }
                html += "</div>\n";
                html += "<ul id=\"" + layerId + "_content\" class=\"subline\">";
                s[i].songs.sort((a,b) => {
                    if (a.sorted < b.sorted) { return -1; }
                    if (a.sorted > b.sorted) { return 1; }
                    return 0;
                });
                for (var j=0; j < s[i].songs.length; j++) {
                    html += "<li class=\"item_nav\" data-songid=\""
                    html += s[i].songs[j].id + "\">";
                    html += s[i].songs[j].title
                    html += (s[i].songs[j].ccli != '') ? " (" + s[i].songs[j].ccli + ")" : ""
                    html += "</li>";
                }
                html += "</ul></li>";
            }
        
        }
        $("#datacontainer").html(html);
        $(".item_dropdown").on("click", function(i) { authXpand(this.id) });
        
        /* enable clicking */
        $(".item_nav").on("click", function () {
            window.location.hash = "song:" + $(this).data("songid");
        });    
        
        drawJumper(letters);
        $("#dbplaceholder").fadeOut();
        $("#dblist").fadeIn();
        $("#dblist").scrollTo(0);
        $("#dbicon").fadeOut();
        $("#loadingspinner").fadeOut();

    }

    /** 
     * Function: writeResults
     * Description: writes the search results to the interface
     * Arguments: d = data object
     * Returns: nothing
     */
    this.writeResults = function (d) {
        var i = 0;
        if (typeof d.results == "undefined") { return false; }
        html = "<h1>" + window.lang.searchResults.title + "</h1>\n";
        if (d.results < 0) {
            html += "<p>" + window.lang.searchResults.no_results + "</p>";
        } else {
            var termsarray = d.terms.split(" ");
            var termtext = window.lang.searchResults.term_text_sing;
            var termlist = "<i>" + d.terms + "</i>";
            if (termsarray.length > 1) {
                termlist = "";
                termtext = window.lang.searchResults.term_text_plur;
                for (i=0; i < termsarray.length; i++) {
                    termlist += "<i>" + termsarray[i];
                    if (i == (termsarray.length - 2)) {
                        termlist += "</i> " + window.lang.searchResults.term_text_and + " ";
                    } else if (i == (termsarray.length - 1)) {
                        termlist += "</i>";
                    } else {
                        termlist += "</i>, ";
                    }
                }
            }
            
            html += "<p>" + window.lang.searchResults.results_text.replace("%r%", d.terms.length);
            html += " " + termtext + " " + termlist +".</p>";
            if (d.results > 0) {
                if (d.songs.length > 0) {
                    s = d.songs;
                    html += "<h2 class=\"search-results\">" + window.lang.searchResults.songs + "</h2>";
                    html += "<ul>";
                    for (i=0; i < s.length; i++) {
                        html += "<li class=\"item item_nav\" data-songid=\"" + s[i].id + "\">"
                        if (s[i].aka == 1) {
                            html += "<i>" + s[i].title + "</i> (" + s[i].official;
                        } else {
                            /* write out authors */
                            a = "";
                            for (var j=0; j<s[i].authors.length; j++) {
                                a += s[i].authors[j].last + ", ";
                            }
                            a = a.substr(0, a.length-2)
                            html += s[i].title + " (" + a;
                        }
                        if (s[i].ccli != '') {
                            html += "; " + s[i].ccli;
                        }
                        html += ")</li>";
                    }
                    html += "</ul>";
                }
                if (d.authors.length > 0) {
                    s = d.authors;
                    html += "<h2 class=\"search_results\">" + window.lang.searchResults.authors + "</h2>";
                    html += "<ul>";
                    for (i=0; i < s.length; i++) {
                        layerId = s[i].last.replace("'","");
                        layerId = layerId.replace(",","");
                        layerId = layerId.replace(".","");
                        layerId = layerId.replace(" ","_");
                        layerId = layerId + "_" + randomID(7);
                        html += "<li class=\"item\">"
                        html += "<div id=\"" + layerId  + "\" class=\"item_dropdown\">";
                        html += s[i].first + " " + s[i].last;
                        html += "</div>\n";
                        html += "<ul id=\"" + layerId + "_content\" class=\"subline\">";
                        if (s[i].songs.length > 1){
                            s[i].songs.sort((a,b) => {
                                if (a.sorted < b.sorted) { return -1; }
                                if (a.sorted > b.sorted) { return 1; }
                                return 0;
                            });
                        }
                        for (var j=0; j < s[i].songs.length; j++) {
                            html += "<li class=\"item_nav\" data-songid=\""
                            html += s[i].songs[j].id + "\">";
                            html += s[i].songs[j].title
                            html += (s[i].songs[j].ccli != '') ? " (" + s[i].songs[j].ccli + ")" : ""
                            html += "</li>";
                        }
                        html += "</ul></li>";
                    }
                    html += "</ul>";
                }
            }
        }
        $("#results").html(html);

        /* enable clicking */
        $(".item_dropdown").on("click", function(i) { authXpand(this.id) });
        $(".item_nav").on("click", function () {
            window.location.hash = "song:" + $(this).data("songid");
        });    
        
        $("#dbplaceholder").fadeOut();
        $("#results").fadeIn();
        $("#loadingspinner").fadeOut();
        $("#searchicon").fadeOut();
    }
}


/* Functions */
/** 
 * Function: authXpand
 * Description: expands or contracts the items underneath a given author
 * Arguments: id = layer id to expand or contract
 * Returns: nothing
 */
function authXpand(id) {
    var content = $("#" + id + "_content");
    if (content.css("display") == "none") {
        content.slideDown();
        $("#" + id).addClass("open");
    } else {
        content.slideUp();
        $("#" + id).removeClass("open");
    }
}

/**  
 * Function: doSearch
 * Description: executes the search function, then calls writeResults
 * Arguments: none
 * Returns: boolean denoting success
 */
function doSearch() {
    window.searching = true;
    $(".navbar-collapse").collapse("hide");
    q = $("#searchbox").val();
    window.location.hash = "search:" + q
    $.post("process.php", {"action":"search", "q":q}, "", "json").done(
        function (d) {
            if (typeof d.error != "undefined") {
                console.log("Search error", d.error);
                return false;
            } else {
                writeResults(d);
                return true;
            }
        }
    ).fail(function (e) { console.log("Search error", e)});
}

/** 
 * Function: drawJumper
 * Description: draws the jumper menu that goes on the right
 * Arguments: letters = array containing letters to draw into jumper
 * Returns: nothing
 */
function drawJumper(letters) {
    if (typeof letters != "object") { return false; }
    var html = "";
    for (var i=0; i < letters.length; i++) {
        
        html += "<div class=\"jumper_item\" data-target=\""
        if (letters[i] == "#") { 
            html += "0";
        } else {
            html += letters[i];
        }
        html += "\">" + letters[i] + "</div>";
    }
    $("#jumper").html(html);
    $("#jumper").fadeIn();
    $(".jumper_item").on("click", function() { 
        $("#dblist").scrollTo("#loc_" + $(this).data("target"), 500);
    });
}

/** 
 * Function: getFirstChar
 * Description: Checks the first character of the line and returns it
 * Arguments: l = line to check
 * Returns: Letter from A-Z or # other character or numeral
 */
function getFirstChar(l) {
    var c = l.substr(0,1).toUpperCase()
    if (c.charCodeAt(0) < 65 || c.charCodeAt(0) > 90) {
        return '#';
    } else {
        return c;
    }
    
}

/** 
 * Function: hashNav
 * Description: When the hash changes, navigates to the next item
 * Arguments: clearonly = boolean; whether to only load things
 * Returns: nothing
 */
function hashNav(clearonly) {
    if (typeof clearonly == 'undefined') { clearonly = false; }
    var h = window.location.hash;
    if (h == "") { h = "#list:all-titles"; }
    $("#jumper").fadeOut();
    $(".dbdata").fadeOut();
    $("#dbplaceholder").fadeIn();
    $("#loadingspinner").fadeIn();
    /* exit function if we are only clearing things to load the next page */
    if (clearonly == true) { return true; }
    if (h.substr(1,5) == "song:") {
        window.songDetail = true;
        loadFromDb("songs", "details", h.substr(6));
    } else if (h.substr(1,7) == "search:") {
        $("#searchicon").fadeIn();
        if (window.searching == false) { 
            $("#searchbox").val(decodeURIComponent(h.substr(8)));
            doSearch();
        } else if (window.searching == true) {
            window.searching = false;
            $("#dbplaceholder").fadeOut();
            $("#results").fadeIn();
            $("#loadingspinner").fadeOut();
            $("#searchicon").fadeOut();
        }
        /* everything else is handled by writeResult */
    } else if (h.substr(1,5) == "list:") {
        if (window.songDetail) {
            window.songDetail = false;
            
            if ($("#sort_order").html() == "Sorted by:") {
                hashNav();
                return;
            }
            $("#dblist").fadeIn();
            $("#dbplaceholder").fadeOut();
            $("#loadingspinner").fadeOut();     
            $("#jumper").fadeIn();
        } else {
            switch (h.substr(6)) {
                case "all-titles":
                    loadFromDb('songs','all',0);
                    break;
                case "official-titles":
                    loadFromDb('songs','official',0);
                    break;
                case "authors-last":
                    loadFromDb('authors','last',0);
                    break;
                case "authors-first":
                    loadFromDb('authors','first',0);
                    break;
            }
        }
    } else if ($(h).length < 1) {
        console.log("div " + h + " doesn't exist");
        window.location.hash = "list:all-titles";
    } else {
        loadPage(h);
    }
}

/** 
 * Function: loadPage
 * Definition loads the passed page into view
 * Arguments: pg = page div id; defaults to 'about';
 * Returns: nothing 
 */
function loadPage(pg) {
    if (typeof pg == "undefined") { pg = "#about"; }
    $("#dbplaceholder").fadeOut();
    $("#loadingspinner").fadeOut();
    $(pg).fadeIn();
}

/**
 * Function: setLegend
 * Description: Writes the proper message in the #legend field
 * Arguments: secType = "songs" or "authors"
 *            sortType = for songs: "all" or "original"
 *                       for authors: "first" or "last"
 * Returns: nothing
 */
function setLegend(secType, sortType) {
    if (typeof secType == "undefined" || typeof sortType == "undefined") { return false;}
    var html =""
    switch (secType) {
        case "songs":
            switch (sortType) {
                case "all":
                    html = window.lang.legend.songs_all
                    break;
                case "original":
                    html = window.lang.legend.songs_original;
                    break;
            }
            break;
        case "authors":
            switch (sortType) {
                case "first":
                    html = window.lang.legend.authors_first;
                    break;
                case "last":
                    html = window.lang.legend.authors_last;
                    break;
            }
            break;
    }
    $("#legend").html(html);
}

/**  
 * Function: writeDbData
 * Description: writes the database response to the page
 * Arguments: d = returned data
 * Returns: nothing
 */
function writeDbData(d) {
    /* Write out sort order */
    switch(d.type) {
        case "authors":
            switch(d.sort){
                case "first":
                    $("#sort_order").html(window.lang.sortOrder.authors_first);
                    break;
                case "last":
                    $("#sort_order").html(window.lang.sortOrder.authors_last);
                    break;
            }
            break;
        
        case "details":
            writeSongDetails(d);
            return true; 
            break;
        
        case "songs":
            switch (d.sort){
                case "all":
                    $("#sort_order").html(window.lang.sortOrder.songs_all);
                    break;
                case "official":
                    $("#sort_order").html(window.lang.sortOrder.songs_official);
                    break;
            }
            break;
    }
    setLegend(d.type, d.sort);
    writeList(d);
}

/** 
 * Function: refreshSongs
 * Description: triggers a refresh of the songs from the database
 * Arguments: none
 * Returns: nothing
 */
function refreshSongs() {
    window.songDetail = false;
    window.search = false;
    var hash = window.location.hash
    if (hash.substr(1,4) == "song") {
        window.location.hash = "#list:all-titles";
    } else {
        hashNav();
    }
    $("#refreshdb").css("display", "none");
}

/**
 * Function: writeSongDetails
 * Description: writes the details data to the screen
 * Arguments: d = data object
 * Returns: nothing
 */
function writeSongDetails(d) {
    if (d.data.length <1) {
        window.location.hash="#all-titles";
        return false;
    }
    if (typeof d != "object") { return false; }

    var a = "";
    var date = new Date(d.data.modified*1000);
    var html = "<h1 id=\"details_title\">" + d.data.title + "</h1>";
    html += "<p><b>" + window.lang.details.file_name + ":</b> <code id=\"details_fn\">" + d.data.filename + "</code></p>";
    html += "<h2>" + window.lang.details.lyrics + "</h2>";
    html += d.data.lyrics;
    if (d.data.chords) { html += "<p class=\"mt-4 text-italic\">" + window.lang.details.chords + "</p>\n"; }
    html += "<h2 class=\"mt-4\">" + window.lang.details.pres_order + "</h2>\n<p>";
    html += (d.data.present == "") ? window.lang.details.not_def : d.data.present;
    html += "</p>\n<h2>" + window.lang.details.details + "</h2>";
    html += "<div id=\"more_details\">"
    for (var i=0; i < d.data.authors.length; i++) {
        a += d.data.authors[i].first + " " + d.data.authors[i].last + ", ";   
    }
    html += "<p><b>" + window.lang.details.authors +":</b> " + a.substr(0,(a.length-2)) + "</b>";
    if (d.data.copyright == "") { d.data.copyright = "&copy; " + a.substr(0,(a.length-2)); }
    html += "<p><b>" + window.lang.details.copyright + ":</b> " + d.data.copyright + "</p>";
    html += "<p><b>" + window.lang.details.ccli + ":</b> ";
    if (d.data.ccli != '') {
        html += "<a href=\"https://us.search.ccli.com/songs/" + d.data.ccli + "\" target =\"_blank\" id=\"details_ccli\">";
        html += d.data.ccli + "</a>";
    } else {
        html += "<span id=\"details_ccli\"></span>";
        html += window.lang.details.no_ccli;
    }
    html += "</p>";
    html += "<p><b>" + window.lang.details.alt_titles + ":</b> ";
    html += (d.data.aka.length < 1) ? window.lang.details.no_alt_titles : d.data.aka;
    html += "</p>";
    html += "<p><b>" + window.lang.details.last_update + ":</b> " + date.toUTCString() + "</p>";  
    html += "</div>";
    
    $("#itemdetail_content").html(html);
    $("#dbicon").fadeOut();
    $("#loadingspinner").fadeOut();
    $("#dbplaceholder").fadeOut();
    $("#itemdetail").fadeIn();
    $("#itemdetail").scrollTo(0); /* make sure that we're always at the top of the item detail */
}

/* connect RegularWrites functions to window-level functions */
window.regWrites = new RegularWrites();
window.writeList = regWrites.writeList;
window.writeResults = regWrites.writeResults;
