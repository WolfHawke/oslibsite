/**
 * OpenSong Library Online
 * 
 * version: 2.0
 * file version: 2.0.2
 * updated: 2021-06-18 | 08:30 UTC
 * 
 */

/** JavaScript object to handle adding a song to the library */

/* create AddSong object */
window.AddSong = function (_parent) {
    /* properties */
    this.curTag = {"B":0, "C":0, "P":0, "T":0, "V":0}

    /* self-reference */
    var _asme = this;

    /* public methods */
    /**
     * Function: addSection
     * Description: adds a section tag to the lyrics field
     * Arguments: s = section type (valid: B, C, P, T, V)
     * Returns: nothing
     */
    this.addSection = function (s) {
        if (typeof s == "undefined" || (! /[BCPTV]/.test(s))) { return false; }
        var areaId = "new_song_lyrics";
        var tag = ""
        _asme.curTag[s]++;

        if (/[BCPT]/.test(s) && _asme.curTag[s] < 3) {
            if (_asme.curTag[s] == 1) {
                tag = "[" + s + "]\n";
            } else if (_asme.curTag[s] == 2) {
                $("#" + areaId).val($("#" + areaId).val().replace("[" + s + "]", "[" + s + "1]"))
                tag = "[" + s + "2]\n";
            }
        } else {
           tag = "[" + s + _asme.curTag[s] + "]\n";
        }
        _asme.insertAtCaret(areaId, tag);
    }

    /**
     * Function: formatLyrics
     * Description: formats the lyrics to work properly with the OpenSong format
     * Arguments: none
     * Returns: nothing
     */
    this.formatLyrics = function () {
        var lyrics = "";
        var lyricArr = $("#new_song_lyrics").val().split("\n");
        for (var i=0; i < lyricArr.length; i++) {
            lyrics += (/^[\[\.;\-\ 0-9]/.test(lyricArr[i])) ? "" : " ";
            lyrics += lyricArr[i] + "\n";
        }

        $("#new_song_lyrics").val(lyrics);
    }

    /**
     * Function: insertAtCaret
     * Description: Inserts text at the caret position in a given textbox
     *              modded to work with jQuery from: https://stackoverflow.com/questions/1064089/inserting-a-text-where-cursor-is-using-javascript-jquery
     * Arguments: areaId = id of textarea
     *            text = text to insert
     * Returns: nothing
     */ 
    this.insertAtCaret = function (areaId, text) {
        /* strip hash from front of areaId */
        if (areaId.indexOf("#") == 0 ) {
            areaId = areaId.substring(1);
        }

        var txtarea = document.getElementById(areaId);
        if (!txtarea) {
            return;
        }

        var scrollPos = txtarea.scrollTop;
        var strPos = 0;
        var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
            "ff" : (document.selection ? "ie" : false));
        if (br == "ie") {
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart('character', -txtarea.value.length);
            strPos = range.text.length;
        } else if (br == "ff") {
            strPos = txtarea.selectionStart;
        }

        var front = (txtarea.value).substring(0, strPos);
        var back = (txtarea.value).substring(strPos, txtarea.value.length);
        txtarea.value = front + text + back;
        strPos = strPos + text.length;
        if (br == "ie") {
            txtarea.focus();
            var ieRange = document.selection.createRange();
            ieRange.moveStart('character', -txtarea.value.length);
            ieRange.moveStart('character', strPos);
            ieRange.moveEnd('character', 0);
            ieRange.select();
        } else if (br == "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
        }

        txtarea.scrollTop = scrollPos;
    }

    /**
     * Function: saveSong
     * Description: saves song to server
     * Arguments: none
     * Returns: nothing
     */
    this.saveSong = function () {
        /* hide response divs */
        $("#add_song_err_form").slideUp();
        $("#add_song_err_save").slideUp();
        $("#add_song_success").slideUp();
        
        /* validate fields */
        if ($("#new_song_title").hasClass("is-invalid") || $("#new_song_author").hasClass("is-invalid") || $("#new_song_ccli").hasClass("is-invalid") || $("#new_song_present").hasClass("is-invalid")) {
            $("#addNewSong").scrollTop();
            $("#add_song_err_form").slideDown();
            return false;
        }

        /* enable spinner */
        $("#newsong_save_spinner").show();
        $("#newsong_save_text").hide();
        $("#btn_new_song_save").attr("disabled", true);

        /* build items */
        var d = {
            "action": "saveSong",
            "title": smartquotes($("#new_song_title").val()),
            "authors": smartquotes($("#new_song_author").val()),
            "ccli": $("#new_song_ccli").val(),
            "copyright": smartquotes($("#new_song_copyright").val()),
            "lyrics": smartquotes($("#new_song_lyrics").val()),
            "presOrder": $("#new_song_present").val(),
            "source": $("#new_song_src").val(),
            "key": _parent.seckey,
        }

        /* submit for saving */
        $.post("process-secured.php", d, "", "json").done(
            function (d) {
                /* disable spinner */
                $("#newsong_save_text").show();
                $("#newsong_save_spinner").hide();
                $("#btn_new_song_save").attr("disabled", false);
                $("#addNewSong").scrollTo(0);
                if (typeof d.error != "undefined") {
                    $("#add_song_err_save").slideDown();
                    return false;                   
                } else if (typeof d.success != "undefined") {                    
                    $("#new_song_success_name").html(d.success.songName);
                    $("#add_song_success").slideDown();
                    setTimeout(function () { $("#add_song_success").slideUp(); }, 10000 ); /* hide success message after 10 sec */
                    /* add song to set, if so desired */
                    if ($("#new_song_to_set").is(":checked")) {
                        sd = {
                            'ccli' : $("#new_song_ccli").val(),
                            'fn': d.success.fileName,
                            'title' : d.success.songName,
                            'songid' : 0,
                            'songtype' : 'new',
                        };
                        _parent.sets.addToSet(sd);
                    } else {
                        $("#new_song_to_set").attr("checked", true);
                    }
                    
                    /* reset all fields */
                    $(".form-control").val("");
                    $(".form-check").removeClass("is-invalid");

                    _parent.seckey = d.success.seckey;
                }
            }
        ).fail(function (e) { 
            console.log("saveSong error: ", e); 
            /* disable spinner */
            $("#newsong_save_text").show();
            $("#newsong_save_spinner").hide();
            $("#btn_new_song_save").attr("disabled", false);
        });   
    }

    /**
     * Function: validateAuthors
     * Description: validates if Authors string is valid
     * Arguments: none
     * Returns: boolean denotig validity
     */
    this.validateAuthors = function() {
        var a = $("#new_song_author");
        a.removeClass("is-invalid");
        $("#new_song_ccli").removeClass("is-invalid");
        if (!$("#new_song_author_label").hasClass("text-bold")) { $("#new_song_author_label").addClass("text-bold"); }
        if (!$("#new_song_ccli_label").hasClass("text-bold")) { $("#new_song_ccli_label").addClass("text-bold"); }
        if (a.val().length > 0) {
            $("#new_song_ccli_label").removeClass("text-bold");
            if(/[\\\<\>:{}\!@\`~#$%^&\*()\-_=+\[\]/]/.test(a.val())) {
                a.addClass('is-invalid');
                return false;
            }
        } 
        return true;
    }

    /**
     * Function: validateCCLI
     * Description: validates if CCLI and author are both empty
     * Arguments: none
     * Returns: boolean denoting validity
     */
    this.validateCCLI = function () {
        var c = $("#new_song_ccli");
        var a = $("#new_song_author");
        c.removeClass("is-invalid");
        a.removeClass("is-invalid");
        if (a.length < 1 && (c.val() == "" || parseInt(c.val()) < 1)) {
            c.addClass("is-invalid");
            if (! a.hasClass("is-invalid")) { a.addClass("is-invalid"); }
            if ($("#auth_err").css("display") != "none") { $("#auth_err").hide(); }
            return false;
        } else if ($("#new_song_author_label").hasClass("text-bold") && a.val().length < 1) {
            $("#new_song_ccli_label").addClass("text-bold");
            $("#new_song_author_label").removeClass("text-bold");
        } 
        return true;
    }

    /**
     * Function: validatePresOrder
     * Description: validates the Presentation order to see if it only contains what it should
     * Arguments: none
     * Returns: nothing
     */
    this.validatePresOrder = function () {
        var p = $("#new_song_present");
        var po = p.val().toUpperCase();
        p.removeClass("is-invalid");
        if (p.val().length > 0) {
            if (!/^[BCPTV0-9 ]+$/.test(po)) {
                p.addClass("is-invalid");
                return false;
            } else {
                p.val(po);
            }
        }
        return true;
    }

    /**
     * Function: validateTitle
     * Description: validates the title of the song
     * Arguments: none
     * Returns: nothing
     */
    this.validateTitle = function () {
        var t = $("#new_song_title");
        t.removeClass("is-invalid");
        if (/[\<\>\=\{\}]/.test(t.val()) || t.val().length < 1) {
            t.addClass("is-invalid");
        }
    }

    /* private methods */
    /* link actions to events */
    $(".os-item-add").click(function () { _asme.addSection($(this).data("type")); });
    $("#new_song_copyright_btn").click(function () {
        $("#new_song_copyright").val("© " + $("#new_song_copyright").val());
    });
    $("#new_song_lyrics").blur(_asme.formatLyrics);
    $("#new_song_title").blur(_asme.validateTitle);
    $("#new_song_author").blur(_asme.validateAuthors);
    $("#new_song_ccli").blur(_asme.validateCCLI);
    $("#new_song_present").blur(_asme.validatePresOrder);
    $("#btn_new_song_save").click(_asme.saveSong);
}

