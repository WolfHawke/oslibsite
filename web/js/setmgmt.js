/**
 * OpenSong Library Online
 * 
 * version: 2.0
 * file version: 2.0.1
 * updated: 2021-06-18 | 07:33 UTC
 * 
 */

/* JavaScript display functions for managing sets */

/* Object */
/* Sets object contains functions for managing the sets */
window.oslibSets = function (_parent) {
    /* properties */
    this.loadDetails = true;
    this.songsInSet = 0;
    this.unsaved = false;
    this.loggingOut = false;
    this.oldSetSongs = [];

    /* self-reference */
    var _sme = this;

    /* methods */
    /* public */
    /**
     * Function: addToSet
     * Description: adds a given song to the set
     * Arguments: d = data object or "details"
     * Returns: nothing
     */
    this.addToSet = function (d) {
        var html;
        var fieldId = randomID(7, "decimal");
        
        _sme.unsaved = true;

        if (d == "details") {
            sd = {
                'ccli' : $("#details_ccli").html(),
                'fn': $("#details_fn").html(),
                'title' : $("#details_title").html(),
                'songid' : window.location.hash.substr(window.location.hash.indexOf(":")+1),
                'songtype' : 'procd',
            };
        } else if (typeof d != 'object') {
            return false;
        } else {
            sd = d;
        }
        if (typeof sd['songtype'] == "undefined") { sd['songtype'] = 'procd'; }

        _sme.songsInSet++;

        /* write out added song to current set page */
        html = "<li class=\"set-row\" id=\"cur_set_" + fieldId + "\" ";
        html += "data-id=\"" + sd['songid'] + "\" ";
        html += "data-fn=\"" + sd['fn'] + "\" ";
        html += "data-title=\"" + sd['title'] + "\" ";
        html += "data-ccli=\"" + sd['ccli'] + "\" ";
        html += "data-songtype=\"" + sd['songtype'] + "\" >\n";
        html += "<div class=\"set-del-item\" title=\"" + _parent.lang.sets.item_del_btn;
        html += "\" data-target=\"cur_set_" + fieldId + "\"><i class=\"far fa-trash-alt\"></i></div>\n";
        if (sd['songtype'] == 'new') {
            html += "<i class=\"fas fa-file-music mr-1\" title=\"" + _parent.lang.sets.item_song_new + "\"></i>\n";
        } else {
            html += "<i class=\"far fa-file-music mr-1\" title=\"" + _parent.lang.sets.item_song_procd + "\"></i> ";
        }
        html += sd['title'];
        if (sd['ccli'] != "") {
            html += " (" + sd['ccli'] + ")";
        }
        html +="</li>\n";
        $("#curset_sortable").append(html);
        $("#curset_empty").hide();

        if ($("#set_title_display_txt").html() == "") {
            now = new Date();
            ds = now.getFullYear() + "-";
            ds += ((now.getMonth()+1) < 10) ? "0" : "";
            ds += (now.getMonth()+1) + "-";
            ds += (now.getDate() < 10 ) ? "0" : "";
            ds += now.getDate();
            $("#set_title_display_txt").html(ds);
            $("#set_title").val(ds);
        }

        /* item removal code */
        $(".set-del-item").click(function () {
            $("#" + $(this).data("target")).remove();
            if ($("#curset_sortable").children().length < 1) {
                $("#curset_empty").show();
                _sme.songsInSet--;
                if (_sme.songsInSet < 1) {
                    $("#items_in_set").html ("");
                    $("#sets_badge").fadeOut();
                    _sme.unsaved = false;
                } else {
                    $("#items_in_set").html("(" + _sme.songsInSet) + ")";
                    $("#sets_badge").html(_sme.songsInSet);
                }
            }
        });

        $("#sets_badge").html(_sme.songsInSet);
        
        if (_sme.songsInSet > 0) {
            $("#items_in_set").html ("(" + _sme.songsInSet + ")");
            $("#sets_badge").fadeIn();
        } else {
            $("#items_in_set").html ("");
            $("#sets_badge").fadeOut();
            _sme.unsaved = false;
        }

    }

    /** 
     * Function: closeExistingSet
     * Description: animation to close existing set window on mobile view
     * Arguments: none
     * Returns: nothing
     */
    this.closeExistingSet = function () {
        /* only trigger at less than md */
        if ($(window).width() >= 997) { return false; }

        var keepGoing = true;
        var d = parseInt($("#oldsets_list").css("left"));
        var w = parseInt($("#oldsets_details").css("width"));
        var jump = 50;
        od = d + jump;
        if (od > 0) {
            ol = w;
            keepGoing = false;
            od = 0;
        } else {
            ol = w+od;
        }
        $("#oldsets_details").css("left", ol + "px");
        $("#oldsets_list").css("left", od + "px"); 
        if (keepGoing) {
            setTimeout(_sme.closeExistingSet, 10);
        } 
    }

    /** 
     * getSets: gets all the existing sets from the server
     * Returns: boolean denoting success or failure
     */
    this.getSets = function () {
        $("#sets_icon").fadeIn();
        $("#oldset_list_container").html("");
        var d = {
            "action": "getAllSets",
            "key": _parent.seckey,
        }
        $.post("process-secured.php", d, "", "json").done(
            function (d) {
                if (typeof d.error != "undefined") {
                    console.log(d.error);
                } else {
                    var html = "";
                    for (var i=0; i < d.sets.length; i++) {
                        html += "<li class=\"oldset-item\" data-fn=\"" + d.sets[i].name + "\" data-fid=\"" + d.sets[i].id + "\"";
                        html += "><i class=\"far fa-list-music\"></i> " + d.sets[i].name + "</li>";
                    }
                    $("#oldset_list_container").html(html);
                    $("#allSets").fadeIn();
                    $("#dbplaceholder").fadeOut();
                    $("#loadingspinner").fadeOut();
                    $("#sets_icon").fadeOut();
                    $(".oldset-item").click(function () { _sme.loadExistingSet($(this).data('fn'), $(this).data('fid'))});
                    _parent.seckey = d.seckey;
                }
            }
        ).fail(function (e) { console.log("getSets error", e)});
        return true;
    }

    /** 
     * loadExistingSet : triggers the load of an exisiting set
     * Returns: boolean denoting success
     * 
     * @var string  set file name 
     * @var integer set numerical pCloud id
     */
    this.loadExistingSet = function (fn, id) {
        $("#oldsets_icon").fadeIn();
        $("#oldsets_spinner").fadeIn();

        /* hide old set if we are in desktop mode */
        if ($(window).width() >= 997) {
            $("#oldsets_details").fadeOut();
        } 
        $("#oldsets_details_title").html(fn);

        var d = {
            "action": "loadSet",
            "sn": id,
            "key": _parent.seckey
        }
        $.post("process-secured.php", d, "", "json").done(
            function(d) {
                _sme.writeExistingSet(d)
            }
        ).fail(function (e) { console.log("loadExistingSet error", e); });

    }

    /**
     * loadSetSong: displays a song from the set
     * 
     * @var integer song id
     */
    this.loadSetSong = function (sid) {
        _parent.setSong = true;
        window.location.hash = "#song:" + sid;
    }

    /**
     * newSet:  creates a new set
     * Returns: nothing
     * 
     * @var boolean allows for deletion
     */
    this.newSet = function (c) {
        if (typeof c == "undefined") { c = false; }
        if (_sme.unsaved == false || c == true) {
            _sme.unsaved = false;
            _sme.songsInSet = 0;
            $("#set_title_display_txt").html("");
            $("#set_title").val("");
            $("#sets_badge").html("");
            $("#sets_badge").fadeOut();
            $("#items_in_set").html("");
            $("#curset_sortable").html("");
            $("#curset_empty").show();
            $("#cursetNoSaveExitConfirmModal").modal("hide");
            /* reroute to logout function, if we were in the process of logging out */
            if (_sme.loggingOut) {
                _parent.doLogout();
            }
        } else {
            $("#cursetNoSaveNewConfirmModal").modal();
        }
    }

    /**
     * oldSetToCurSet: loads the songs from the old set into the current set
     * Returns: nothing
     */
    this.oldSetToCurSet = function () {
        var s = {'ccli' : "", 'fn': "", 'title': "",
        'songid' : "",'songtype' : "" }

        /* clear the current set */
        _sme.newSet(true);

        for (var i=0; i<_sme.oldSetSongs.length; i++) {
            s.ccli = _sme.oldSetSongs[i].ccli;
            s.fn = _sme.oldSetSongs[i].name;
            s.title = (_sme.oldSetSongs[i].title == "") ? s.fn : _sme.oldSetSongs[i].title;
            s.songid = _sme.oldSetSongs[i].id;
            s.songtype = (s.songid > 0) ? "procd" : "new";
            _sme.addToSet(s);
        }
        $("#set_title_display_txt").html($("#oldsets_details_title").html());
        $("#set_title").val($("#oldsets_details_title").html());
        $("#loadExistingSetModal").modal("hide");
        window.location.hash = "#currentSet";
    }

    /**
     * openExistingSet: animation to open the set window when in mobile size
     * Returns: nothing
     */
    this.openExistingSet = function () {
        /* only trigger at less than md */
        if ($(window).width() >= 997) { return false; }

        var keepGoing = true;
        var l = parseInt($("#oldsets_details").css("left"));
        var w = parseInt($("#oldsets_list").css("width"));
        var jump = 50;
        od = l-jump;
        if (od < 0) {
            od = 0;
            keepGoing = false;
            ol = (-1)*w;
        } else {
            ol = (-1)*(w-od);
        }
        $("#oldsets_details").css("left", od + "px");
        $("#oldsets_list").css("left", ol + "px"); 
        if (keepGoing) {
            setTimeout(_sme.openExistingSet, 10);
        }
    }

    /**
     * saveSet: saves the current set
     * Returns: nothing
     */
    this.saveSet = function () {
        $("#cur_set_save_error").slideUp();
        $("#cur_set_save_success").slideUp();

        $("#save_set_icon").hide();
        $("#save_set_spinner").show();

        var list = $("#curset_sortable").sortable("toArray");
        var data = {"action" : "saveSet", 
                 "key": _parent.seckey,
                 "fn": $("#set_title_display_txt").html(), 
                 "sets": [],
                }

        if (list.length > 0){    
            for (var i=0; i < list.length; i++) {
                data.sets[i] = $("#" + list[i]).data();
            }
        } else {
            return false;
        }

        $.post("process-secured.php", data, "", "json").done(
            function (d) {
                $("#save_set_icon").show();
                $("#save_set_spinner").hide();
                if (typeof d.success != "undefined") {
                    $("#cur_set_save_success").slideDown();
                    setTimeout(function() { $("#cur_set_save_success").slideUp(); }, 10000);
                    _parent.seckey = d.success;                    
                } else if (typeof d.error != "undefined") {
                    $("#cur_set_save_error").slideDown();
                }
            }
        ).fail(function (e) { 
            console.log("saveSet error", e); 
            $("#save_set_icon").show();
            $("#save_set_spinner").hide();
        });

        _sme.unsaved = false;
        /* make sure we're not logging out, if we were */
        _sme.loggingOut = false;
    }

    /**
     * setsResize: handles window resizing functions for sets screen
     * Returns: nothing
     */
    this.setsResize = function () {
        $("#oldsets_details").removeAttr("style");
        $("#oldsets_list").removeAttr("style");
        calcViewHeight();
    }

    /**
     * writeExistingSet: writes the data from the existing set
     * Returns: nothing
     * 
     * @var array  contains set item objects
     */
    this.writeExistingSet = function (s) {
        var html = "";
        var icons = {"song": "music", "scripture": "bible",
                     "custom": "rectangle-landscape",
                     "style": "paint-brush-alt",
                     "image": "image", "external": "browser" };

        if (typeof s.error != "undefined") {
            $("#oldset_file_error").show();
        } else {
            for (var i=0; i < s.length; i++) {
                html += "<li"
                html += (s[i].type == "song" && s[i].id > 0) ? " class=\"oldset-song\" data-target=\"" + s[i].id + "\"" : "";
                html += "><i class=\"far fa-" + icons[s[i].type] + "\" ";
                html += "title=\"" + _parent.lang.sets.sets_items[s[i].type] + "\"></i> ";
                html += (s[i].type == 'style') ? _parent.lang.sets.change_style : s[i].name;
                if (s[i].type == "song") { 
                    _sme.oldSetSongs[_sme.oldSetSongs.length] = s[i];
                } else if (s[i].type == 'scripture' || s[i].type == 'custom') {
                    html += " (" + s[i].slides + " ";
                    html += (s[i].slides == 1) ? _parent.lang.sets.sets_items.custom.toLowerCase() : _parent.lang.sets.sets_items.slides.toLowerCase();
                    html += ")";
                } else if (s[i].type == 'image') {
                    html += " (" + s[i].slides + " ";
                    html += (s[i].slides == 1) ? _parent.lang.sets.sets_items.image.toLowerCase() : _parent.lang.sets.sets_items.images.toLowerCase();
                    html += ")";
                } else if (s[i].type == 'style') {
                    
                }
                html+= "</li>\n";
            }
        }
        $("#oldsets_details_content").html(html);
        $(".oldset-song").click( function () { _sme.loadSetSong($(this).data("target")); });

        $("#oldsets_icon").fadeOut();
        $("#oldsets_spinner").fadeOut();

        /* open written set */
        $("#oldsets_details").fadeIn();
        if ($(window).width() < 997) {
            _sme.openExistingSet();
        }
    }

    /**
     * Function: writeList
     * Description: writes the list data to the screen
     * Arguments: d = data object
     * Returns: nothing
     */
    this.writeList = function (d) {
        if (typeof d != "object") { return false; }
        var html = "";
        var curLet = ""; /* current letter */
        var letters = []
        var idLet = "0";
        var line = "";
        var s = d["data"];
        var a = "";

        for (var i=0; i < s.length; i++) {
            switch (d.type) {
                case "songs":
                    line = s[i].sorted;
                    break;
                case "authors":
                    if (d.sort == "last") {
                        line = s[i].last;
                    } else if (d.sort == "first") {
                        line = (s[i].first == "") ? "#" : s[i].first;
                    }
                    break;
            }
            if (curLet != getFirstChar(line)) {
                curLet = getFirstChar(line);
                letters[letters.length] = curLet
                idLet = (curLet == "#") ? "0" : curLet;
                html += "<li id=\"loc_" + idLet + "\" class=\"newsection\">"
                html += curLet + "</li>"
            } 
            if (d.type == "songs") {
                html += "<li class=\"item item_nav\" data-songid=\"" + s[i].id + "\">"

                /* here is where this function is different: add to set button */
                html += "<button class=\"btn btn-sm btn-secondary set-btn\" title=\"" + _parent.lang.sets.add_btn_lbl + "\" ";
                if (s[i].aka == 1) {
                    html += "onclick=\"return false;\" data-title=\"" + s[i].official + "\" ";
                } else {
                    html += "onclick=\"return false;\" data-title=\"" + s[i].title + "\" ";
                }
                html += "data-fn=\"" + s[i].filename + "\" data-ccli=\"" + s[i].ccli + "\" ";
                html += "data-songid=\"" + s[i].id + "\"><i class=\"far fa-plus\"></i>";
                html += "<i class=\"far fa-list-music\"></i></button>&nbsp;&nbsp;";

                if (s[i].aka == 1) {
                    html += "<i>" + s[i].title + "</i> (" + s[i].official;
                } else {
                    /* write out authors */
                    a = "";
                    for (var j=0; j<s[i].authors.length; j++) {
                        a += s[i].authors[j].last + ", ";
                    }
                    a = a.substr(0, a.length-2)    
                    html += s[i].title + " (" + a;
                }
                if (s[i].ccli != '') {
                    html += "; " + s[i].ccli;
                }
                html += ")</li>";
            } else if (d.type == "authors") {
                layerId = s[i].last.replace("'","");
                layerId = layerId.replace(",","");
                layerId = layerId.replace(".","");
                layerId = layerId.replace(" ","_");
                layerId = layerId + "_" + randomID(7);
                html += "<li class=\"item\">"
                html += "<div id=\"" + layerId  + "\" class=\"item_dropdown\">";
                if (d.sort == "first") {
                    html += s[i].first + " " + s[i].last;
                } else if (d.sort == "last") {
                    html += s[i].last + ", " + s[i].first;
                }
                html += "</div>\n";
                html += "<ul id=\"" + layerId + "_content\" class=\"subline\">";
                s[i].songs.sort((a,b) => {
                    if (a.sorted < b.sorted) { return -1; }
                    if (a.sorted > b.sorted) { return 1; }
                    return 0;
                });
                for (var j=0; j < s[i].songs.length; j++) {
                    html += "<li class=\"item_nav\" data-songid=\""
                    html += s[i].songs[j].id + "\">";
                    html += "<button class=\"btn btn-sm btn-secondary set-btn\" title=\"" + _parent.lang.sets.add_btn_lbl + "\" ";
                    html += "onclick=\"return false;\" data-title=\"" + s[i].songs[j].title + "\" ";
                    html += "data-fn=\"" + s[i].songs[j].filename + "\" data-ccli=\"" + s[i].songs[j].ccli + "\" ";
                    html += "data-songid=\"" + s[i].songs[j].id + "\"><i class=\"far fa-plus\"></i>";
                    html += "<i class=\"far fa-list-music\"></i></button>&nbsp;&nbsp;";
                    html += s[i].songs[j].title
                    html += (s[i].songs[j].ccli != '') ? " (" + s[i].songs[j].ccli + ")" : ""
                    html += "</li>";
                }
                html += "</ul></li>";
            }
        
        }
        $("#datacontainer").html(html);
        $(".item_dropdown").on("click", function(i) { authXpand(this.id) });
        
        /* enable clicking */
        $(".item_nav").on("click", function () {
            if (_sme.loadDetails) {
                window.location.hash = "song:" + $(this).data("songid");
            } else {
                _sme.loadDetails = true;
            }
        });
        $(".set-btn").click(function () { 
            _sme.loadDetails = false;
            _sme.addToSet($(this).data()); 
        });
        
        drawJumper(letters);
        $("#dbplaceholder").fadeOut();
        $("#dblist").fadeIn();
        $("#dblist").scrollTo(0);
        $("#dbicon").fadeOut();
        $("#loadingspinner").fadeOut();

    }

    /** 
     * Function: writeResults
     * Description: writes the search results to the interface
     * Arguments: d = data object
     * Returns: nothing
     */
    this.writeResults = function (d) {
        var i = 0;
        if (typeof d.results == "undefined") { return false; }
        html = "<h1>" + window.lang.searchResults.title + "</h1>\n";
        if (d.results < 0) {
            html += "<p>" + window.lang.searchResults.no_results + "</p>";
        } else {
            var termsarray = d.terms.split(" ");
            var termtext = window.lang.searchResults.term_text_sing;
            var termlist = "<i>" + d.terms + "</i>";
            if (termsarray.length > 1) {
                termlist = "";
                termtext = window.lang.searchResults.term_text_plur;
                for (i=0; i < termsarray.length; i++) {
                    termlist += "<i>" + termsarray[i];
                    if (i == (termsarray.length - 2)) {
                        termlist += "</i> " + window.lang.searchResults.term_text_and + " ";
                    } else if (i == (termsarray.length - 1)) {
                        termlist += "</i>";
                    } else {
                        termlist += "</i>, ";
                    }
                }
            }
            
            html += "<p>" + window.lang.searchResults.results_text.replace("%r%", d.terms.length);
            html += " " + termtext + " " + termlist +".</p>";
            if (d.results > 0) {
                if (d.songs.length > 0) {
                    s = d.songs;
                    html += "<h2 class=\"search-results\">" + window.lang.searchResults.songs + "</h2>";
                    html += "<ul>";
                    for (i=0; i < s.length; i++) {
                        html += "<li class=\"item item_nav\" data-songid=\"" + s[i].id + "\">"
                        html += "<button class=\"btn btn-sm btn-secondary set-btn\" title=\"" + _parent.lang.sets.add_btn_lbl + "\" ";
                        html += "onclick=\"return false;\" data-title=\"" + s[i].title + "\" ";
                        html += "data-fn=\"" + s[i].filename + "\" data-ccli=\"" + s[i].ccli + "\" ";
                        html += "data-songid=\"" + s[i].id + "\"><i class=\"far fa-plus\"></i>";
                        html += "<i class=\"far fa-list-music\"></i></button>&nbsp;&nbsp;";
                        if (s[i].aka == 1) {
                            html += "<i>" + s[i].title + "</i> (" + s[i].official;
                        } else {
                            /* write out authors */
                            a = "";
                            for (var j=0; j<s[i].authors.length; j++) {
                                a += s[i].authors[j].last + ", ";
                            }
                            a = a.substr(0, a.length-2)
                            html += s[i].title + " (" + a;
                        }
                        if (s[i].ccli != '') {
                            html += "; " + s[i].ccli;
                        }
                        html += ")</li>";
                    }
                    html += "</ul>";
                }
                if (d.authors.length > 0) {
                    s = d.authors;
                    html += "<h2 class=\"search_results\">" + window.lang.searchResults.authors + "</h2>";
                    html += "<ul>";
                    for (i=0; i < s.length; i++) {
                        layerId = s[i].last.replace("'","");
                        layerId = layerId.replace(",","");
                        layerId = layerId.replace(".","");
                        layerId = layerId.replace(" ","_");
                        layerId = layerId + "_" + randomID(7);
                        html += "<li class=\"item\">"
                        html += "<div id=\"" + layerId  + "\" class=\"item_dropdown\">";
                        html += s[i].first + " " + s[i].last;
                        html += "</div>\n";
                        html += "<ul id=\"" + layerId + "_content\" class=\"subline\">";
                        if (s[i].songs.length > 1){
                            s[i].songs.sort((a,b) => {
                                if (a.sorted < b.sorted) { return -1; }
                                if (a.sorted > b.sorted) { return 1; }
                                return 0;
                            });
                        }
                        for (var j=0; j < s[i].songs.length; j++) {
                            html += "<li class=\"item_nav\" data-songid=\""
                            html += s[i].songs[j].id + "\">";
                            html += "<button class=\"btn btn-sm btn-secondary set-btn\" title=\"" + _parent.lang.sets.add_btn_lbl + "\" ";
                            html += "onclick=\"return false;\" data-title=\"" + s[i].songs[j].title + "\" ";
                            html += "data-fn=\"" + s[i].songs[j].filename + "\" data-ccli=\"" + s[i].songs[j].ccli + "\" ";
                            html += "data-songtype=\"procd\" ";
                            html += "data-songid=\"" + s[i].songs[j].id + "\"><i class=\"far fa-plus\"></i>";
                            html += "<i class=\"far fa-list-music\"></i></button>&nbsp;&nbsp;";
                            html += s[i].songs[j].title
                            html += (s[i].songs[j].ccli != '') ? " (" + s[i].songs[j].ccli + ")" : ""
                            html += "</li>";
                        }
                        html += "</ul></li>";
                    }
                    html += "</ul>";
                }
            }
        }
        $("#results").html(html);

        /* enable clicking */
        $(".item_dropdown").on("click", function(i) { authXpand(this.id) });
        $(".item_nav").on("click", function () {
            if (_sme.loadDetails) {
                window.location.hash = "song:" + $(this).data("songid");
            } else {
                _sme.loadDetails = true;
            }
        });
        $(".set-btn").click(function () { 
            _sme.loadDetails = false;
            _sme.addToSet($(this).data()); 
        });
        $("#oldsets_details_close").click(_sme.closeExistingSet);
        $(".oldset-item").click(function () {
            loadExistingSet($(this).data("target"));
        })

         
        $("#dbplaceholder").fadeOut();
        $("#results").fadeIn();
        $("#loadingspinner").fadeOut();
        $("#searchicon").fadeOut();
    }

    /* private */
    /**
     * Function: initSets
     * Description: initializes the sets interface
     * Arguments: none
     * Returns: nothing
     */
    var initSets = function () {
        /* add add to set button to Details pane */
        var html = "<button class=\"btn btn-sm btn-secondary detail-set-btn\" title=\"" + _parent.lang.sets.add_btn_lbl + "\" ";
        html += "onclick=\"return false;\"><i class=\"far fa-plus\"></i>";
        html += "<i class=\"far fa-list-music\"></i></button>&nbsp;&nbsp;";
        $(".detail-set-btn-cntnr").html(html);
        $(".detail-set-btn").click(function () { _sme.addToSet("details"); });

        /* activate buttons on current set screen */
        $("#set_title_edit_go").click( function() {
            $("#set_title_display").hide();
            $("#set_title_edit").css("display","flex");
        });
        $("#save_set_title").click(function() {
            $("#set_title_display_txt").html($("#set_title").val());
            $("#set_title_edit").hide();
            $("#set_title_display").show();
        });
        $("#curset_sortable").sortable();
        $("#curset_sortable").disableSelection();
        $("#save_set").click(_sme.saveSet);
        $("#new_set").click(_sme.newSet);
        $("#curset_confirm_nosave_new_btn").click(function () { _sme.newSet(true); });
        $("#curset_confirm_nosave_logout_btn").click(function () { 
            $("#cursetNoSaveExitConfirmModal").modal("hide");
            _sme.unsaved = false;
            _parent.doLogout(); 
        });
        $("#curset_confirm_nosave_stay_btn").click(function () { _sme.loggingOut = false; });
        $("#oldsets_details_close").click(_sme.closeExistingSet);
        $("#load_existing_set_yes").click(_sme.oldSetToCurSet);
        

        /* activate set button display functions */
        window.writeList = _sme.writeList;
        window.writeResults = _sme.writeResults;
        window.onresize = _sme.setsResize;

        /* check for unsaved set */
        $(window).bind("beforeunload", function() {
            if (_sme.unsaved) {
                return true;
            }
        });
    }

    initSets();
}
