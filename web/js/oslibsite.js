/**
 * OpenSong Library Online
 * 
 * version: 2.0
 * file version: 2.0.2
 * updated: 2021-06-18 | 08:29 UTC
 * 
 */

/* JavaScript file for OpenSong Library Display Site with base functions */

/* object to do first run */
var FirstRun = function() {
    /* properties */
    this.songList = [];
    this.currentItem = 0;
    this.totalItems = 0;

    /* self-referent */
    _me = this;

    /* methods */
    /**
     * Function: FirstRun.finishUp
     * Description: updates the database status and loads the database
     * Arguments: none
     * Returns: nothing
     */
    this.finishUp = function() {
        $.post("process.php", {"action":"updateStats"}, "", "json").done(
            function (d) {
                if (typeof d.error != "undefined") {
                    console.log("FirstRun error when finishing up:", d.error);
                } else {
                    $("#navList").fadeIn();
                    $("nav form").fadeIn();
                    getStats();
                    $("#firstrunmsg").fadeOut();
                    loadFromDb('songs','all');
                }
            }
        ).fail(function (e) { console.log("FirstRun error when finishing up:", e); });
    }
    /**
     * Function: FirstRun.go
     * Description: Triggers the first run by getting the items
     * Arguments: none
     * Returns: nothing
     */
    this.go = function () {
        /* hide the links on the menu bar */
        $("#navList").hide();
        $("nav form").hide();
        $.post("process.php", {"action":"firstrun", "fid":0}, "", "json").done(
            function(d){
                if (typeof d.error != "undefined") {
                    if (d.error == "pCloudPathBad") {
                        $("#firstrunmsg").html("The path provided for pCloud is invalid.<br>You may want to rerun setup.");
                        $("#loadingspinner").fadeOut();
                        $("#dbicon").fadeOut();
                        $("#erroricon").fadeIn();
                        return false;
                    } else {
                        console.log("FirstRun error", d.error);
                    }
                } else {
                    _me.songList = d;
                    _me.totalItems = d.length;
                    _me.procSong();
                }
            }).fail(function(e) {console.log("FirstRun error", e);});
    }

    /**
     * Function: FirstRun.procSong
     * Description: processes the next song
     * Arguments: none
     * Returns: nothing
     */
    this.procSong = function () {
        _me.updateStatus();
        $.post("process.php", {"action":"firstrun", 
                              "fid": _me.songList[_me.currentItem].id}, "", "json").done(
            function(d){
                if (typeof d.error != "undefined") {
                    console.log("FirstRun.go error", "fid: " + _me.songList[_me.currentItem].id, d.error);
                } else if (typeof d.success != "undefined") {
                    _me.currentItem++;
                    if (_me.currentItem >= _me.totalItems) {
                        _me.finishUp();
                    } else {
                        _me.procSong();
                    }
                }
            }).fail( function(e) { console.log("Firstrun.go error", e); })
    }

    /**
     * Function: updateStatus
     * Description: updates the status message to display progress
     * Arguments: none
     * Returns: nothing
     */
    this.updateStatus = function () {
        var html = window.lang.firstRun;
        var active = _me.currentItem + 1;
        html = html.replace("%current%", active);
        html = html.replace("%total%", _me.totalItems);
        $("#firstrunmsg").html(html);
    }

}

/* functions for displaying the database */
/**
 * Function: calcViewHeight
 * Description: calculates the view screen height and sizes the main window appropriately
 * Arguments: none
 * Returns: nothing
 */
function calcViewHeight() {
    w = parseInt($(window).height());
    n = parseInt($("nav").height());
    h = parseInt($("#pgtitle").height());
    f = parseInt($("footer").height());
    $(".dbcontent").css("height", (w - n - h - f - 20) + "px");
}


/**
 * Function: getStats
 * Description: gets the latest statistics from the database
 * Arguments: none
 * Returns: object with timestamp and song number
 */
function getStats() {
    $.post('process.php', {"action":"getStats"},"", "json").done(function(d) {
        loadStats(d);
    }).fail(function(e){
        console.log("getStats error: ", e);
    });
}

/** 
 * Function: loadFromDb
 * Description: loads data from db
 * Arguments: q = 'songs' or 'authors' 
 *            d = for songs: 'all', 'official' or 'details'
 *                for authors: 'first' or 'last'
 *            i = song ID for songs:details
 * Returns: nothing
 */
function loadFromDb(q,d,i) {
    if (typeof q == "undefined") { q = "songs"; }
    if (typeof d == "undefined") { d = "all"; }
    if (q == "songs" && d == "details" && typeof i == "undefined") { return false; }
    if (typeof i == "undefined") { i = 0; }
    $(".dbdata").fadeOut();
    $("#loadingspinner").fadeIn();
    $("#dbicon").fadeIn();
    this.query = {"item":q, "sort": d, "song_id": i}
    _me = this;
    $.post("process.php",{"action":"loadFromDb", "item":q, "sort":d, "song_id":i}, "", "json").done(function(d) {
        if (typeof d.data.firstrun != "undefined") {
            $("#firstrunmsg").fadeIn();
            var fr = new FirstRun();
            fr.go();
        } else if (typeof d.error != "undefined") {
            if (d.data.error == "nosetup") {
                $("#loadingspinner").fadeOut();
                $("#splashscreen").fadeOut();
                $("#modalSetupNotComplete").modal();
            }
        } else {
            if (d.update) {
                procNewSongs();
            }
            writeDbData(d);
        }
    }).fail(function(e) { console.log("Error in loadFromDb", e)});
}

/**
 * Function: loadStats
 * Description: writes the statistics to the interface
 * Arguments: d = stats object
 * Returns: nothing
 */
function loadStats(d) {
    $("#last_update").html(d.time);
    $("#total_songs").html(d.songs);
}

/**
 * Function: procNewSongs
 * Description: triggers the processing of any new songs that have been verified
 * Arguments: none
 * Returns: nothing
 */
function procNewSongs() {
    if (typeof window.procInProc != "undefined" && window.procInProc == true) { return true; }
    window.procInProc = true;
    $("#updatingspinner").css("display", "inline-block");
    $.post("process.php", {"action":"procNewSongs"}, "", "json").done(
        function (d) {
            window.procInProc = false;
            $("#updatingspinner").css("display", "none");
            if (typeof(d.error) != "undefined") {
                $("#updatebad").css("display","inline-block");
                console.log("Process new songs errors:", d.error);
            } else {
                if (d.success == "updated") {
                    getStats();
                    $("#refreshdb").css("display", "inline-block");
                } 
                $("#updategood").css("display", "inline-block");
                setTimeout(function() { $("#updategood").fadeOut(); }, 30000);
                procOldSongs();
            }
            
        }
    ).fail(function (e) { console.log("procNewSongs error:", e); });
}


/**
 * Function: procOldSongs
 * Description: triggers the processing of any old songs that have been modified
 * Arguments: none
 * Returns: nothing
 */
function procOldSongs() {
    $.post("process.php", {"action":"updateSongs"}, "", "json").done(
        function (d) {
            if (typeof d.error != "undefined") {
                $("#updategood").css("display","none");
                $("#updatebad").css("display","inline-block");
                console.log("Check for changes in old songs error:",d.error);
            }
        }
    ).fail(function (e) { console.log("procOldSongs error:", e)});
}

/**
 * Function: randomID
 * Description: generates a random alphanumeric id in a given length
 * Arguments: chars = how long the string should be, defaults to 8
 *            kind = "any", "decimal", "alphabet", "hexadecimal",
 *                   defaults to any
 * Returns: string containing random ID
 */
function randomID (chars, kind) {
    if (typeof chars == "undefined") { chars = 8; }
    if (typeof kind == "undefined") { kind = "any"; }
    var seed = ["0","1","2","3","4","5","6","7","8","9","A","B","C",
                "D","E","F","G","H","I","J","K","L","M","N","O","P",
                "Q","R","S","T","U","V","W","X","Y","Z","a","b","c",
                "d","e","f","g","h","i","j","k","l","m","n","o","p",
                "q","r","s","t","u","v","w","x","y","z"];
    var r = -1;
    var id = "";
    var start = 0;
    var end = seed.length - 1;
    switch (kind) {
        case "decimal":
            end = 9;
            break;
        case "hexadecimal":
            end = 15;
            break;
        case "alphabet":
            start = 10;
            break;
    }

    for (var i=0; i<chars; i++) {
        while (r < start || r > end) {
            r = Math.floor((Math.random() * 100));
        }
        id += seed[r];
        r = -1;
    }
    return id;
}
