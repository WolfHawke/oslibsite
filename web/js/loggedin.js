/**
 * OpenSong Library Online
 * 
 * version: 2.0
 * file version: 2.0.2
 * updated: 2021-06-18 | 08:28 UTC
 * 
 */

/** JavaScript functions to handle everything once logged in */

/* secure side object */
window.oslibSecure = function(usrlvl) {
    if (typeof usrlvl == "undefined") { usrlvl = "normal"; }
    /* properties */
    /* public */
    this.usrlvl = ""; /* 'normal' or 'mgr' */
    this.usrname = "";
    this.usrfull = "";
    this.usrlibmgr = ""; /* library manager */
    this.seckey = "";
    this.lang = {};
    this.weAreMgr = "";
    this.resetPwd = false;
    this.layers = ["challengeReset","profile","allSets","currentSet","addNewSong","sets_icon"];
    this.recurse = 0;
    this.cancelRecurse = false;
    this.setSong = false;

    /* addSong functions placeholder */
    this.addsong = null;

    /* self-referent */
    var _me = this;

    /* methods */
    /* public */
    /** 
     * Name: checkPwd
     * Description: checks to see if the password is okay
     * Arguments: p = password to check
     * Returns: boolean to prevent page from being submitted
     */
    function checkPwd(p) {
        var result = true;
        if ((p.length < 8) || (!p.match(/[A-Z]/) || (!p.match(/[a-z]/)) || (!p.match(/[0-9]/)) || (p.match(/[\"\'\\]/)) )) {
            result = false;
            $("#site_admin_pwd").addClass("is-invalid");
            $("body").scrollTo("#site_admin_pwd");
        }
        return result;
    }

    /**
     * Function: doLogout
     * Description: triggers the logout function
     * Arguments: none
     * Returns: nothing
     */
    this.doLogout = function () {
        /* check for unsaved set */
        if (_me.sets.unsaved) {
            window.location.hash = "#currentSet";
            $("#cursetNoSaveExitConfirmModal").modal();
            return false;
        } else {
            $(".modal-backdrop").remove();
        }

        $("#navbarSupportedContent").removeClass("show");
        $(".navbar-toggler").addClass("collapsed");

        /* revert to non-logged-in hashchange manager */
        $(window).bind('hashchange', _me.extendedHashNavhashNav);
        $(window).bind('hashchange', hashNav);

        _me.username = "";
        _me.usrlvl = "";
        _me.seckey = "";
        _me.usrfull = "";
        _me.lang = {};
        _me.weAreMgr = "";
        _me.resetPwd = false;
        $("#navList").show(); /* in case we'd hidden it */

        /* delete layers that should only be available when logged in */
        _me.layers.forEach(function (l) { 
            $("#" + l).remove();
        });
        $(".detail-set-btn-cntnr").html("");
        
        /* remove menu items */
        $("#setsDropdown").remove();
        $("#userDropdown").remove();
        $("#addSongLink").remove();
        $("#libmgmtDropdown").remove();
        $("#loginLink").show();
        $.post("process.php", {"action":"dologout"}, "", "json").fail(function (e) { console.log("dologout", e); });
        
        /* reset to non-logged-in functions */
        window.location.hash = "#login";        
        window.onresize = calcViewHeight;
        loginmgmt.doLogout();
    }

    /**
     * Function: extendedHashNav
     * Description: hashNav with extended features for secure side
     * Arguments: none
     * Returns: nothing
     */
    this.extendedHashNav = function() {
        var h = window.location.hash;
        /* this code is to prevent the infinite recursion that sometimes happens when #challengeReset
         * is called upon a second site login. */
        if ((_me.recurse > 2 || _me.cancelRecurse) && h == "#challengeReset") {
            _me.cancelRecurse = true;
            _me.recurse--;
            if (_me.recurse < 0) {
                _me.cancelRecurse = false;
                _me.recurse = 0;
            }
            return false;
        }
        _me.recurse++;
        /* now we actually go and load the page */
        if (_me.resetPwd && $("#challengeReset").length > 0 && h != "#challengeReset") {
            window.location.hash = "#challengeReset";
            return;
        } else if (_me.resetPwd != true && h == "#challengeReset") {
            return;
        } else if (h == "#allSets") {
            hashNav(true); /* clear layers only */
            if (_me.setSong) {
                $("#allSets").fadeIn();
                $("#dbplaceholder").fadeOut();
                $("#loadingspinner").fadeOut();
                _me.setSong = false;
            } else {
                _me.sets.getSets();
            }
            return;
        } else if (h == "#login" && window.loginmgmt.login) {
            window.location.hash = "#list:all-titles";
            return;
        } else if (h == "#usr_mgmt" && window.loginmgmt.login && _me.usrlvl == "mgr") {
            hashNav(true); /* clear layers only */
            _me.weAreMgr.loadUsers();
            return true;
        } 
        hashNav();
    }

    /** 
     * Function: initIface
     * Description: initializes interface by adding links and layers
     * Arguments: none
     * Returns: nothing
     */
    this.initIface = function () {
        if (_me.usrlvl == "mgr" || _me.usrlvl == "normal") {
            /* add layers */
            var html = ""
            _me.layers.forEach(function (l) { 
                $("#mainContainer").append("<div id=\"" + l + "\" class=\"dbcontent dbdata\"></div>\n");
            });
            
            /* add layer contents */
            /* force password reset */
            html ="<!-- Modal -->";
            html += "<div class=\"modal fade\" id=\"resetChallengeSuccessModal\" data-backdrop=\"static\" ";
            html += "data-keyboard=\"false\" tabindex=\"-1\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">";
            html += "<div class=\"modal-dialog\">\n<div class=\"modal-content\">";
            html += "<div class=\"modal-header\">";
            html += "<h5 class=\"modal-title\" for=\"modalSetupNotComplete\">" + _me.lang.profile.pwdrst_title + "</h5>\n</div>\n";
            html += "<div class=\"modal-body\">" + _me.lang.profile.pwdrst_success_msg + "</div>";
            html += "<div class=\"modal-footer\">";
            html += "<button type=\"button\" class=\"btn btn-secondary btn-purple\" ";
            html += "onclick=\"window.weAreSecure.pwdResetComplete();\">" + _me.lang.profile.mdl_btn_close + "</button>";
            html += "</div>\n</div>\n</div>\n</div>";
            html += "<div class=\"row mt-4\">\n";
            html += "<div class=\"col-lg-4 col-md-3 col-sm-2 col1\">&nbsp;</div>";
            html += "<div class=\"col-lg-4 col-md-6 col-sm-8 col-10\">";
            html += "<div id=\"challengeResetContainer\" class=\"card card-body\">";
            html += "<h5 class=\"card-title text-center\">" + _me.lang.profile.pwdrst_title + "</h5>";
            html += "<div id=\"force_pwd_reset_error\" class=\"alert alert-danger\">";
            html += "<i class=\"fas fa-times\"></i> " + _me.lang.profile.pwdrst_err_txt + "</div>";
            html += "<form id=\"force_pwd_reset\" onsubmit=\"window.weAreSecure.updPwd('forced'); return false;\">";
            html += "<div class=\"form-group\">";
            html += "<label for=\"temppwd\">" + _me.lang.profile.pwdrst_tmppwd + ":</label>";
            html += "<input type=\"password\" class=\"form-control field-check\" id=\"tmppwd\" name=\"tmppwd\" ";
            html += "placeholder=\"" + _me.lang.profile.pwdrst_tmppwd_placeholder + "\" taborder=\"1\">";
            html += "<div class=\"invalid-feedback\">" + _me.lang.profile.pwd_chg_old_err + "</div>\n";
            html += "</div>\n<div class=\"form-group\">\n";
            html += "<label for=\"forcenew\">" + _me.lang.profile.pwd_chg_new_title + ":</label>";
            html += "<input type=\"password\" class=\"form-control field-check\"id=\"forcenew\" name=\"forcenew\" ";
            html += "placeholder=\"" + _me.lang.profile.pwd_chg_new_placeholder + "\" taborder=\"2\">"
            html += "<small>" + _me.lang.profile.pwd_chg_new_directives + "</small>";
            html += "<div class=\"invalid-feedback\">" + _me.lang.profile.pwd_chg_new_err + "</div>";
            html += "</div>\n<div class=\"form-group\">";
            html += "<label for=\"forceconf\">" + _me.lang.profile.pwd_chg_cnf_title + ":</label>";
            html += "<input type=\"password\" class=\"form-control field-check\"id=\"forceconf\" name=\"forceconf\" ";
            html += "placeholder=\"" + _me.lang.profile.pwd_chg_cnf_placeholder + "\">"
            html += "<div class=\"invalid-feedback\">" + _me.lang.profile.pwd_chg_cnf_err + "</div>\n</div>";
            html += "<div class=\"text-right\">";
            html += "<button class=\"btn btn-secondary\" onclick=\"weAreSecure.doLogout();\" taborder=\"4\"><i class=\"fas fa-ban\"></i> ";
            html += _me.lang.profile.pwdrst_but_cancel + "</button> ";
            html += "<button class=\"btn btn-secondary btn-purple ml-2\" onclick=\"weAreSecure.updPwd('forced');\" taborder=\"3\">";
            html += "<i class=\"far fa-key\"></i> " + _me.lang.profile.pwd_chg_btn + "</button>";
            html += "</div>\n</form>\n</div>\n</div>\n</div>";
            html += "<div class=\"col-lg-4 col-md-3 col-sm-2 col1\">&nbsp;</div>";
            html += "</div>\n</div>";
            $("#challengeReset").html(html);

            /* profile */
            html = "<h1>" + _me.lang.profile.title + " " + _me.usrname + "</h1>";
            html += "<div class=\"row\" id=\"profile_success\">\n<div class=\"col col-lg-5\">\n";
            html += "<div class=\"alert alert-success\">\n<i class=\"far fa-check\"></i> <span id=\"profile_success_txt\"></span>\n";
            html += "</div>\n</div>\n</div>\n";
            html += "<div class=\"row mb-5 mt-4\">\n<div class=\"col\">\n";
            html += "<form id=\"full_name_change\" name=\"full_name_change\" onsubmit=\"return false;\">";
            html += "<div class=\"form-inline\">\n<div class =\"form-group\">";
            html += "<label for=\"user_full_name\" class=\"mr-2\">" + _me.lang.profile.full_name_title + ":</label>";
            html += "<input type=\"text\" class=\"form-control mr-2\" id=\"user_full_name\" name=\"user_full_name\"";
            html += " placeholder=\"" + _me.lang.profile.full_name_placeholder + "\" value=\"" + _me.usrfull + "\">\n</div>\n";
            html += "<button class=\"btn btn-secondary btn-purple\" ";
            html += "title=\"" + _me.lang.profile.full_name_button +"\" id=\"full_name_save\"><i class=\"far fa-save\"></i></button>";
            html += "</div>\n</form>\n</div>\n</div>\n";
            html += "<div class=\"row mb-5 mt-4\">\n<div class=\"col\">\n";
            html += "<form id=\"email_change\" name=\"email_change\" onsubmit=\"return false;\">";
            html += "<div class=\"form-inline\">\n<div class =\"form-group\">";
            html += "<label for=\"user_email\" class=\"mr-2\">" + _me.lang.profile.email_title + ":</label>";
            html += "<input type=\"text\" class=\"form-control field-check mr-2\" id=\"user_email\" name=\"user_email\"";
            html += " placeholder=\"" + _me.lang.profile.email_placeholder + "\" value=\"" + _me.usremail + "\">\n</div>\n";
            html += "<button class=\"btn btn-secondary btn-purple\" ";
            html += "title=\"" + _me.lang.profile.email_button +"\" id=\"email_save\"><i class=\"far fa-save\"></i></button>";
            html += "<div class=\"invalid-feedback\">" + _me.lang.profile.email_err + "</div>\n</div>";
            html += "</div>\n</form>\n</div>\n</div>\n";            html += "<h2>" + _me.lang.profile.pwd_chg_title + "</h2>";
            html += "<div class=\"row\">\n<div class=\"col col-lg-5\">";
            html += "<form id=\"pwd_reset\" name=\"pwd_reset\" onsubmit=\"return false\">";
            html += "<div class=\"form-group\">";
            html += "<label for=\"old_pwd\">" + _me.lang.profile.pwd_chg_old_title + ":</label>";
            html += "<input type=\"password\" class=\"form-control field-check\" name=\"old_pwd\" id=\"old_pwd\"";
            html += " placeholder=\"" + _me.lang.profile.pwd_chg_old_placeholder + "\">";
            html += "<div class=\"invalid-feedback\">" + _me.lang.profile.pwd_chg_old_err + "</div>";
            html += "</div>\n<div class=\"form-group\">";
            html += "<label for=\"new_pwd\">" + _me.lang.profile.pwd_chg_new_title + ":</label>";
            html += "<input type=\"password\" class=\"form-control field-check\" name=\"new_pwd\" id=\"new_pwd\" ";
            html += "placeholder=\"" + _me.lang.profile.pwd_chg_new_placeholder + "\">";
            html += "<small>" + _me.lang.profile.pwd_chg_new_directives + "</small>";
            html += "<div class=\"invalid-feedback\">" + _me.lang.profile.pwd_chg_new_err + "</div>";
            html += "</div>\n<div class=\"form-group\">";
            html += "<label for=\"confirm_pwd\">" + _me.lang.profile.pwd_chg_cnf_title + ":</label>";
            html += "<input type=\"password\" class=\"form-control field-check\" name=\"confirm_pwd\" id=\"confirm_pwd\" ";
            html += "placeholder=\"" + _me.lang.profile.pwd_chg_cnf_placeholder + "\">";
            html += "<div class=\"invalid-feedback\">" + _me.lang.profile.pwd_chg_cnf_err + "</div>\n</div>";
            html += "<button class=\"btn btn-secondary btn-purple\" id=\"pwd_reset_btn\">";
            html += "<span id=\"pwd_reset_btn_txt\"><i class=\"far fa-key\"></i> " + _me.lang.profile.pwd_chg_btn + "</span>";
            html += "<span id=\"pwd_reset_btn_spinner\" class=\"spinner-border\" role=\"status\">";
            html += "<span class=\"sr-only\">Loading...</span>";
            html += "</span>\n</button>\n</form>\n</div>\n</div>";
            $("#profile").html(html);
            $("#full_name_save").on("click", _me.updFullName);
            $("#pwd_reset_btn").on("click", _me.updPwd);
            $("#email_save").on("click", _me.updEmail);

            /* add song form */
            html = "<!-- Add Song interface -->";
            html += "<h1>" + _me.lang.add_song.title + "</h1>\n";
            html += "<p>" + _me.lang.add_song.blurb + "</p>\n";
            html += "<div id=\"add_song_err_form\" class=\"alert alert-danger add-song-alerts\"><i class=\"fas fa-times\"></i> " + _me.lang.add_song.err_bad_fields + "</div>\n";
            html += "<div id=\"add_song_err_save\" class=\"alert alert-danger add-song-alerts\"><i class=\"fas fa-times\"></i> " + _me.lang.add_song.err_no_save + "</div>\n";
            html += "<div id=\"add_song_success\" class=\"alert alert-success add-song-alerts\"><i class=\"fas fa-check\"></i> ";
            html += _me.lang.add_song.success1.replace ("%s%", "<code id=\"new_song_success_name\"></code>");
            html += " <span id=\"add_song_set_success\"> " + _me.lang.add_song.success2 + "</span></div>\n";
            html += "<form id=\"add_song\" onsubmit=\"return false;\">\n<div class=\"row\">\n";
            html += "<div class=\"col-12 col-md-8 col-lg-6\">\n<div class=\"form-group\">\n";
            html += "<label for=\"new_song_title\" class=\"text-bold\">" + _me.lang.add_song.song_title_title + ":</label>";
            html += "<input type=\"text\" name=\"new_song_title\" id=\"new_song_title\" class=\"form-control form-check\" tabindex=\"1\">";
            html += "<small>" + _me.lang.add_song.song_title_blurb + "</small>";
            html += "<div class=\"invalid-feedback\">" + _me.lang.add_song.song_title_err + "</div>\n";
            html += "</div>\n</div>\n</div>\n";
            html += "<div class=\"row\">\n<div class=\"col-12 col-md-8 col-lg-6\">\n";
            html += "<div class=\"form-group\">\n";
            html += "<label id=\"new_song_author_label\" for=\"new_song_author\" class=\"text-bold\">" + _me.lang.add_song.authors_title + ":</label>\n";
            html += "<input type=\"text\" name=\"new_song_author\" id=\"new_song_author\" class=\"form-control form-check\" ";
            html += "placeholder=\"" + _me.lang.add_song.authors_placeholder + "\" tabindex=\"2\">\n";
            html += "<small>" + _me.lang.add_song.authors_blurb + "</small>\n";
            html += "<div class=\"invalid-feedback\" id=\"auth_err\">" + _me.lang.add_song.authors_err + "</div>\n";
            html += "</div>\n</div>\n</div>\n";
            html += "<div class=\"row\">\n";
            html += "<div class=\"col-6 col-md-4 col-lg-2\">\n<div class=\"form-group\">\n";
            html += "<label class=\"text-bold\" id=\"new_song_ccli_label\" for=\"new_song_ccli\">" + _me.lang.add_song.ccli_title + ":</label>\n";
            html += "<input type=\"number\" name=\"new_song_ccli\" id=\"new_song_ccli\" ";
            html += "class=\"form-control form-check\" placeholder=\"1234\" tabindex=\"3\">\n";
            html += "<div class=\"invalid-feedback\">" + _me.lang.add_song.auth_ccli_err + "</div>\n";
            html += "</div>\n</div>\n</div>";
            html += "<div class=\"row\">";
            html += "<div class=\"col-12 col-md-8 col-lg-6\">\n<div class=\"form-group\">\n";
            html += "<label for=\"new_song_copyright\">" + _me.lang.add_song.copy_title + ":</label>";
            html += "<input type=\"text\" name=\"new_song_copyright\" id=\"new_song_copyright\" class=\"form-control field-w-button-after\" tabindex=\"4\">\n";
            html += "<button type=\"button\" class=\"btn btn-secondary button-after-field\" id=\"new_song_copyright_btn\"";
            html += "title=\"" + _me.lang.add_song.copy_btn + "\">&copy;</button>\n";
            html += "</div>\n</div>\n</div>\n";
            html += "<div class=\"row\">";
            html += "<div class=\"col-12 col-md-8 col-lg-8\">\n<div class=\"form-group\">";
            html += "<label for=\"new_song_lyrics\">" + _me.lang.add_song.lyrics_title + ":</label>";
            html += "<textarea class=\"form-control\" id=\"new_song_lyrics\" name=\"new_song_lyrics\" rows=\"20\" tabindex=\"5\"></textarea>\n";
            html += "</div>\n</div>\n";
            html += "<div class=\"col-12 col-md-4 col-lg-4\">\n<p>" + _me.lang.add_song.lyrics_blurb1 + "</p>\n";
            html += "<button type=\"button\" class=\"btn btn-secondary os-item-add\" onclick=\"return false;\" data-type=\"V\">" + _me.lang.add_song.lyrics_btn_v + " [V]</button>";
            html += "<button type=\"button\" class=\"btn btn-secondary os-item-add\" onclick=\"return false;\" data-type=\"C\">" + _me.lang.add_song.lyrics_btn_c + " [C]</button>";
            html += "<button type=\"button\" class=\"btn btn-secondary os-item-add\" onclick=\"return false;\" data-type=\"P\">" + _me.lang.add_song.lyrics_btn_p + " [P]</button>";
            html += "<button type=\"button\" class=\"btn btn-secondary os-item-add\" onclick=\"return false;\" data-type=\"B\">" + _me.lang.add_song.lyrics_btn_b + " [B]</button>";
            html += "<button type=\"button\" class=\"btn btn-secondary os-item-add\" onclick=\"return false;\" data-type=\"T\">" + _me.lang.add_song.lyrics_btn_t + " [T]</button>\n";
            html += "<p class=\"mt-5\">" + _me.lang.add_song.lyrics_blurb2 + "</p>\n";
            html += "</div>\n</div>";
            html += "<div class=\"row\">";
            html += "<div class=\"col-12 col-md-8 col-lg-6\">\n<div class=\"form-group\">";
            html += "<label for=\"new_song_present\">" + _me.lang.add_song.order_title + "</label>\n";
            html += "<input type=\"text\" name=\"new_song_present\" id=\"new_song_present\" class=\"form-control form-check\" "
            html += "placeholder=\"" + _me.lang.add_song.order_placeholder + "\" tabindex=\"6\">\n";
            html += "<small>" + _me.lang.add_song.order_blurb + "</small>\n";
            html += "<div class=\"invalid-feedback\">" + _me.lang.add_song.order_err + "</div>\n";
            html += "</div>\n</div>\n</div>\n";
            html += "<div class=\"row\">\n";
            html += "<div class=\"col-12 col-md-8 col-lg-6\">\n<div class=\"form-group\">\n";
            html += "<label for=\"new_song_src\">" + _me.lang.add_song.source_title + ":</label>";
            html += "<input type=\"text\" name=\"new_song_src\" id=\"new_song_src\" class=\"form-control form-check\" "
            html += "placeholder=\"" + _me.lang.add_song.source_placeholder + "\" tabindex=\"7\">\n";
            html += "<small>" + _me.lang.add_song.source_blurb + "</small>";
            html += "</div>\n</div>\n</div>\n";
            html += "<div class=\"row\">";
            html += "<div class=\"col-12 col-md-8 col-lg-6\">\n<div class=\"form-group fac fac-checkbox fac-purple\">\n";
            html += "<span></span><input type=\"checkbox\" name=\"new_song_to_set\" id=\"new_song_to_set\" class=\"\" checked tabindex=\"8\">\n";
            html += "<label for=\"new_song_to_set\">" + _me.lang.add_song.add_to_set_title + "</label>\n";
            html += "</div>\n</div>\n</div>\n";
            html += "<div class=\"row\">\n<div class=\"col-12 text-center\">\n";
            html += "<button type=\"button\" class=\"btn btn-secondary btn-purple text-center\" id=\"btn_new_song_save\" tabindex=\"9\">";
            html += "<span id=\"newsong_save_text\" class=\"but-txt\">"
            html += "<i class=\"far fa-save\"></i>&nbsp;&nbsp;" + _me.lang.add_song.btn_submit_song + "</span>"; 
            html += "<div id=\"newsong_save_spinner\" class=\"spinner-border spinner-border-sm btn-spinner\" role=\"status\"><span class=\"sr-only\">Loading...</span></div>"
            html += "</button>";
            html += "</div>\n</div>\n</div>\n</form>\n";
            $("#addNewSong").html(html);

            /* current set screen */
            html = "<!-- Confirm Current Set New Without Save Modal -->";
            html += "<div class=\"modal fade\" id=\"cursetNoSaveNewConfirmModal\" tabindex=\"-1\" aria-labelledby=\"cursetNoSaveNewConfirmModalLabel\" aria-hidden=\"true\">";
            html += "<div class=\"modal-dialog\">\n<div class=\"modal-content\">\n";
            html += "<div class=\"modal-header\">\n";
            html += "<h5 class=\"modal-title\" id=\"cursetNoSaveNewConfirmModalLabel\">" + _me.lang.sets.cur_set_mdl_title + "</h5>\n</div>\n";
            html += "<div class=\"modal-body\">" + _me.lang.sets.cur_set_mdl_body + "</div>\n";
            html += "<div class=\"modal-footer\">";
            html += "<button type=\"button\" class=\"btn btn-secondary\" id=\"curset_confirm_nosave_new_btn\">" + _me.lang.sets.but_yes + "</button>";
            html += "<button type=\"button\" class=\"btn btn-secondary btn-purple\" data-dismiss=\"modal\">" + _me.lang.sets.but_no + "</button>";
            html += "</div>\n</div>\n</div>\n</div>\n";
            html += "<!-- Confirm Current Set Exit Without Save Modal -->";
            html += "<div class=\"modal fade\" id=\"cursetNoSaveExitConfirmModal\" tabindex=\"-1\" aria-labelledby=\"cursetNoSaveExitConfirmModalLabel\" aria-hidden=\"true\">";
            html += "<div class=\"modal-dialog\">\n<div class=\"modal-content\">\n";
            html += "<div class=\"modal-header\">\n";
            html += "<h5 class=\"modal-title\" id=\"cursetNoSaveExitConfirmModalLabel\">" + _me.lang.sets.cur_set_mdl_title + "</h5>\n</div>\n";
            html += "<div class=\"modal-body\">" + _me.lang.sets.cur_set_mdl_logout_body + "</div>\n";
            html += "<div class=\"modal-footer\">";
            html += "<button type=\"button\" class=\"btn btn-secondary\" id=\"curset_confirm_nosave_logout_btn\">" + _me.lang.sets.but_yes + "</button>";
            html += "<button type=\"button\" class=\"btn btn-secondary btn-purple\" id=\"curset_confirm_nosave_stay_btn\" data-dismiss=\"modal\">" + _me.lang.sets.but_no + "</button>";
            html += "</div>\n</div>\n</div>\n</div>\n";
            html += "<div class=\"row\">";
            html += "<div class=\"col-10\"><h1>" + _me.lang.sets.cur_set_title + "</h1></div>";
            html += "<div class=\"col-2 text-right\">";
            html += "<button class=\"btn btn-secondary btn-purple mr-1\" id=\"new_set\" onclick=\"return false;\" title = \"" + _me.lang.sets.cur_set_btn_new + "\"><i class=\"far fa-file\"></i></button>";
            html += "<button class=\"btn btn-secondary btn-purple mr-1\" id=\"save_set\" onclick=\"return false;\" title = \"" + _me.lang.sets.cur_set_btn_save + "\">";
            html += "<span id=\"save_set_icon\"><i class=\"far fa-save\"></i></span>";
            html += "<div id=\"save_set_spinner\" class=\"spinner-border spinner-border-sm btn-spinner\" role=\"status\">";
            html += "<span class=\"sr-only\">Loading...</span></div></button>";
            html +="</div>\n</div>";
            html += "<div class=\"row\">\n<div class=\"col\">\n";
            html += "<p>" + _me.lang.sets.cur_set_blurb + "</p>";
            html += "<div id=\"cur_set_save_error\" class=\"alert alert-danger\"><i class=\"fas fa-times\"></i> " + _me.lang.sets.cur_set_save_error + "</div>";
            html += "<div id=\"cur_set_save_success\" class=\"alert alert-success\">";
            html += "<div class=\"float-right\"id=\"close_cur_set_save_success\"><i class=\"far fa-times\"></i></div>";
            html += "<i class=\"far fa-check\"></i> " + _me.lang.sets.cur_set_save_success + "</div>";
            html += "</div>\n</div>\n";
            html += "<div class=\"row mb-5\">\n<div class=\"col\">\n";
            html += "<div id=\"set_title_display\">\n<b>" + _me.lang.sets.cur_set_set_title + ":</b> <span id=\"set_title_display_txt\"></span>&nbsp;<span id=\"set_title_edit_go\"><i class=\"far fa-edit\"></i></span>\n</div>";
            html += "<div class=\"form-group form-inline w-75\" id=\"set_title_edit\">\n";
            html += "<label for=\"set_title\" class=\"mr-1\"><b>" + _me.lang.sets.cur_set_set_title + ":</b></label>\n";
            html += "<input type=\"text\" class=\"form-control\" name=\"set_title\" id=\"set_title\" value=\"\">\n";
            html += "<button class=\"btn btn-secondar btn-purple ml-1\" id=\"save_set_title\" onclick=\"return false;\" title=\"" + _me.lang.sets.cur_set_set_title_btn_save + "\"><i class=\"far fa-save\"></i></button>\n";
            html += "</div>\n</div>\n</div>\n";
            html += "<div class=\"row\">\n";
            html += "<div class=\"col-sm-2\"></div>\n";
            html += "<div class=\"col-sm-8\">\n";
            html += "<p id=\"curset_empty\">" + _me.lang.sets.cur_set_empty + "</p>\n";
            html += "<ul id=\"curset_sortable\"></ul>\n</div>\n";
            html += "<div class=\"col-sm-2\"></div>\n</div>";
            $("#currentSet").html(html);

            /* sets spinner */
            html = "<i class=\"far fa-list-music\"></i>";
            $("#sets_icon").html(html);

            /* all sets screen*/
            html = "<!-- Modal to confirm loading of set -->\n";
            html += "<div class=\"modal fade\" id=\"loadExistingSetModal\" tabindex=\"-1\" aria-labelledby=\"loadExistingSetModalTitle\" aria-hidden=\"true\">\n";
            html += "<div class=\"modal-dialog\">\n<div class=\"modal-content\">\n<div class=\"modal-header\">\n";
            html += "<h5 class=\"modal-title\" id=\"loadExistingSetModalTitle\">" + _me.lang.sets.old_sets_modal_title + "</h5>\n</div>\n";
            html += "<div class=\"modal-body\">\n";
            html += "<p>" +  _me.lang.sets.old_sets_modal_text1.replace("%apptitle%", $("#title_txt").html()) + "</p>\n";
            html += "<p>" +  _me.lang.sets.old_sets_modal_text2 + "</p>\n";
            html += "</div>\n<div class=\"modal-footer\">\n";
            html += "<button type=\"button\" class=\"btn btn-secondary\" id=\"load_existing_set_yes\">" +  _me.lang.sets.but_yes + "</button>";
            html += "<button type=\"button\" class=\"btn btn-secondary btn-purple\" data-dismiss=\"modal\">" +  _me.lang.sets.but_no + "</button>";
            html += "</div>\n</div>\n</div>\n</div>\n";
            html += "<div id=\"oldsets_icon\"><i class=\"far fa-list-music\"></i></div>\n";
            html += "<div id=\"oldsets_spinner\"><div class=\"spinner-border text-purple\" role=\"status\"><span class=\"sr-only\">Loading...</span></div></div>\n";
            html += "<h1>" +  _me.lang.sets.old_sets_title + "</h1>\n";
            html += "<div id=\"oldsets_container\" class=\"row\">\n";
            html += "<div id=\"oldsets_list\" class=\"col-lg-6 p-3 h-100\">\n";
            html += "<ul id=\"oldset_list_container\">\n</ul>\n</div>";
            html += "<div id=\"oldsets_details\" class=\"col-lg-6 p-3 h-100\">\n";
            html += "<div id=\"oldsets_details_close\" class=\"text-right\"><i class=\"far fa-times\"></i></div>";
            html += "<div class=\"row\">\n<div class=\"col-10\">\n<h2 id=\"oldsets_details_title\"></h2>\n</div>";
            html += "<div class=\"col-2 text-right\">";
            html += "<button type=\"button\" class=\"btn btn-secondary btn-purple\" id=\"edit_cur_oldset\" data-toggle=\"modal\" data-target=\"#loadExistingSetModal\" title=\"" + _me.lang.sets.but_edit_set + "\"><i class=\"far fa-pencil\"></i></button>";
            html += "</div>\n</div>\n";
            html += "<div id=\"oldset_file_error\" class=\"alert alert-danger\"><i class=\"far fa-times\"></i> " + _me.lang.sets.oldset_file_error + "</div>";
            html += "<div class=\"overflow-auto h-100\">\n<ul id=\"oldsets_details_content\"></ul>";
            html += "</div>\n</div>\n</div>";
            $("#allSets").html(html);

            /* add links */
            if (_me.usrlibmgr) {
                html = "<li class=\"nav-item dropdown\"id=\"libmgmtDropdown\">\n";
                html += "<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"libmgmtList\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"far fa-books\"></i> " + _me.lang.links.libmgmt_menu + " </a>\n";
                html += "<div class=\"dropdown-menu\" aria-labelledby=\"libmgmtList\">\n";
                html += "<a class=\"dropdown-item\" href=\"#addNewSong\" onclick=\"window.songDetail=false;\">" + _me.lang.links.add_song + "</a>\n";
                html += "<a id=\"upDb\" class=\"dropdown-item\" onclick=\"window.procNewSongs();\">" + _me.lang.links.libmgmt_dbupdate + "</a>\n";
                html += "</div>\n</li>\n";    
            } else {
                html = "<li class=\"nav-item\"><a class=\"nav-link\" href=\"#addNewSong\" id=\"addSongLink\">";
                html += "<i class=\"fal fa-music-alt\"></i> " + _me.lang.links.add_song +  " </a></li>";
            }
            html += "<li class=\"nav-item dropdown\" id=\"setsDropdown\">";
            html += "<div id=\"sets_badge\" class=\"bg-danger\" title=\"" + _me.lang.links.sets_num + "\"></div>\n";
            html += "<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"setsList\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"far fa-list-music\"></i> " + _me.lang.links.sets + "</a>\n";
            html += "<div class=\"dropdown-menu\" aria-labelledby=\"setsList\">\n";
            html += "<a class=\"dropdown-item\" href=\"#currentSet\" onclick=\"window.songDetail=false;\">" + _me.lang.links.sets_current + " <span id=\"items_in_set\"></span></a>\n";
            html += "<a class=\"dropdown-item\" href=\"#allSets\" onclick=\"window.songDetail=false;\">" + _me.lang.links.sets_all + "</a>\n";
            html += "</div>\n</li>\n";
            html += "<li class=\"nav-item dropdown\"id=\"userDropdown\">\n";
            html += "<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"userList\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fal fa-user-circle\"></i> " + _me.usrname + " </a>\n";
            html += "<div class=\"dropdown-menu\" aria-labelledby=\"userList\">\n";
            html += "<a class=\"dropdown-item\" href=\"#profile\" onclick=\"window.songDetail=false;\">" + _me.lang.links.user_profile + "</a>\n";
            html += (_me.usrlvl=="mgr") ? "<a class=\"dropdown-item\" href=\"#usr_mgmt\" onclick=\"window.songDetail=false;\">" + _me.lang.links.user_management + "</a>\n" : "";
            html += "<a id=\"logoutLink\" class=\"dropdown-item\" href=\"#login\" onclick=\"window.songDetail=false;\">" + _me.lang.links.user_logout + "</a>\n";
            html += "</div>\n</li>\n";
            $("#loginLink").hide();
            $("#navList").append(html);
            $("#logoutLink").click(_me.doLogout);
            $(window).unbind('hashchange', hashNav);
            $(window).bind('hashchange', _me.extendedHashNav);
            calcViewHeight();
            jQuery.getScript("js/smartquotes.js");
            /* include set management script */
            jQuery.getScript("js/setmgmt.js", function() { _me.sets = new oslibSets(_me);});
            /* include song addition scripts */
            jQuery.getScript("js/songadd.js", function() { _me.addsong = new AddSong(_me); });
            /* user management script */
            if (_me.usrlvl == "mgr") { jQuery.getScript("js/usrmgmt.js"); }

            window.setTimeout(function (p) {
                    if (p) {
                        window.location.hash = "#challengeReset";
                    } else { 
                        window.location.hash = "";
                        loadFromDb('songs','all',0);
                    }
                }, 500, _me.resetPwd);
        } else {
            console.log("Login failed: user level");
        }
    }

    /**
     * Function: pwdResetComplete
     * Description: switches off the password reset interface
     */
    this.pwdResetComplete = function() {
        _me.resetPwd = false;
        $("#resetChallengeSuccessModal").modal("hide");
        $("#navList").show();
        $("#identity").val(_me.usrname);
        $(".modal-backdrop").hide();
        _me.doLogout();
    }

    /**
     * Function: updEmail
     * Description: updates the email address of the user on the profile
     */
    this.updEmail = function() {
        $("#profile_success").fadeOut();
        $(".field-check").removeClass("is-invalid");

        /* check to make sure we have an e-mail address */
        const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        var email = $("#user_email").val();
        if (re.test(email)) {
            var a = { "action" : "updateUser", "what" : "email", "val" : email, "key": _me.seckey };
            $.post("process-secured.php", a, "", "json").done(
                function (d) {
                    $("#profile_success_txt").html(_me.lang.profile.email_success);
                    $("#profile_success").fadeIn();
                    _me.usremail = $("#user_email").val();
                    _me.seckey = d.success;
                }
            ).fail(function (e) { console.log("updEmail error", e); });
        } else {
            $("#user_email").addClass("is-invalid");
            $("#user_email").focus();
        }
    }

    /**
     * Function: updFullName
     * Description: updates the full name of the user on the profile
     */
    this.updFullName = function () {
        $("#profile_success").fadeOut();

        var a = {
            "action" : "updateUser",
            "key" : _me.seckey, 
            "what" : "fullname",
            "user_full_name" : $("#user_full_name").val()
        };
        $.post('process-secured.php', a, "", "json").done(
            function (d) {
                if (typeof d.error != "undefined") {
                    console.log(d.error);
                } else {
                    $("#profile_success_txt").html(_me.lang.profile.ufull_success);
                    $("#profile_success").fadeIn();
                    _me.usrfull = $("#user_full_name").val();
                    _me.seckey = d.success;
                }
            }
        ).fail(function (e) { console.log("updFullName error", e) });
    }

    /**
     * Function: updPwd
     * Description: updates the Password of the user
     * Argument: f = form to use: 'normal' (default) or 'forced'
     */
    this.updPwd = function (f) {
        if (typeof f == "undefined") { f = 'normal'; }
        var err = false; 
        
        this.oldpwd = "";
        this.newpwd = "";
        this.newconf = "";
        this.state = f; /* whether or not we're forced or not */

        var _ume = this;

        if (f == "forced") {
            oldpwd = $("#tmppwd");
            newpwd = $("#forcenew");
            newconf = $("#forceconf");
        } else {
            oldpwd = $("#old_pwd");
            newpwd = $("#new_pwd");
            newconf = $("#confirm_pwd");
        }

        /* reset invalid notifications */
        $(".field-check").removeClass("is-invalid");
        $("#profile_success").fadeOut();
        $("#pwd_reset_error").fadeOut();

        /* validate new password */
        if (!checkPwd(newpwd.val())) {
            newpwd.addClass("is-invalid");
            newpwd.focus();
            return false;
        }
        /* validate password equality */
        if (newpwd.val() != newconf.val()) {
            newconf.addClass("is-invalid");
            newconf.focus();
            return false;
        }

        /* submit password for reset */
        var d= {
            "action" : "updateUser",
            "key" : _me.seckey, 
            "what" : "challenge",
            "old_pwd" : oldpwd.val(),
            "new_pwd" : newpwd.val(),
        };
        $.post("process-secured.php", d, "", "json").done(
            function (d) {
                if (typeof d.error != "undefined") {
                    if (d.error == "old password bad") {
                        _ume.oldpwd.addClass("is-invalid");
                        _ume.oldpwd.focus();
                        return false;
                    } else {
                        console.log(d.error);
                    }
                } else {
                    _me.seckey = d.success;
                    if (_ume.state == "forced"){ 
                        $("#resetChallengeSuccessModal").modal();
                        _me.resetPwd = false;
                    } else {
                        $("#profile_success_txt").html(_me.lang.profile.pwd_success);
                        $("#profile_success").fadeIn();
                    }
                }
            }
        ).fail(function (e) { console.log("updPwd error", e)});
    }

    /* private */
    /** 
     * Function: initSecure
     * Description: initializes the secure connection by validating the user again
     * Arguments: none
     * Returns: nothing
     */
    var initSecure = function() {
        /* verify login */
        $.post("process-secured.php", { "action" : "validateUser", "key" : loginmgmt.seckey, "getLang": true }, "", "json").done(
            function (d) {
                if (typeof d.success != "undefined") {
                    _me.seckey = d.success.key;
                    _me.usrlvl = d.success.level;
                    _me.usrname = d.success.user;
                    _me.usrfull = d.success.userfull;
                    _me.usremail = d.success.email;
                    _me.resetPwd = (d.success.reset > 0) ? true : false;
                    _me.lang = d.strings;
                    _me.usrlibmgr = d.success.libmgmt;
                    $("head").append("<link rel=\"stylesheet\" href=\"css/loggedin.css?v=" + randomID() + "\">");
                    _me.initIface();
                } else {
                    console.log("Login failed: key verification");
                    return false;
                }
            }
        ).fail(function (e) { console.log("oslibSecure.initSecure error", e)});
            
    }
    initSecure();
}

window.weAreSecure = new oslibSecure();
