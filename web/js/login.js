/**
 * OpenSong Library Online
 * 
 * version: 2.0
 * file version: 2.0.1
 * updated: 2021-06-18 | 08:31 UTC
 * 
 */

/** JavaScript functions to handle logging in, etc. */

/* login object */
window.Login = function() {
    /* properties */
    this.login = false;
    this.seckey = "";
    this.paused = false;
    this.loginWait = null;
    this.failedLogins = 0;

    /* self-reference */
    var _me = this;

    /* public methods */
    /**
     * Function: checkUsr
     * Description: validates whether the user name and password only contain valid chars
     * Arguments: what = 'username' or 'challenge'
     *            s = string to check
     * Returns: boolean denoting validity
     */
    this.checkUsr = function (what, s) {
        if (typeof what == "undefined" || typeof s == "undefined" || s == "") { return false; }
        regex = "";
        res = false;

        switch (what) {
            case "username":
                /* only standard alphanumeric characters and . and - and _ */
                regex = /^[A-Za-z0-9_\.\-]+$/;
                break;
            case "challenge":
                /* any character but \ ' " */
                regex = /^((?![\\\'\"]).)*$/;
                break;
        }
        res = regex.test(s);
        return res;
    }

    /**
     * Function: doLogin
     * Description: executes the login function
     * Arguments: none
     * Returns: false to prevent form from submitting
     */
    this.doLogin = function() {

        /* self-referent */
        var _lme = this;

        /* check for maxed out failed logins */
        if (_me.failedLogins > 4) {
            console.log("Logins maxed out");
            $.post("process.php",{"action":"maxFailedLogins", "u":$("#identity").val()}, "", "json").done(
                function (d){ 
                    $(window).unbind("hashchange",hashNav);
                    window.location.hash = ""; 
                    window.location.reload(); 
                }
            );
        }

        /* disable everything to slow down login */
        this.slowMeDown = function ()  {
            _me.paused = true;
            _me.failedLogins++;
            $("#identity").attr("disabled", true);
            $("#challenge").attr("disabled", true);
            $("#login_btn").attr("disabled", true);
            /* renable everything afterward */
            _me.loginWait = setTimeout(function () {
                $("#identity").attr("disabled", false);
                $("#challenge").attr("disabled", false);
                $("#login_btn").attr("disabled", false);
                _me.paused = false;
                $("#identity").focus();
            }, 2000);
        }
        
        /* don't work if we are on the wait loop */
        if (_me.paused) { return false; }
        $("#login_error").hide();
        var id = $("#identity").val();
        var pw = $("#challenge").val();
        if (!_me.checkUsr("username", id) || !_me.checkUsr("challenge", pw)) {
            $("#login_error").fadeIn();
            _lme.slowMeDown();
            return false;
        }
        var data = {
            "action": "dologin",
            "id": id,
            "challenge": pw,
            "failed": _me.failedLogins,
        };
        $("#login_btn_txt").hide();
        $("#login_btn_spinner").show();
        $.post("process.php", data, "", "json").done(
            function (d) {
                $("#login_btn_txt").show();
                $("#login_btn_spinner").hide();
                if (typeof d.error != "undefined") {
                    if (d.error == "ip banned") {
                        $(window).unbind("hashchange",hashNav);
                        window.location.hash = "";
                        window.location.reload();
                    }
                    $("#login_error").fadeIn();
                    _me.failedLogins++;
                    _lme.slowMeDown();                    
                } else if (typeof d.success != "undefined") {
                    $("#identity").val("");
                    $("#challenge").val("");
                    _me.seckey = d.success;
                    _me.login = true;
                    _me.failedLogins = 0;
                    if (typeof window.oslibSecure == "undefined"){
                        jQuery.getScript("js/loggedin.js");
                    } else {
                        window.weAreSecure = new oslibSecure();
                    }
                }
            }
        ).fail(function(e) { 
            console.log("doLogin error:", e); 
            $("#login_btn_txt").show();
            $("#login_btn_spinner").hide();
        });
        return false;
    }

    /**
     * Function: doLogout
     * Description: does a full logout by destroying the login object and resetting the items here
     * Arguments: none
     * Returns: boolean denoting success
     */
    this.doLogout = function () {
        _me.login = false;
        _me.seckey = "";
        window.weAreSecure = null;
        window.writeList = regWrites.writeList;
        window.writeResults = regWrites.writeResults;
    }

    /**
     * Function: rndUserName
     * Description: returns a random user name from a list
     * Arguments: none
     * Returns: string
     */
    this.rndUserName = function() {
        var unames = ["cathryn", "MCard", "Audrey-A", "F.J.Crosby", "MattRed", "ChrisT", "DZesch", "Hope_Fellowship", "cloverton", "WorshipDude"];
        return(unames[Math.floor(Math.random() * 10)]);
    }
    
    /* private methods */
    /**
     * Function: initIface
     * Description: writes the login link and screen to the page 
     * Arguments: none
     * Returns: nothing 
     */
    var initIface = function () {
        /* this will only load if the IP is NOT banned */
        $.post("process.php", {"action":"checkIP"}, "", "json").done( function () {
            var html = "";

            /* write in sign in and sign out links */
            html = "<li class=\"nav-item\" id=\"loginLink\">";
            html += "<a class=\"nav-link\" href=\"#login\"><i class=\"far fa-sign-in-alt\"></i> ";
            html += window.lang.login.login_link + "</a>";
            html += "</li>";
            $("#navList").append(html);
            $("#logoutLink").click(_me.doLogout);

            /* add login div */
            html = "<div id=\"login\" class=\"dbcontent dbdata\"></div>";
            $("#mainContainer").append(html);

            /* write out login form */
            html = "<div class=\"row mt-4\">\n";
            html += "<div class=\"col-lg-4 col-md-3 col-sm-2 col-1\">&nbsp;</div>\n";
            html += "<div class=\"col-lg-4 col-md-6 col-sm-8 col-10\">\n";
            html += "<div id=\"loginContainer\" class=\"card\">\n";
            html += "<div class=\"card-body\">\n";
            html += "<h5 class=\"card-title text-center\">" + window.lang.login.title + "</h5>\n";
            html += "<div id=\"login_error\" class=\"alert alert-danger\"><i class=\"fas fa-times\"></i> "
            html += window.lang.login.error + "</div>\n";
            html += "<form id=\"login\" name=\"login\" onsubmit=\"return false;\">\n";
            html += "<div class=\"form-group\">\n";
            html += "<label for=\"identity\">" + window.lang.login.user + ":</label>\n";
            html += "<input type=\"text\" class=\"form-control field-check\" id=\"identity\" ";
            html +=  "name=\"identity\" placeholder=\"" + _me.rndUserName() + "\">\n";
            html += "<small>" + window.lang.login.usrCase + "</small>";
            html += "</div>\n";
            html += "<div class=\"form-group\">\n";
            html += "<label for=\"challenge\">" + window.lang.login.passwd + ":</label>\n";
            html += "<input type=\"password\" class=\"form-control field-check\" id=\"challenge\" ";
            html += "name=\"challenge\" placeholder=\"" + window.lang.login.passwd + "\">\n";
            html += "</div>\n";
            html += "<div class=\"text-center\">\n";
            html += "<button class=\"btn btn-dark btn-purple text-center\" id=\"login_btn\">";
            html += "<span id=\"login_btn_txt\"><i class=\"far fa-sign-in-alt\"></i> ";
            html += window.lang.login.button + "</span>";
            html += "<div id=\"login_btn_spinner\" class=\"spinner-border spinner-border-sm btn-spinner\" role=\"status\">";
            html += "<span class=\"sr-only\">Loading...</span>";
            html += "</div></button>\n";
            html += "</div>\n</form>\n</div>\n</div>\n</div>\n";
            html += "<div class=\"col-lg-4 col-md-3 col-sm-2 col-1\">&nbsp;</div>\n";
            html += "</div>";
            $("#login").html(html);
            $("#login_btn").click(_me.doLogin);
            $("#loginLink a").click(function () {
                setTimeout(function () { $("#identity").focus() }, 500);
            });
            calcViewHeight();
        });
    }

    initIface();
}
loginmgmt = new Login();
