/**
 * OpenSong Library Online
 * 
 * version: 2.0
 * file version: 2.0.2
 * updated: 2021-06-18 | 08:30 UTC
 * 
 */

/** JavaScript functions to handle user management functions */

/* User Management Functions object */
window.oslibUsrMgmt = function (_parent) {
    /* properties */
    this.users = [];
    this.lang = window.weAreSecure.lang.usr_mgmt;
    this.lang.title = window.weAreSecure.lang.links.user_management;
    this.saveInProgress = false;

    /* self-referent */
    var _umme = this;

    /* methods */
    /**
     * Function: cancelAddUser
     * Description: cancels the user add feature
     * Arguments: uname = user name whose field to cancel
     * Returns: none
     */
    this.cancelEditUser = function(uname) {
        $("#" + uname + "_edit").hide();
        $("#" + uname + "_name").removeClass("is-valid");
        $("#" + uname + "_name").removeClass("is-invalid");
        $("#" + uname + "_email").removeClass("is-valid");
        $("#" + uname + "_email").removeClass("is-invalid");
        if (uname == "new_user"){
            $("#" + uname + "_name").val("");
            $("#" + uname + "_full_name").val("");
            $("#" + uname + "_email").val("");
            $("#" + uname + "_lock").attr("checked", false);
            $("#" + uname + "_admin").attr("checked", false);
        } else {
            $("#" + uname + "_display").css("display", "flex");
        }
    }

    /**
     * Function: checkForUser
     * Description: checks for a given user
     * Arguments: ufield = field name to check
     * Returns: boolean denoting whether user exists
     */
    this.checkForUser = function(ufield) {
        /* link to field */
        var u = $("#" + ufield);
        var uval = u.val();

        /* clear state */
        u.removeClass("is-invalid");
        u.removeClass("is-valid");
        
        /* regex for valid characters in username */
        regex = /^[A-Za-z0-9_\.\-]+$/;

        if ((uval.length < 3) || _umme.users.includes(uval) || !regex.test(uval) || (uval == "new_user")) {
            u.addClass("is-invalid");
            return true;
        } else {
            u.addClass("is-valid");
            return false;
        }
    }

    /**
     * Function: loadUsers
     * Description: loads the users from database
     * Arguments: none
     * Returns: object containing user data
     */
    this.loadUsers = function () {
        $("#dbicon").fadeIn();
        $.post("process-secured.php", {"action" : "loadUsers", "key" : _parent.seckey}, "", "json").done(
            function (d) {
                if (typeof d.error == "undefined") {
                    _umme.writeUsers(d);
                } else {
                    console.log(d.error);
                }
            }
        ).fail( function (e) { console.log("loadUsers error", e); } );
    }

    /** 
     * Function: showUserForm
     * Description: shows the user form
     * Arguments: uname = user name to show
     * Returns: nothing
     */
    this.showUserForm = function (uname) {
        $("#" + uname + "_edit").css("display", "flex");
        if (uname != "new_user") {
            $("#" + uname + "_display").hide();
            $("#" + uname +"_full_name").focus();
        } else {
            $("#" + uname +"_name").focus();
        }
    }

    /**
     * Function: updateUserList
     * Description: updates the user list
     * Arguments: none
     * Returns: boolean denoting success
     */
    this.updateUserList = function () {

    }

    /**
     * Function: userDel
     * Description: deletes a given user from the database
     * Arguments: i = id of user to delete
     *                'delete confirmed' to delete the active user
     * Returns: nothing
     */
    this.userDel = function (i) {
        if (typeof i == "undefined") { return false; }
        if (i == "delete confirmed") {
            var d= {"action":"mgrDelUser", "user":$("#usr_to_del").val(), "key":_parent.seckey}
            $.post("process-secured.php", d, "", "json").done(
                function (d) {
                    /* reset dialog */
                    $("#modalUserDel").modal("hide");
                    $("#usr_to_del_txt").html("");
                    $("#usr_to_del").val("");
                    
                    /* reset key */
                    if (typeof d.success != "undefined") {
                        _parent.seckey = d.success;
                    } else {
                        console.log(d.error);
                    }

                    /* reload all users */
                     $("#usr_list").html("");
                     _umme.loadUsers();                            
                }
            ).fail(function(e) { console.log("userDel error: ", e); });
        } else {
            $("#usr_to_del_txt").html(i);
            $("#usr_to_del").val(i);
            $("#modalUserDel").modal();
        }
    }

    /**
     * Function: userPwdReset
     * Description: executes the password reset for a user
     * Arguments: i = id of user to edit
     * Returns: nothing
     */    
    this.userPwdReset = function (i) {
        var d = {
            "key" : _parent.seckey,
            "action" : 'resetchallenge',
            "user" : i
        }
        $.post("process-secured.php", d, "", "json").done(
            function (d) {
                if (typeof d.error != "undefined") {
                    console.log("userPwdReset error: ", d);
                } else {
                    if (typeof d.success.key != "undefined") {
                        _parent.seckey = d.success.key;
                        $("#usr_pwrst_tmppwd").html(d.success.tmppwd);
                        $("#usr_pwrst_uid").html(d.success.uid);
                        if (d.success.mail) {
                            $("#usr_pwrst_wmail").show();
                            $("#usr_pwrst_nomail").hide();
                        } else {
                            $("#usr_pwrst_wmail").hide();
                            $("#usr_pwrst_nomail").show();
                        }
                        $("#modalPwdReset").modal();
                    } else {
                        /* error because it didn't work properly */
                    }
                }
            }
        ).fail(function (e) { console.log ("userPwdReset error: ", e); });
        
    }

    /**
     * Function: userSave
     * Description: saves the data for a given user to the database
     * Arguments: i = id of user to save
     * Returns: nothing
     */    
    this.userSave = function (i) {
        if (typeof i == "undefined") { return false; }
        if (_umme.saveInProgress) { return true; }
        _umme.saveInProgress = true;

        var d = {"action":"mgrAddUpdateUser", "key": _parent.seckey}
        $("#uadd_error").slideUp();

        /* check for invalid fields */
        if ($("#" + i + "_user_name").hasClass("is-invalid")) {
            $("#uadd_error").slideDown();
            $("#" + i + "_user_name").focus();
            return false;
        }
        if ($("#" + i + "_user_full_name").hasClass("is-invalid")) {
            $("#uadd_error").slideDown();
            $("#" + i + "_user_full_name").focus();
            return false;
        }
        if ($("#" + i + "_user_email").hasClass("is-invalid")) {
            $("#uadd_error").slideDown();
            $("#" + i + "_user_email").focus();
            return false;
        }

        /* action to do */
        d["what"] = (i == "new") ? "add" : "update";

        /* load items */
        d["uname"] = $("#" + i + "_user_name").val();
        d["ufull"] = $("#" + i + "_user_full_name").val();
        d["umail"] = $("#" + i + "_user_email").val();
        d["ulock"] = ($("#" + i + "_user_lock").is(":checked")) ? 1 : 0;
        d["ulevel"] = ($("#" + i + "_user_admin").is(":checked")) ? "mgr" : "normal";
        d["ulib"] = ($("#" + i + "_user_lib_admin").is(":checked")) ? 1 : 0;

        $.post("process-secured.php", d, "", "json").done(
            function (d) {
                if (typeof d.success != "undefined") {
                    if (d.success.action == "add") {
                        $("#user_add_uid").html(d.success.user);
                        $("#addusr_pwd").html(d.success.challenge);
                        if (d.success.mail) { 
                            $("#usr_add_nomail").hide();
                            $("#usr_add_wmail").show(); 
                        } else {
                            $("#usr_add_nomail").show();
                            $("#usr_add_wmail").hide(); 
                        }
                        $("#modalUserAdded").modal();
                        _umme.cancelEditUser("new_user");
                    } else if (d.success.action == "update") {
                        $("#usr_update_uid").html(d.success.user);
                        $("#modalUserUpdated").modal();
                    }
                    _parent.seckey = d.success.seckey;
                    _umme.saveInProgress = false;

                    /* reload all users */
                    $("#usr_list").html("");
                    _umme.loadUsers();
                } else {
                    $("#uadd_error").slideDown();
                    if (typeof d.error != "undefined") {
                        console.log("userSave error: " + d.error);
                    } else {
                        console.log("userSave error: ", d);
                    }
                }

            }
        ).fail(function (e) { console.log("userSave error: ", e); });
    }

        /**
     * Function: validateEmail
     * Description: validates that an e-mail address is properly formed
     * Arguments: fid = id of field in whose e-mail address to validate
     * Returns: boolean denoting validity
     */
    this.validateEmail = function(fid) {
        if (typeof fid == "undefined") { return false; }
        var f = $("#" + fid);
        var e = f.val();

        /* clear previous check */
        f.removeClass("is-invalid");
        f.removeClass("is-valid");

        /* if the field is empty, just skip it */
        if (e.length < 1) { return true; }

        var regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (regex.test(e)) {
            f.addClass("is-valid");
            return true;
        } else {
            f.addClass("is-invalid");
            return false;
        }
    }

    /**
     * Function: validateUserFullName
     * Description: Makes sure that the user's full name is entered properly (1 char min)
     * Arguments: fid = field id
     * Returns: boolean denoting validity
     */
    this.validateUserFullName = function (fid) {
        f = $("#" + fid);
        f.removeClass("is-invalid");
        f.removeClass("is-valid");
        if (f.val().length < 1) {
            f.addClass("is-invalid");
        } else {
            f.addClass("is-valid");
        }
    }

    /** 
     * Function: writeUserLine
     * Description: writes the line for one user
     * Arguments: ud = user data to write to fields
     * Returns: boolean denoting success
     */
    this.writeUserLine = function (ud) {
        if (typeof ud != "object") { return false; }
        _umme.users[_umme.users.length] = ud.user;

        /* don't write master user out if you aren't the master user */
        if (ud.id == 1 && _parent.usrname != ud.user) { return ""; }

        var html = "";

        /* display */
        html += "<div class=\"row usr-mgmt-item-display\" id=\"" + ud.user + "_display\">\n";
        html += "<div class=\"col-2\">" + ud.user + "</div>\n";
        html += "<div class=\"col-3\">" + ud.userfull + "</div>\n";
        html += "<div class=\"col-3\">" + ud.email + "</div>\n";
        html += "<div class=\"col-1 text-center\"><i class=\"far ";
        html += (ud.lock > 0) ? "fa-check-square" : "fa-square";
        html += "\"></i></div>\n";
        html += "<div class=\"col-1 text-center\"><i class=\"far ";
        html += (ud.level == "mgr") ? "fa-check-square" : "fa-square";
        html += "\"></i></div>\n";
        html += "<div class=\"col-1 text-center\"><i class=\"far ";
        html += (ud.lib > 0) ? "fa-check-square" : "fa-square";
        html += "\"></i></div>\n";
        html += "<div class=\"col-1\">";
        if (_parent.usrname != ud.user && ud.id > 1) {
            html += "<div class=\"btn-group\">";
            html += "<button class=\"btn btn-secondary btn-sm dropdown-toggle\" type=\"button\" ";
            html += "data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
            html += "<i class=\"far fa-ellipsis-h\"></i></button>\n<div class=\"dropdown-menu\">";
            html += "<a class=\"dropdown-item usr-edit\" data-target=\"" + ud.user + "\">";
            html += "<i class=\"far fa-pencil-alt\"></i> " + _umme.lang.but_usredit + "</a>";
            html += "<a class=\"dropdown-item usr-pwd-rst\" data-target=\"" + ud.user + "\">";
            html += "<i class=\"far fa-key\"></i> " + _umme.lang.but_usrpwdrst + "</a>";
            html += "<a class=\"dropdown-item usr-del\" data-target=\"" + ud.user + "\">";
            html += "<i class=\"fal fa-trash-alt\"></i> " + _umme.lang.but_usrdel + "</a>";
            html += "</div></div></div>"
        }  else {
            html += "&nbsp;";
        }
        html += "</div>\n";
        /* edit form */
        html += "<form name=\"" + ud.user + "_form\" onsubmit=\"return false;\">\n";
        html += "<input type=\"hidden\" name=\"" + ud.user + "_user_key\" value=\"" + ud.id + "\">\n";
        html += "<div class=\"row usr-mgmt-item-edit\" id=\"" + ud.user + "_edit\">";
        html += "<div class=\"col-2\"><input type=\"text\" class=\"form-control uname-check\" ";
        html += "name=\"" + ud.user + "_user_name\" id=\"" + ud.user + "_user_name\" value=\"" + ud.user + "\" ";
        html += "placeholder=\"" + _umme.lang.ph_uname + "\" title=\"" + _umme.lang.uname_explanation + "\"></div>";
        html += "<div class=\"col-3\"><input type=\"text\" class=\"form-control ufull-check\" ";
        html += "name=\"" + ud.user + "_user_full_name\" id=\"" + ud.user + "_user_full_name\" ";
        html += "value=\"" + ud.userfull + "\" placeholder=\"" + _umme.lang.ph_fname + "\"></div>";
        html += "<div class=\"col-3\"><input type=\"email\" class=\"form-control uemail-check\" ";
        html += "name=\"" + ud.user + "_user_email\" id=\"" + ud.user + "_user_email\" ";
        html += "value=\"" + ud.email + "\" placeholder=\"" + _umme.lang.ph_email + "\"></div>";
        html += "<div class=\"col-1\"><div class=\"fac fac-checkbox fac-user-checks\">";
        html += "<span></span><input type=\"checkbox\" name=\"" + ud.user + "_user_lock\" ";
        html += "value=\"1\" id=\"" + ud.user + "_user_lock\"";
        html += (ud.lock > 0) ? " checked" : ""; 
        html += "><label for=\"" + ud.user + "_user_lock\">&nbsp;</label></div></div>";
        html += "<div class=\"col-1\"><div class=\"fac fac-checkbox fac-user-checks\">";
        html += "<span></span><input type=\"checkbox\" name=\"" + ud.user + "_user_admin\" ";
        html += "value=\"1\" id=\"" + ud.user + "_user_admin\"";
        html += (ud.level == "mgr") ? " checked" : "";
        html += "><label for=\"" + ud.user + "_user_admin\">&nbsp;</label></div></div>";
        html += "<div class=\"col-1\"><div class=\"fac fac-checkbox fac-user-checks\">";
        html += "<span></span><input type=\"checkbox\" name=\"" + ud.user + "_user_lib_admin\" ";
        html += "value=\"1\" id=\"" + ud.user + "_user_lib_admin\"";
        html += (ud.lib > 0) ? " checked" : "";
        html += "><label for=\"" + ud.user + "_user_lib_admin\">&nbsp;</label></div></div>";
        html += "<div class=\"col-1\">";
        html += "<button class=\"btn btn-secondary btn-edit-cancel\" data-target=\"" + ud.user + "\"";
        html += "title=\"" +_umme.lang.but_cancel + "\"><i class=\"far fa-times\"></i></button>";
        html += "&nbsp;<button class=\"btn btn-secondary btn-purple btn-save\" data-target=\"" + ud.user + "\"";
        html += "title=\"" + _umme.lang.but_save + "\"><i class=\"far fa-save\"></i></button>";
        html += "</div>";
        html += "</div>\n</form>\n</div>\n";

        /* return the result */
        return html;
    }

    /** 
     * Function: writeUsers
     * Description: writes out the user data to the screen
     * Arguments: users = data object containing users
     * Returns: boolean denoting success
     */
    this.writeUsers = function (users) {
        if (typeof users != "object") { return false; }
        var i;
        var html = "";
        /* delete users if they are already there */
        _umme.users = [];
        if (users.length < 1) { 
            console.log("User list error: no users found!");
            return false;
        }
        for (i=0; i<users.length; i++) {
            /* add user name to users field */
            html += _umme.writeUserLine(users[i]);
        }
        $("#usr_list").html(html);
        /* link button actions */
        $(".usr-edit").click( function () { _umme.showUserForm($(this).data("target")); });
        $(".usr-pwd-rst").click( function () { _umme.userPwdReset($(this).data("target")); });
        $(".usr-del").click( function () { _umme.userDel($(this).data("target")); });
        $(".btn-save").click( function () { _umme.userSave($(this).data("target")); });

        /* link checking actions */
        $(".uname-check").focusout(function () { _umme.checkForUser($(this).attr("id")); });
        $(".uemail-check").focusout(function () { _umme.validateEmail($(this).attr("id")); });
        $(".ufull-check").focusout(function () { _umme.validateUserFullName($(this).attr("id")); })

        /* enable cancel button */
        $(".btn-edit-cancel").click(function () { _umme.cancelEditUser($(this).data("target")); });


        $("#usr_mgmt").fadeIn();
        $("#dbplaceholder").fadeOut();
        $("#dbicon").fadeOut();
        $("#loadingspinner").fadeOut();
        return true;
    }

    /* private function to write out missing data */
    var initUsrMgmt = function () {
        /* add container */
        $("#mainContainer").append("<div id=\"usr_mgmt\" class=\"dbcontent dbdata\"></div>\n");
        /* fill containter */
        /* modals */
        /* password reset */
        var html = "<!-- Modal for success in resetting password -->";
        html += "<div class=\"modal fade\" id=\"modalPwdReset\" data-backdrop=\"static\" data-keyboard=\"false\"";
        html += " tabindex=\"-1\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">";
        html += "<div class=\"modal-dialog\">\n<div class=\"modal-content\">\n<div class=\"modal-header\">";
        html += "<h5 class=\"modal-title\" for=\"modalPwdReset\">" + _umme.lang.mdl_title + "</h5></div>\n";
        html += "<div class=\"modal-body\">";
        html += "<p id=\"usr_pwrst_txt1\">";
        html += _umme.lang.mdl_txt1.replace("%u%", "<code id=\"usr_pwrst_uid\"></code>") + "</p>";
        html += "<p id=\"usr_pwrst_wmail\">" + _umme.lang.mdl_txt_mail + "</p>\n";
        html += "<p id=\"usr_pwrst_nomail\">" + _umme.lang.mdl_txt_nomail + "</p>\n";
        html += "<p class=\"text-center\"><code id=\"usr_pwrst_tmppwd\"></code></p>";
        html += "<p id=\"usr_pwrst_txt2\">" + _umme.lang.mdl_txt2 + "</p>\n";
        html += "</div>";
        html += "<div class=\"modal-footer\">";
        html += "<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        html += _umme.lang.mdl_btn_close;
        html += "</button>\n</div>\n</div>\n</div>\n</div>";
        /* add user success */
        html += "<!-- Modal for success in adding user -->";
        html += "<div class=\"modal fade\" id=\"modalUserAdded\" data-backdrop=\"static\" data-keyboard=\"false\" ";
        html += "tabindex=\"-1\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">\n";
        html += "<div class=\"modal-dialog\">\n<div class=\"modal-content\">\n<div class=\"modal-header\">\n";
        html += "<h5 class=\"modal-title\" for=\"modalUserAdded\">" + _umme.lang.uadd_success_title +"</h5>";
        html += "</div>\n<div class=\"modal-body\">\n";
        html += "<p id=\"usr_add_txt1\">"; 
        html += _umme.lang.uadd_success_txt1.replace("%u%", "<code id=\"usr_add_uid\"></code>").replace("%apptitle%", document.title);
        html += "</p>\n<p id=\"usr_add_wmail\">" + _umme.lang.mdl_txt_mail + "</p>\n"
        html += "<p id=\"usr_add_nomail\">" +_umme.lang.mdl_txt_nomail + "</p>\n";
        html += "<p class=\"text-center\"><code id=\"addusr_pwd\"></code></p>\n";
        html += "<p id=\"usr_add_txt2\">" + _umme.lang.mdl_txt2 + "</p>\n";
        html += "<p id=\"usr_add_txt3\">" + _umme.lang.uadd_success_txt3 + "</p>\n</div>";
        html += "<div class=\"modal-footer\">\n";
        html += "<button type=\"button\" class=\"btn btn-secondary btn-purple\" data-dismiss=\"modal\">";
        html += _umme.lang.but_ok + "</button>\n</div>\n</div>\n\</div>\n</div>\n";
        /* update user success */
        html += "<!-- Modal for success in updating user -->";
        html += "<div class=\"modal fade\" id=\"modalUserUpdated\" data-backdrop=\"static\" data-keyboard=\"false\" ";
        html += "tabindex=\"-1\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">\n";
        html += "<div class=\"modal-dialog\">\n<div class=\"modal-content\">\n<div class=\"modal-header\">\n";
        html += "<h5 class=\"modal-title\" for=\"modalUserUpdated\">" + _umme.lang.uupd_success_title +"</h5>";
        html += "</div>\n<div class=\"modal-body\">\n";
        html += "<p id=\"usr_add_txt1\">"; 
        html += _umme.lang.uupd_success_txt1.replace("%u%", "<code id=\"usr_update_uid\"></code>").replace("%apptitle%", document.title);
        html += "</p>\n";
        html += "<p id=\"usr_add_txt3\">" + _umme.lang.uadd_success_txt3 + "</p>\n</div>";
        html += "<div class=\"modal-footer\">\n";
        html += "<button type=\"button\" class=\"btn btn-secondary btn-purple\" data-dismiss=\"modal\">";
        html += _umme.lang.but_ok + "</button>\n</div>\n</div>\n\</div>\n</div>\n";
        /* delete user dialog */
        html += "<!-- Modal for User Deletion confirmation -->\n";
        html += "<div class=\"modal fade\" id=\"modalUserDel\" data-backdrop=\"static\" ";
        html += "data-keyboard=\"false\" tabindex=\"-1\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">\n";
        html += "<div class=\"modal-dialog\">\n<div class=\"modal-content\">\n";
        html += "<div class=\"modal-header\">\n";
        html += "<h5 class=\"modal-title\" for=\"modalUserDel\">" + _umme.lang.udel_mdl_title;
        html += "</h5>\n</div>\n";
        html += "<div class=\"modal-body\">\n";
        html += "<input type=\"hidden\" name=\"usr_to_del\" id=\"usr_to_del\" value=\"\">\n";
        html += "<p id=\"usr_del_dialog_txt1\">";
        html += _umme.lang.udel_mdl_txt1.replace("%u%", "<code id=\"usr_to_del_txt\"></code>"); 
        html += "</p>\n<p id=\"usr_del_dialog_txt2\" class=\"text-bold\">";
        html += _umme.lang.udel_mdl_txt2 + "</p>\n</div>\n";
        html += "<div class=\"modal-footer\">\n";
        html += " <button class=\"btn btn-secondary btn-purple\" id=\"user_del_dialog_yes\">" + _umme.lang.but_yes + "</button>\n";
        html += "<button class=\"btn btn-secondary btn-purple ml-3\" data-dismiss=\"modal\">" + _umme.lang.but_no + "</button>\n";
        html += "</div>\n</div>\n</div>\n</div>\n";
        /* heading for user list */
        html += "<div id=\"add_user\"><button id=\"add_user_btn\" class=\"btn btn-secondary btn-purple\">";
        html += "<i class=\"far fa-plus\"></i> " + _umme.lang.but_usradd + "</button></div>\n";
        html += "<h1>" + _umme.lang.title + "</h1>\n";
        /* user add error message */
        html += "<div id=\"uadd_error\" class=\"alert alert-danger\"><i class=\"far fa-times-circle\"></i> ";
        html += _umme.lang.uadd_error + "</div>\n"
        html += "<div class=\"row usr-mgmt-head\">\n";
        html += "<div class=\"col-2\">" + _umme.lang.col_uname + "</div>";
        html += "<div class=\"col-3\">" + _umme.lang.col_fname + "</div>";
        html += "<div class=\"col-3\">" + _umme.lang.col_email + "</div>";
        html += "<div class=\"col-1\">" + _umme.lang.col_locked + "</div>";
        html += "<div class=\"col-1\" title=\"" + _umme.lang.col_admin_blurb + "\">" + _umme.lang.col_admin + "</div>";
        html += "<div class=\"col-1\" title=\"" + _umme.lang.col_lib_admin_blurb + "\">" + _umme.lang.col_lib_admin + "</div>";
        html += "<div class=\"col-1\">&nbsp;</div>\n</div>";
        /* new user form */
        html += "<form name=\"new_user_form\" onsubmit=\"return false;\">";
        html += "<div class=\"row usr-mgmt-item-edit\" id=\"new_user_edit\">";
        html += "<div class=\"col-2\"><input type=\"text\" class=\"form-control uname-check\" ";
        html += "name=\"new_user_name\" id=\"new_user_name\" value=\"\" ";
        html += "placeholder=\"" + _umme.lang.ph_uname + "\" title=\"" + _umme.lang.uname_explanation + "\"></div>\n";
        html += "<div class=\"col-3\"><input type=\"text\" class=\"form-control ufull-check\" ";
        html += "name=\"new_user_full_name\" id=\"new_user_full_name\" ";
        html += "value=\"\" placeholder=\"" + _umme.lang.ph_fname + "\"></div>";
        html += "<div class=\"col-3\"><input type=\"email\" class=\"form-control uemail-check\" ";
        html += "name=\"new_user_email\" id=\"new_user_email\"";
        html += "value=\"\" placeholder=\"" + _umme.lang.ph_email + "\"></div>\n";
        html += "<div class=\"col-1\"><div class=\"fac fac-checkbox fac-user-checks\">";
        html += "<span></span><input type=\"checkbox\" name=\"new_user_lock\" value=\"1\"";
        html += "id=\"new_user_lock\"><label for=\"new_user_lock\">&nbsp;</label></div></div>\n";
        html += "<div class=\"col-1\"><div class=\"fac fac-checkbox fac-user-checks\">"
        html += "<span></span><input type=\"checkbox\" name=\"new_user_admin\" value=\"1\"";
        html += "id=\"new_user_admin\"><label for=\"new_user_admin\">&nbsp;</label></div></div>\n";
        html += "<div class=\"col-1\"><div class=\"fac fac-checkbox fac-user-checks\">"
        html += "<span></span><input type=\"checkbox\" name=\"new_user_lib_admin\" value=\"1\"";
        html += "id=\"new_user_lib_admin\"><label for=\"new_user_lib_admin\">&nbsp;</label></div></div>\n";
        html += "<div class=\"col-1\">";
        html += "<button class=\"btn btn-secondary btn-edit-cancel\" id=\"new_user_cancel_btn\"";
        html += "title=\"" +_umme.lang.but_cancel + "\" data-target=\"new_user\"><i class=\"far fa-times\"></i></button>";
        html += "&nbsp;<button class=\"btn btn-secondary btn-purple btn-save\" data-target=\"new\"";
        html += "title=\"" + _umme.lang.but_save + "\"><i class=\"far fa-save\"></i></button>";
        html += "</div>";
        html += "</div>\n</form>\n";
        /* div for user list */
        html += "<div id=\"usr_list\"></div>";
        $("#usr_mgmt").html(html);
        $("#add_user_btn").click(function () { _umme.showUserForm("new_user"); });
        $("#user_del_dialog_yes").click(function () { _umme.userDel("delete confirmed"); })
        calcViewHeight();
    }

    initUsrMgmt();
}

if (typeof window.weAreSecure.weAreMgr != "undefined") {
    window.weAreSecure.weAreMgr = new oslibUsrMgmt(window.weAreSecure); 
}
