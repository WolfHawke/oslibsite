/**
 * OpenSong Library Online
 * 
 * version: 2.0
 * file version: 2.0.2
 * updated: 2021-06-18 | 08:26 UTC
 * 
 */

/* Page initialization JavaScript */

/** 
 * Function: loadInterface
 * Description: triggers the language interface load
 * Arguments: none
 * Returns: nothing
 */
function loadInterface() {
    $.post("process.php", {"action":"loadlang"}, "", "json").done(
        function(d) {
            if (typeof d.error != "undefined" && d.error == "nosetup")  {
                writeLanguageStrings(d);
                $("footer").hide();
                $("#navList").hide();
                $("nav form").hide();
                $("#loadingspinner").hide();
                $("#splashscreen").fadeOut();
                if (typeof d.key != "undefined ") {
                    $("#modalSetupNotComplete button").on("click", function() { window.location = "./setup/?unlock-code=" + d.key; })
                }
                $("#modalSetupNotComplete").modal();
            } else if (typeof d.language != "undefined") {
                window.ipBanned = d.ip_banned;
                $(window).bind('hashchange', hashNav);
                calcViewHeight();
                window.onresize = calcViewHeight;            
                if (writeLanguageStrings(d)) {
                    getStats();
                    $("#splashscreen").fadeOut();
                    hashNav();
                }
            } else {
                console.log("Error loadInterface", d); 
            }
        }).fail(function(e) { console.log ("Error loadInterface", e); });
}

/** 
 * Function: writeLanguageStrings
 * Description: writes the language strings to the interface
 * Arguments: d = language string object
 * Returns: boolean
 */
function writeLanguageStrings(d) {
    var html = "";
    
    window.lang = {};
    document.title = d.strings.title;

    $("html").attr("lang", d.lang);

    $("#title_txt").html(d.strings.title);

    /* nav bar */
    $("#displayList").append(d.strings.display_link);
    $("#displayListAllTitles").append(d.strings.display_all_titles_link);
    $("#displayListOfficialTitles").append(d.strings.display_official_titles_link);
    $("#displayListAuthorLast").append(d.strings.display_authors_last_link);
    $("#displayListAuthorFirst").append(d.strings.display_authors_first_link);
    $("#aboutLink").append(d.strings.about_link);
    $("#searchbox").attr("placeholder", d.strings.search_box_placeholder);

    /* footer */
    $("#lastUpdateText").html(d.strings.footer_last_updated);
    $("#totalSongsTxt").html(d.strings.footer_total_songs);
    $("#updategood").attr("title", d.strings.footer_upgood_title);
    $("#updatebad").attr("title", d.strings.footer_upgood_title);
    $("#updatingspinner").attr("title", d.strings.footer_upgood_title);
    $("#refreshdb").attr("title", d.strings.footer_refreshdb_title);

    /* return links */
    $(".back-to-list").append(d.strings.link_back_to_list);

    /* setup incomplete modal */
    $("#modalSetupNotComplete div.modal-body").html(d.strings.modal_no_setup.msg),
    $("#modalSetupNotComplete div.modal-footer button").html(d.strings.modal_no_setup.button); 

    /* first run messages */
    $("#firstrunmsg").html(d.strings.firstrun.msg_init)
    window.lang.firstRun = d.strings.firstrun.msg_cont;
    
    /* legend text */
    window.lang.legend = d.strings.legend;

    /* sort order text */
    window.lang.sortOrder = d.strings.sort_order;

    /* detail text */
    window.lang.details = d.strings.song_details;

    /* search results text */
    window.lang.searchResults = d.strings.search_results;

    /* about page */
    html = "<h1>" + d.strings.about_link + " " + d.strings.title + "</h1>";
    html += d.strings.about_pg;
    $("#about").html(html);

    /* if https, show log in link */
    if (window.location.protocol == "https:" && !window.ipBanned && d.login_on) {
        window.lang.login = {
            "login_link" : d.strings.log_in_link,
            "logout_link" : d.strings.log_out_link,
            "title" : d.strings.login.title,
            "error" : d.strings.login.error,
            "user" : d.strings.login.user_title,
            "passwd" : d.strings.login.pwd_title,
            "button" : d.strings.login.btn_text,
            "usrCase" : d.strings.login.user_case
        }
        jQuery.getScript("js/login.js");
    }

    return true;
}

$(document).ready(function() {
    loadInterface();
    $(".back-to-list").on("click", function() {
        window.history.back();
    })
    var navMain = $(".navbar-collapse"); // avoid dependency on #id
        // "a:not([data-toggle])" - to avoid issues caused
        // when you have dropdown inside navbar
        navMain.on("click", "a:not([data-toggle])", null, function () {
            navMain.collapse("hide");
    });
});
