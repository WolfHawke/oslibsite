<?php
/*
 * IPCA OpenSong Library Database Online Version
 * Remote Database update script
 *
 * Copyright © 2014,2015 Hawke AI. All Rights Reserved.
 *
 * Last Updated: 2015-03-20 | JMW
 *
 */

/* load requirements */
require('components/definitions_remote.php');
require('components/functions_remote.php');
require('components/pppkeys.php');

/* set timezone to UTC for epoch */
if (date_default_timezone_get() != "UTC") {
	date_default_timezone_set("UTC");
}

/* set interval for epoch to be valid in seconds
 * default is 10 seconds
 */
$interval = 60;
$input = file_get_contents('php://input');
$querystring = gzinflate($input); /* expand compressed received data */
parse_str($querystring,$rd);


/* make sure we've got a post array */
if (isset($rd['key'])) {
	$local_key = $keys[$rd['number']];
	$local_epoch_top = date("U") + $interval;
	$local_epoch_bottom = date("U") - $interval;
	$local_hash = hash("sha256",$p_cleartext);

	/* validate data */
	if ($local_key == $rd['key']) {
		if (($rd['epoch'] >= $local_epoch_bottom) && ($rd['epoch'] <= $local_epoch_top)) {
			if ($rd['hash'] == $local_hash) {
				if ($rd['data'] == 'cleardb') {
					$tables = array("os_aka_xlink","os_authors","os_sa_xlink","os_songs","os_lyrics","os_status");
					exQuery('INSERT','os_status',array('total_songs' => 0));
					foreach ($tables as $table) {
						$success = emptyTable($table);
					}
					if ($success) {
						print "success";
					}	else {
						print "unable to empty database";
					}
				} else {
					$json = json_decode($rd['data']);
					if (isset($json->table)) {
						foreach ($json->data as $row) {
							$row_data = array();
							foreach ($row as $key => $data) {
                                /* fix issue of some keys being passed as strings */
                                if (strpos($key,"_key") !== FALSE){
                                    $row_data[$key] = intval($data);
                                } else {
								    $row_data[$key] = fixApostrophe($data);
                                }
							}

							$success = exQuery('INSERT',$json->table,$row_data);
						}
						if ($success) {
							print "success";
						} else {
							print "error processing {$json->table}";
						}
					} else {
						print "incorrect json passed";
					}
				}
			} else {
			 print "password incorrect";
			 die();
			}
		} else {
			print "request timed out: epoch {$rd['epoch']} sought in range {$local_epoch_bottom} - {$local_epoch_top}";
			die();
		}
	} else {
		print "bad key";
		die();
	}
} else {
	print "no key or data passed";
	die();
}

/* if everything is okay, execute funcitons */

?>
