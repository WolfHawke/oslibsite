<?php
/**
 * OpenSong Library Display Site
 * Action processing script when site is secured (logged in)
 * 
 * version: 2.0
 * file version: 2.0.1
 * updated: 2021-06-18 | 07:33 UTC
 *  
 * Copyright © 2020, 2021 Hawke AI
 */

$dev = FALSE;

/* start session */
session_start();

/* set language */
$lang = 'en';

/* load external libraries */
require_once("../vendor/autoload.php");
require_once("lib/pCloud/autoload.php");

/* instantiate logger */
$logger = new Katzgrau\KLogger\Logger(__DIR__."/../log/", Psr\Log\LogLevel::DEBUG, array('extension'=>'log', 'prefix'=>'oslibweb_'));

/* load language */
include ("lang/{$lang}.php");

if (!file_exists("config/defs.php")) {
    print(json_encode(array("error"=>"nosetup", "language" => $lang, "strings" => $LANGUAGE)));
    die();
}

if ($dev !== TRUE && (!isset($_SESSION['key']) || !isset($_SESSION['user']) || !isset($_SESSION['uid']) || !isset($_SESSION['level']))) {
    print(json_encode(array("error"=>"nologin")));
    die();
}

/* load local functions */
include ("config/defs.php");
include ("components/db_functions.php");
include ("components/pCloud.php");
include ("components/user.php");
include ("components/sets.php");
include ("components/comlink.php");

/* make sure we're either in dev mode or we're being called from the proper location */
if ($dev === TRUE) {
    if (isset($_GET['action'])) {
        $input = $_GET;
    } elseif (isset($_POST['action'])) {
        $input = $_POST;
    } else {
        print("No action passed"); die();
    }
    
} else {
    if (isset($_POST['action']) && isset($_SERVER['HTTP_REFERER']) 
        && strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) !== FALSE) {
        $input = $_POST; 
    } else {
        print("Nope. You're not real.");
        die();
    }
}

if (isset($input['key']) && checkKey($input['key'])) {

    switch($input['action']) {

        /* get all sets */
        case "getAllSets":
            $s = procSongs('get-list', $access_token, $sets_dir);
            if (is_array($s)) {
                $c = array_column($s, 'name');
                array_multisort($c, SORT_ASC, $s);
                print(json_encode(array('seckey' => genKey(), 'sets' => $s)));
            } else {
                print(json_encod(array('error' => 'unable to get sets')));
            }
            break;

        /* load existing set */
        case "loadSet":
            if (isset($input['sn'])) {
                /* get file using file id */
                $sf = procSongs('get-song', $access_token, $sets_dir, intval($input['sn']));
                if (is_array($sf)) {
                    print(json_encode(loadSet($sf['name'])));
                    unlink("./newsongs/{$sf['name']}");
                } else {
                    print(json_encode(array("error" => "no download")));
                }
            } else {
                print(json_encode(array("error" => "no file passed")));
            }
            break;

        /* load User info */
        case "loadUsers":
            if (checkKey($input['key'])){
                print(getAllUsers());
            } else {
                print(json_encode(array("error" => "no action defined")));
            }
            break;

        /* manager: add user info */
        case "mgrAddUpdateUser":
            if ($_SESSION['level'] == 7) {
                $what = ($input['what'] == 'add') ? 'insert' : $input['what']; /* i.e. update */
                /* set up array with user fields */
                $uarray = array("user_name" => $input['uname'], "user_full_name" => $input['ufull'],
                                "user_email" => $input['umail'], "user_lock" => $input['ulock'],
                                "user_lib_admin" => $input['ulib']);
                $uarray["user_admin"] = ($input['ulevel'] == "mgr") ? 7 : 0;              
                /* execute update */
                print(userAddEdit($what, $uarray));
            } else {
                print(json_encode(array('error' => 'insufficient privileges')));
            }
            break;

        /* manager: delete user */
        case "mgrDelUser":
            if ($_SESSION['level'] == 7) {
                print(userDelete($input['user']));
            } else {
                print(json_encode(array('error' => 'insufficient privileges')));
            }
            break;
        
        /* manager: reset user password */
        case 'resetchallenge':
            if ($_SESSION['level'] == 7) {
                $MAILKEY = randomId();
                $p = resetPassword($input['user']);
                if (isset($p['success'])){
                    $p['success']['key'] = genKey();
                }
                print(json_encode($p));
            } else {
                print(json_encode(array('error' => 'insufficient privileges')));
            }
            break;

        /* save Set */
        case "saveSet":
            $MAILKEY = randomId();
            $s = saveSet($input['fn'], $input['sets']);
            if ($s == 'success') {
                print(json_encode(array('success' => genKey())));
            } else {
                print(json_encode(array('error' => $s)));
            }
            break;

        /* save Song */
        case 'saveSong':
            $MAILKEY = randomId();
            $s = addSong($input);
            if (is_array($s)) {  
                sendAddSong($input['title'], $s);
                print(json_encode(array('success' => array('seckey' => genKey(), 'songName' => $input['title'], 'fileName' => $s['filename']))));
            } else {
                print(json_encode(array('error' => 'no write')));
            }
            break;

        /* update user info */
        case "updateUser":
            if (isset($input['what'])) {
                if ($input['what'] == 'fullname') {
                    if (isset($input['user_full_name'])) {
                        updateUser($_SESSION['uid'], 'name', $input['user_full_name']);
                        $_SESSION['ufull'] = $input['user_full_name'];
                    } /* if nothing is passed, do nothing */
                    print(json_encode(array("success" => genKey())));
                } elseif ($input['what'] == 'challenge') {
                    /* check for old password */
                    if (checkPwd($_SESSION['user'], $input['old_pwd'])) {
                        if (updateUser($_SESSION['uid'], 'pwd', $input['new_pwd'])) {
                            if ($_SESSION['pwdReset'] > 0) {
                                updateUser($_SESSION['uid'], 'pwdLock', FALSE);
                                $_SESSION['pwdReset'] = 0;
                                $logger->info("{$_SESSION['user']} created a new password after a password reset.");
                            } else {
                                $logger->info("{$_SESSION['user']} changed their password.");
                            }
                            print(json_encode(array("success" => genKey()))); 
                        } else {
                            print(json_encode(array("error" => "password not updated")));
                        }
                    } else {
                        print(json_encode(array("error" => "old password bad")));
                    }
                } elseif ($input['what'] == 'email') {
                    if (isset($input['val'])) {
                        updateUser($_SESSION['uid'], 'email', $input['val']);
                    } /* if nothing is passed, do nothing */
                    print(json_encode(array("success" => genKey())));
                } else {
                    print(json_encode(array("error" => "no subaction defined")));
                }
            } else {
                print(json_encode(array("error" => "no action defined")));
            }
            break;

        /* validate user */
        case "validateUser":
            $k = genKey();
            $out = array("success" => array("key" => $k, 
                "user" => $_SESSION['user'],
                "level" => ($_SESSION['level'] == 7) ? "mgr" : "normal",
                "userfull" => $_SESSION['ufull'],
                "email" => $_SESSION['umail'],
                "reset" => $_SESSION['pwdReset'],
                "libmgmt" => ($_SESSION['libmin'] == 1) ? true : false,
                ),
            );
            if (isset($input['getLang']) && $input['getLang'] == TRUE) {
                $out['strings'] = $LANGUAGE['secured'];
            }
            print(json_encode($out));
            break;
        
    }
} else {
    print(json_encode(array("error" => "invalid key")));
}
?>
