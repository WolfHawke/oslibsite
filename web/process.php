<?php
/**
 * OpenSong Library Display Site
 * Action processing script
 *
 * version: 2.0
 * file version: 2.0.2
 * updated: 2021-06-18 | 08:21 UTC
 * 
 * Copyright © 2020 Hawke AI
 */

$dev = FALSE;

/* start session */
session_start();

/* set language */
$lang = 'en';

/* load external libraries */
require_once("../vendor/autoload.php");
require_once("lib/pCloud/autoload.php");

/* instantiate logger */
$logger = new Katzgrau\KLogger\Logger(__DIR__."/../log/", Psr\Log\LogLevel::DEBUG, array('extension'=>'log', 'prefix'=>'oslibweb_'));

/* load language */
include ("lang/{$lang}.php");

if (!file_exists("config/defs.php")) {
    print(json_encode(array("error"=>"nosetup", "language" => $lang, "strings" => $LANGUAGE)));
    die();
}

/* load local functions */
include ("config/defs.php");
include ("components/db_functions.php");
include ("components/pCloud.php");
include ("components/procSongFiles.php");
include ("components/display.php");

/* make sure we're either in dev mode or we're being called from the proper location */
if ($dev === TRUE) {
    if (isset($_GET['action'])) {
        $input = $_GET;
    } elseif (isset($_POST['action'])) {
        $input = $_POST;
    } else {
        print("No action passed"); die();
    }
    
} else {
    if (isset($_POST['action']) && isset($_SERVER['HTTP_REFERER']) 
        && strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) !== FALSE) {
        $input = $_POST; 
    } else {
        print("Nope. You're not real.");
        die();
    }
}


switch($input['action']) {

    /* check for IP Ban */
    case 'checkIP':
        print(json_encode(array('ipBan' => checkIPBan())));
        break;

    /* do login */
    case 'dologin':
        /* slow things down */
        if (isset($_SESSION['logonPause'])) {
            if (time() < ($_SESSION['logonPause'] + 2)) {
                print(json_encode(array("waiting"=>TRUE)));
            }
        }

        if (!isset($_SESSION['failedLogons'])) { $_SESSION['failedLogons'] = 0; }
        if ($input['failed'] > 0) { $_SESSION['failedLogons'] = $input['failed']; }
        /* if we exceed 5 failed logons */
        if ($_SESSION['failedLogons'] > 5) { 
            banIP(); 
            lockUser($input['id']);
        }
        if (!checkIPBan()) {
            $u = exQuery('SELECT', 'os_users', '*', "`user_name` = '{$input['id']}'");
            if (is_array($u)) {
                if (password_verify($input['challenge'], $u[0]['user_pass']) && $u[0]['user_lock'] == 0) {
                    $_SESSION['key'] = randomId(32);
                    $_SESSION['user'] = $input['id'];
                    $_SESSION['uid'] = $u[0]['user_key'];
                    $_SESSION['level'] = $u[0]['user_admin'];
                    $_SESSION['ufull'] = $u[0]['user_full_name'];
                    $_SESSION['umail'] = $u[0]['user_email'];
                    $_SESSION['libmin'] = $u[0]['user_lib_admin'];
                    $_SESSION['pwdReset'] = $u[0]['user_pwdreset'];             
                    unset($_SESSION['failedLogons']);
                    unset($_SESSION['logonPause']);
                    print(json_encode(array("success" => $_SESSION['key'])));
                } else {
                    $_SESSION['failedLogons']++;
                    $_SESSION['logonPause'] = time();
                    print(json_encode(array("error" => "bad challenge")));
                }
            } else {
                $_SESSION['failedLogons']++;
                $_SESSION['logonPause'] = time();
                print(json_encode(array("error" => "bad identity")));
            }
        } else {
            print(json_encode(array("error" => "ip banned")));
        }
        break;
    
    case 'dologout':
        $fields = ['key', 'user', 'uid', 'level', 'ufull', 'umail'];
        foreach ($fields as $f) {
            unset($_SESSION[$f]);
        }
        print(json_encode(array("success" => true)));
        break;
    
    
    /* build entire database on first run */
    case 'firstrun':
        /* get list  of songs */
        if ($input['fid'] == 0) {
            $out = procSongs('get-list', $access_token, $proc_dir);
            if (!is_array($out)){
                $out = array("error" => $out);
            }
            print(json_encode($out));
        } elseif ($input['fid'] == 'complete') {
            /* write the last update time */
            $f = @fopen("config/last_check.txt", "w");
            fwrite($f, intval(date("U")));
            fclose($f);

            /* update the statistics */
            $q = updateStats();
            if ($q === TRUE) {
                print(json_encode(array("success" => "initial database import complete")));
            } else {
                print(json_encode(array("error" => "unable to update stats: {$q}")));
            }
        } else {
            $song = procSongs('get-song', $access_token, $proc_dir, intval($input['fid']));
            if (is_array($song)) {
                if (insertSong($song['name'], parseSong("./newsongs/{$song['name']}"), $song['modified']) == TRUE) {
                    print(json_encode(array("success" => $song['id'])));
                } else {
                    print(json_encode(array("error" => "unable insert {$song['name']}")));
                }
                /* remove song file to save space */
                try {
                    unlink("./newsongs/{$song['name']}");
                } catch (Exception $e) {
                    $logger->error("Unable to unlink ./newsong/{$song['name']}", $e);
                }
            } else {
                /* $song here because we're returning the text for an Exception */
                print(json_encode(array("error" => $song))); 
            }
        }
        break;

    /* get database stats */
    case 'getStats':
        print(getStats());
        break;

    /* load songs from database */
    case 'loadFromDb':
        if (!isset($_SESSION['timeToUpdate'])) { $_SESSION['timeToUpdate'] = timeToUpdate(); }
        switch ($input['item']) {
            case 'authors':
                print(getAuthorList($input['sort']));
                break;
            case 'songs':
                if ($input['sort'] == "details") {
                    print(getSongDetails($input['song_id']));
                } else {
                    print(getSongList($input['sort']));
                }
                break;
        }
        break;
    
    /* load language */
    case 'loadlang':
        $l = array("language" => $lang, "strings" => $LANGUAGE);
        if (isset($app_title)) {
            $l["strings"]["title"] = $app_title;
        } 
        /* check to see if database exists */
        if (intval(countTables()) < 7) { 
            $l['error'] = 'nosetup'; 
            $l['key'] = $unlock_code;
        } else {
            /* check to see if IP has been banned */
            $l['ip_banned'] = checkIPBan();
            /* check to see if user admin exists */
            $l['login_on'] = isLoginEnabled();
        }
        print(json_encode($l));
        break;

    /* maximum login errors on frontend reached */
    case 'maxFailedLogins':
        banIP();
        if ($input['u'] != '') {
            lockUser($input['u']);
        }
        print(json_encode(array("action"=>"ip banned")));
        break;

    /* insert new songs into database */
    case 'procNewSongs':
        $out = array("error" => array());
        $updated = FALSE;
        /* download new songs */
        $ns = procSongs('get-all', $access_token, $new_dir);
        if (count($ns) > 0) {
            foreach ($ns as $s){
                $sf = "./newsongs/{$s['name']}";
                /* check XML for user2 */
                $sx = parseSong($sf);
                if (intval($sx['user2']) > 0) {
                    $updated = TRUE;
                    if (writeSongIdToXML($sx['user2'], $s['name'])) {
                        /* upload to service */
                        $sp = procSongs('put', $access_token, $proc_dir, $s['name']);
                        if (is_array($sp)) {
                            /* insert to database */
                            insertSong($s['name'], $sx, $sp['modified']);
                            /* delete  old song on remote */
                            procSongs('del', $access_token, $new_dir, $s['id']);
                        } else {
                            $msg = "The song \"{$s['name']}\" could not be uploaded to remote site. It was not entered in the database.";
                            $logger->error($msg);
                            $out["error"][] = $msg;
                        }
                    } else {
                        $msg = "The song id {$sx['user2']} for \"{$s['name']}\" not updated.";
                        $logger->error($msg);
                        $out["error"][] = $msg;
                    }
                    /* delete local copy of song */
                    unlink($sf);
                }
            }
        }
        /* remove */
        if (count($out['error']) > 0 ) {
            print(json_encode($out));
        } elseif ($updated) {
            updateStats();
            print(json_encode(array("success" => "updated")));
        } else {
            print(json_encode(array("success" => "noupdate")));
        }
        break;
    
    /* search */
    case 'search':
        if (isset($input['q'])) {
            print(doSearch($input['q']));
        } else {
            print(json_encode(array("error" => "no search terms")));
        }
        
        break;
    
    /* check for changes to songs in database */
    case 'updateSongs':
        $result = array();
        $osf = procSongs('get-list', $access_token, $proc_dir);
        $osdbq = exQuery('SELECT', 'os_songs', 'song_id, song_filename, song_modified', 'song_aka = 0');
        foreach ($osf as $s) {
            foreach ($osdbq as $si) {
                if ($si['song_filename'] == $s['name'] && $si['song_modified'] != $s['modified']) {
                    $song = procSongs('get-song', $access_token, $proc_dir, intval($s['id']));
                    if (is_array($song)) {
                        if (insertSong($song['name'], parseSong("./newsongs/{$song['name']}"), $song['modified']) == TRUE) {
                            $result[] = $song['id'];
                        } else {
                            print(json_encode(array("error" => "unable insert {$song['name']}")));
                        }
                        /* remove song file to save space */
                        try {
                            unlink("./newsongs/{$song['name']}");
                        } catch (Exception $e) {
                            $logger->error("Unable to unlink ./newsong/{$song['name']}", $e);
                        }
                    } else {
                        /* $song here because we're returning the text for an Exception */
                        print(json_encode(array("error" => $song))); 
                    }
                }
            }
        }
        print(json_encode(array("update" => "complete", "updated_songs" => $result)));
        $_SESSION['timeToUpdate'] = FALSE;
        break;


    /* do a database update */
    case 'updateStats':
        print(json_encode(array("stats"=> updateStats())));
        break;

}

?>
