<?php
/**
 * OpenSong Library Display Site
 * Site Definitions
 * 
 * Last Updated: {$date}
 */

/* pCloud connection data */
/* To get your pCloud access token run getAccessToken.php from your browser */
$access_token = '';

/* OpenSong directories */
/* path in pCloud to directory containing Open Song Data subfolders (leave off trailing slash) */
$os_dir ='/OpenSong/OpensSong Data';
/* path in pCloud to directory containing new songs (leave off trailing slash) */
$new_dir = '/OpenSong/OpenSong Data/Songs/New Songs';
/* path in pCloud to directory where processed songs are kept (leave off trailing slash) */
$proc_dir = '/OpenSong/OpenSong Data/Songs/English';
/* path in pCloud to directory where sets are kept (leave off trailing slash) */
$sets_dir = '/OpenSong/OpenSong Data/Sets';

/* App title to display on front page */
$app_title = 'OpenSong Library Online';

/* Code to unlock the site. Should be 15-character alphanumeric string */
$unlock_code = '0123456789AbCdE';

/* database connection data */
$db_host = 'localhost';
$db_base = 'oslib';
$db_user = 'user';
$db_pass = 'password';

