# OpenSong Library Online README #

OpenSong library online is a PHP (back-end) and JavaScript (front-end) based web application that allows its users to access an OpenSong database on a [pCloud](https://www.pcloud.com) share from the internet. See **How OpenSong Library Online Works** for details as to how the pCloud share needs to be set up.

* Version 2.0
* Features
    * Fully integrated with OpenSong 2.2.7 and higher.
    * Lists songs according to song title, official title, author first or last name.
    * Show song details, including lyrics and copyright information.
    * Automatic update of the online database from vetted songs.
    * Search for song titles or authors
    * User interface to allow users to add songs, create and edit sets.
    * Ready for localization to other languages.
* System requirements: 
    * PHP 7.3 or higher
    * MySQL / MariaDB database
    * Web browser with JavaScript enabled
    * pCloud account with file structure as defined in **How OpenSong Library Online Works**
* License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International ([CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)).

## Setting up OpenSong Library Online ##
1. Clone or download the repository to the server.
2. Configure your web server to serve up the `oslibsite/web/` directory.
3. Access the site and click the button to start the setup process. The form should be pretty self-explanatory. For details, look at the **How OpenSong Library Online Works** section below.
4. Once the setup is complete, access the front page and let OpenSong Online populate the site.
5. You can copy `web/lang/en-about-example.html` to web/lang/en-about.html to create your own about text.

## How OpenSong Library Online Works ##
### Directory Structure ###
OpenSong Library Online is designed to access an OpenSong library stored in a pCloud share. The OpenSong Data folder in the pCloud folder must have at least the Songs and Sets directories in it. The Songs directory must have at least one subdirectory for new songs in it. It is recommended to have a second subdirectory for the processed songs. Below is a sample directory structure relative to the pCloud root:

    OpenSong/
        OpenSong Data/
            Backgrounds/
            Sets/
            Settings/
            Songs/
                Processed/
                New/

Thus, when setting up the OpenSong Library online for the first time, you would enter `/OpenSong/OpenSong Data` in the *Path to OpenSong data directories* field. Then you would enter `/Processed` in the *Directory containing processed (displayed) songs* and `/New` in the *Directory containing new songs* field. OpenSong checks to see if the Sets directory exists in relation to the *Path to OpenSong data directories* field. 

These directories can all be empty when starting up, but it is recommended to at least have songs in the processed songs directory that have been set up according to the information below.

### OpenSong Song File Requirements ###
OpenSong Library online tracks the songs using the <user2> field in the OpenSong Song XML file (see [OpenSong’s File Format Specifications](http://www.opensong.org/home/file-formats) for details). These should be numbered sequentially with unique integers beginning from `1`. You can use the `set_song_numbers.py` tool on any desktop with the pCloud client installed to do this automatically (it requires the xmltodict package to work). 

The <user1> field is used to store the location from which the lyrics and chords were sourced.

The following fields in the OpenSong files are required for OpenSong Library Online to function:
* Title
* Author
* Lyrics

If entering multiple authors, their names should be separated with a `;` and entered as first name last name (e.g `Martin Luther; Johann Sebastian Bach`).

It is strongly recommended to enter the CCLI number for the song (this can be looked up using [SongSelect](https://songselect.ccli.com/)), copyright information and the presentation order, as well. OpenSong Library Online will work without these, though.

### Loading Processed Songs into the Database ###
The loading of already processed songs into the website functions as follows: OpenSong Library Online, pulls the full list of available songs from pCloud. Each song is then individually downloaded, parsed, and the data from the song are written to the database. Depending on the size of your library, this could take quite a while. Please do not close your browser while the initial load happens. Invalid XML files will be skipped.

After this the web site will check daily for updates to the database, first in the new songs folder (see below for details), then in the processed songs folder. Any changes to the files will be added into the database. All modifications to the song database are noted in the logs.

### Adding Songs to the Database ###
Songs can be added to the database from the new songs directory defined when OpenSong Library Online was set up. If you use the Add Song interface on the website, songs will be saved to that location in the OpenSong XML format. Songs entered by the OpenSong application should be saved there. OpenSong Library Online assumes that you have a library administrator who will check the uploaded or added songs and make sure that they follow the proper formats used by OpenSong before designating them for addition to the online database. When a song has been checked, the User 2 field should be either set to `0` or `*`. That way OpenSong Library Online will know to parse the XML file and add it to the online database.

### Registered User Area and Site Security ###
OpenSong Online maintains a registered-user-only portion of the site that allows users to add songs to the database and create and edit sets. This is useful if you have several different song leaders who need access to the library away from the primary computer. The registered user area is specifically designed to *not have* a self-serve signup function. The idea behind this is that OpenSong Library Online is meant only for vetted users of a given church or organization. It‘s not meant for general use. Thus, the main admin is the one who adds and removes users. 

Users log in using user names, not e-mail addresses. User names must be at least 3 characters long and consist of ASCII upper- and lowercase characters, digits and the characters `.`, `-` or `_`. User names are case sensitive. Passwords need to be at least 8 characters long and contain at least one upper case and one lower case character and one digit. Special characters are highly recommended, but cannot be `'`, `"` or `\`. These limitations are to prevent the use of special JavaScript, PHP or SQL commands to try to hack the site, since those all require the use of the `\` for escaping characters and `'` and `"` for encapsulating strings.

Users can update their full name and e-mail address and change their password on the profile page. Avatars are not supported for simplicity’s sake.

Users will be locked out of the site after five failed attempts to sign in and their IP address will be banned for 72 hours. If a lock-out occurs, the login screen will be disabled. An administrator with user management rights can unlock a locked-out user. The login form forces the user to wait one second between failed logins to mitigate against brute-force attacks. 

When users are created, they are assigned a temporary password for first entry into the site, after which point they can define their own password. If a user forgets their password, a user admin can reset their password and obtain a temporary password. If an e-mail address is defined for the given user, then they will be e-mailed the temporary password. Otherwise the user admin needs to communicate the temporary password to the user by other means.

There are two kinds of administrators:
1. The user admin can add or remove users (other than the main admin with user id 1) and reset their passwords.
2. The library admin will be e-mailed when a user adds a song to the database via the Add Song interface. They can also initiate a manual library update.

These two admin types can be used in any combination. The main admin cannot be deleted or modified except by using the `mainadmintool.php` commandline tool (see below). The main admin will *always* be designated as both a user admin and a library admin. If the main admin does not want to get notifications, simply set their e-mail address to blank.

The registered-user-only portion of the site can be easily disabled by deleting the main admin using the `mainadmintool.php` commandline tool. If no main admin is defined, no one can log in. To (re)activate the registered-user only portion, define a main admin using the `mainadmintool.php` commandline tool.


## Tools ##
### Main Admin Management Tool (mainadmintool.php) ###
OpenSong Library Online contains a command-line maintenance tool to manipulate the main user of the database. This will only work if the web-based setup has been run and the database has been initialized.

Usage: `php mainadmintool.php [unlock-code] [command] [data]`

**Unlock Code:** The value found under `$unlock_code` in `web/config/defs.php`.

#### Commands: ####

`create`

> Create main administrator if it does not exist.

>> A user name is unique and can only consist of upper- and lowercase letters, digits and the symbols `.`, `_` or `-`. 

>> The password must be at least 8 characters long, contain one digit, one uppercase and one lowercase character, and cannot contain the characters `\`, `"` or `'`.

> Data: `[username] [password] [email address (optional)]`

> Example: `php mminadmintool.php 12345678 create humblePie P@ssw0rd me@there.com`

`delete`    

> Deletes main administrator **(This cannot be undone. Use with discretion!)**

>> If the main administrator is deleted, the login function and functions behind it will be disabled.

> Data: `[username]`

> Example: `php mminadmintool.php 12345678 delete humblePie`

`info`

> Shows main administrator name and information


`reset`

> Resets the password of the main administrator. 

>> The password must be at least 8 characters long, contain one digit, one uppercase and one lowercase character, and cannot contain the characters `\ "` or `'`.

> Data: `[username] [password]`

> Example: `php mminadmintool.php 12345678 reset humblePie neW-P@ssw0rd`

### Song Number Setting Tool (set_song_numbers.py) ###
When you prepare your existing OpenSong library to work with OpenSong Library Online, the `set_song_numbers.py` tool makes it possible to easily add the song id numbers to the <user2> field of the OpenSong Song XML file. The usage of the script is simple:

    python set_song_numbers.py [Path to OpenSong processed Songs directory]

On Windows it might look as follows:

    py set_song_numbers.py "P:\OpenSong\OpenSong Data\Songs\Processed"

On Mac it might look like this:

    python3 set_song_numbers.py "/Users/me/pCloud Drive/OpenSong/OpenSong Data/Songs/Processed"

The tool requires the `xmltodict` module to run. You can install it by running `pip install xmltodict` from the command line.

### Contribution guidelines ###

* Issues can be submitted via the bug tracker associated with this repo.
* Contributions can be submitted for bugfixes or for expansion to make it possible to use OpenSong Library Online with other sharing platforms like Sync, OneDrive, Google Drive or Dropbox.
* Code review will be very slow, as this is more of a hobby project.
* Localization of the interface would be nice, if you have time. OpenSong currently is localized to 19 languages. It would be cool if OpenSong Library Online would have the same languages available.

### Who do I talk to? ###

* The repository owner.
