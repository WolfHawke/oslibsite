# script to walk through current songs and update <user2> song numbers sequentially
import os, argparse
try: 
    import xmltodict
except ImportError:
    print('The xmltodict module is required for this script to function. Please install it using the command pip install xmltodict \n')

id = 1

parser = argparse.ArgumentParser(description="Number OpenSong XML files <user2> field sequentially in the defined directory.")
parser.add_argument('path',help="Directory containing the XML files to parse.")

args = parser.parse_args()

path = args.path

if (path[-1] == os.sep):
    path = path[0:-1]

songs = os.listdir(path)
songs.sort()

for s in songs:
    print(path + os.sep + s)
    with open(path + os.sep + s, "r", encoding="utf-8") as f:
        x = xmltodict.parse(f.read())
    x['song']['user2'] = id
    id=id+1
    with open(path + "/" + s, "w", encoding="utf-8") as f:
        f.write(xmltodict.unparse(x))
