<?php
/**
 * OpenSong Library Display Site
 * Tool for updating main admin
 *
 * Copyright © 2020 Hawke AI
 */

/* global variables */
$version = 0.1;

/* functions */
/**
 * color: return a bash color code
 * Returns: string
 * 
 * Colors: black, red, green, brown, blue, purple, cyan, gray, white, lt gray,  lt red, lt green, yellow, lt blue, lt purple, lt cyan
 * Effects: underline, reverse, none
 * 
 * @var string  foreground color
 * @var string  effect (optional)
 * @var string  background color (optional)
 */
function color($fg, $effect='none', $bg='') {
    $colors = array('black' => '0;30', 'red' => '0;31', 'green' => '0;32', 'brown' => '0;33', 'blue' => '0;34', 'purple' => '0;35',
                    'cyan' => '0;36', 'gray' => '1;30', 'white' => '1;37', 'yellow' => '1;33', 'lt gray' => '0;37',
                    'lt red' => '1;30', 'lt green' => '1;32', 'lt blue' => '1;34', 'lt purple' => '1;35', 'lt cyan' => '1;36');
    $effects = array('underline' => '4', 'reverse' => '7');

    $out = "\e[" . $colors[$fg];
    if ($effect == 'underline' || $effect == 'reverse') {
        $out .= ";{$effects['$effect']}";
    }
    $out .= ($bg != '') ? str_replace(';3', ';4', substr($colors[$bg], 1)) : '';
    $out .= "m";

    return $out;
}

/**
 * helpScreen: diplays the help screen
 * Returns: string 
 */
function helpScreen() {
    global $version;
    $out = color('yellow') . "OpenSong Library Display Site Main Admin Tool" . color('white') . "
Version: " . color('lt gray') . "{$version}

" . color('white') . "Usage: " . color('lt blue') . "php mainadmintool.php [unlock-code] [command] [data]" . color('lt gray') . "

" . color('white') . "Unlock Code:" . color('lt gray') . " This is the value found under " . color('lt blue') . "\$unlock_code" . color('lt gray') . " in defs.php

" . color('white') . "Commands:" . color('lt gray') . "
" . color('green') . "    create" . color('lt gray') . "      Create main administrator if it does not exist. 
                    A user name is unique and can only consist of upper- and lowercase letters, digits and the symbols " . color('lt blue') . ". _" . color('lt gray') . " or " . color('lt blue') . "-" . color('lt gray') . ".
                    The password must be at least 8 characters long, contain one digit, one uppercase and one lowercase character, 
                    and cannot contain the characters " . color('lt blue') . "\\ \"" . color('lt gray') . " or " . color('lt blue') . "'" . color('lt gray') . ".
                Data: [username] [password] [email address (optional)]

" . color('green') . "    delete" . color('lt gray') . "      Deletes main administrator " . color('red') . "(This cannot be undone. Use with discretion!)" . color('lt gray') . "
                    If the main administrator is deleted, the login function and functions behind it will be disabled.
                Data: [username]

" . color('green') . "    info" . color('lt gray') . "        Shows main administrator name and information

" . color('green') . "    reset" . color('lt gray') . "       Resets the password of the main administrator
                    The password must be at least 8 characters long, contain one digit, one uppercase and one lowercase character, 
                    and cannot contain the characters " . color('lt blue') . "\\ \"" . color('lt gray') . " or " . color('lt blue') . "'" . color('lt gray') . ".
                Data: [username] [password]
";    
    return $out;
}

/**
 * validate: validates a password or a user name
 * Returns: boolean denoting success
 * 
 * @var string  what to validate: 'username' or 'password'
 * @var string  string to validate
 */
function validate($w, $s) {
    switch ($w) {
        case 'username':
            return preg_match("/^[A-Za-z0-9\-_\.]+$/", $s);
            break;
        case 'password':
            if (preg_match("/[\\\"']+/", $s) || strlen($s) < 8) {
                return FALSE;
            } else {
                if (preg_match("/[A-Z]/", $s) && preg_match("/[a-z]/", $s) && preg_match("/[0-9]/", $s)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
            break;
        case 'email':
            return preg_match("/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i", $s);
            break;
    }
    return FALSE;
}

/* check for required files */
if (!file_exists('web/config/defs.php')) {
    print(color('red') . "Error! " . color('lt gray') . "The application has not yet been set up. Please run the setup script through your web browser before continuing.\n");
    die();
}
require_once('web/config/defs.php');

/* check for database connectivity */
require_once('web/components/db_functions.php');

if ($osdb === FALSE) {
    print(color('red') . "Error! " . color('lt gray') . "Unable to connect to database. Please check your defs.php file and try again.\n");
    die();
}

if (count($argv) > 2) {
    if ($argv[1] == $unlock_code) {
        $q = exQuery('SELECT', 'os_users', '*', 'user_key = 1');
        switch($argv[2]) {
            case 'create':
                if (is_array($q)) {
                    print("A main administrator with the user name " . color('lt blue') . "{$q[0]['user_name']}" . color('lt gray') . "already exists.\n");
                    print("You will need to delete the main administrator to create a new one.");
                } elseif (count($argv) > 4 && count($argv) < 7) {
                    /* make sure username is not in use */
                    if (is_array(exQuery('SELECT', 'os_users', '*', "user_name LIKE '{$argv[3]}'"))) {
                        print("A user with the user name " . color('lt blue') . "{$q[0]['user_name']}" . color('lt gray') . "already exists.\n");
                        print("Please choose a different user name.");
                    } else {
                        if (validate('username', $argv[3]) && validate('password', $argv[4])) {
                            $uarr = array('user_key' => 1, 'user_name' => $argv[3], 'user_full_name' => 'Main Admin', 
                                          'user_pass' => password_hash($argv[4], PASSWORD_DEFAULT), 'user_admin' => 7, 'user_lib_admin' => 1);
                            if (isset($argv[5]) && validate('email', $argv[5])) { $uarr['user_email'] = $argv[5]; }
                            $e = exQuery('INSERT', 'os_users', $uarr);
                            print(color("white") . "Successfully created main administrator." . color("lt gray"));
                        } else {
                            print("Either the user name or the password contain invalid characters. Please check them and try again.\n");
                            print("Run the " . color("green") . "help" . color("lt gray") . " command for details on what the requirements are.");
                        }
                    }
                } else {
                    print("Not enough arguments passed. Run " . color('lt blue') . "php mainadmintool.php help" . color('lt gray') . " for command syntax.");
                }
                break;
            case 'delete':
                if (count($argv) > 3) {
                    if (is_array($q) && $q[0]['user_name'] == $argv[3]) {
                        $e = exQuery('DELETE', 'os_users', '', 'user_key = 1');
                        print("Successfully deleted main administrator.");
                    } else {
                        print(color('red') . "Error! " . color('lt gray') . "The user name you entered is not the main administrator or the main administrator does not exist.");
                    }
                } else {
                    print("Not enough arguments passed. Run " . color('lt blue') . "php mainadmintool.php help" . color('lt gray') . " for command syntax.");
                }
                break;
            case 'info':
                /* pull main admin from database */
                if (is_array($q)) {
                    print("Main administrator: " . color('lt blue') . "{$q[0]['user_name']}" . color('lt gray'));
                } else {
                    print("No main administrator has been defined.");
                }
                break;
            case 'reset':
                if (count($argv) > 4) {
                    if (is_array($q) && $q[0]['user_name'] == $argv[3]) {
                        if (validate('password', $argv[4])) {
                            $e = exQuery('UPDATE', 'os_users', array('user_pass' => password_hash($argv[4], PASSWORD_DEFAULT)), 'user_key = 1');
                            print("Successfully reset main administrator password.");
                        } else {
                            print ("Password contains invalid characters. Please check it and try again.");
                            print("Run the " . color("green") . "help" . color("lt gray") . " command for details on what the requirements are.");
                        }
                    } else {
                        print(color('red') . "Error! " . color('lt gray') . "The user name you entered is not the main administrator.");
                    }
                } else {
                    print("Not enough arguments passed. Run " . color('lt blue') . "php mainadmintool.php help" . color('lt gray') . " for command syntax.");
                }
                break;
            case 'help':
                print(helpScreen());
                break;
            default:
                print("Invalid command. Run " . color('lt blue') . "php mainadmintool.php help" . color('lt gray') . " for valid commands.");
                break;
        }
    } else {
        print("Invalid unlock code.");
    }
} else {
    /* help screen */
    print(helpScreen());
}

/* make sure that the last thing that is printed in each case is a newline character */
print("\n");
?>
